#!/bin/sh
#**********************************************************************
#*
#*  Name: backup_db.sh
#*
#**********************************************************************
set -x

if [ $# -ne 1 ];then
   echo 'Usage:$0 SID'
   exit 1;
fi

ORACLE_SID=`echo $1 | tr A-Z a-z`
export ORACLE_SID

# Set environment to this ORACLE_SID
. $HOME/.profile_12c
export mail_list='ricardog@appcard.com,i2w2c4h4l5b5n7v5@appcard.slack.com'
###. $HOME/.maillist


# Skip if it is a physical standby
sqlplus -s "/ as sysdba" <<-EOF > /$HOME/ops/database_role
set verify off
set head off
select database_role from v\$database;
EOF

status=`grep 'PRIMARY' /$HOME/ops/database_role >/dev/null 2>&1; echo $?`
if [  $status -eq 1 ]; then
   rm -f /tmp/database_role
   exit 2
fi

export DATE=`date '+%m%d%Y'`
export NLS_DATE_FORMAT='DD-MON-YYYY_HH24:MI:SS' 
#BACKUP_TAG=incr_backup_`date +"%d-%m-%Y"`
ACRH_TAG=arc_`date +"%d-%m-%Y"`
CRTL_TAG=ctl_`date +"%d-%m-%Y"`
SPF_TAG=spfile_`date +"%d-%m-%Y"`
DAY_OF_WEEK=`date +%a`
BACKUP_TYPE='INCREMENTAL LEVEL 0'
TYPE=FULL
BACKUP_TAG=full_backup_`date +"%d-%m-%Y"`
###export EXP_BACKUP_DIR="/dump/$ORACLE_SID/export"
export RMAN_BACKUP_DIR="/dump/$ORACLE_SID/rman"
export NLS_LANG=AMERICAN_AMERICA.AL32UTF8
export EXP_BACKUP_LOG=export_backup_$DATE.log
export RMAN_BACKUP_LOG=$RMAN_BACKUP_DIR/rman_backup_$DATE.log
export EXP_FILE=export_$DATE
export TRACE_CONTROLFILE=$RMAN_BACKUP_DIR/trace_controlfile_$DATE.ctl
export BACKUP_ERROR=/tmp/backup_error_$$


#clear old export file 
rm $EXP_BACKUP_DIR/*.dmp

echo "Started  DATAPUMP EXPORT backup of $ORACLE_SID at `date`"
# Full datapump export
time expdp backup_admin/backup_admin  DUMPFILE=EXP_DATABASE:$EXP_FILE%U.dmp   LOGFILE=EXP_DATABASE:$EXP_BACKUP_LOG   FULL=YES PARALLEL=4 > /dev/null 2>&1
###exp backup_admin/backup_admin file=$EXP_FILE log=$EXP_BACKUP_LOG full=y statistics=none direct=y recordlength=65535 > /dev/null 2>&1
###gzip $EXP_FILE
#pk gzip /dump/ytrsprd1/rman/ora_*
echo "Finished EXPORT backup of $ORACLE_SID at `date`"

# Check for errors during RMAN backup
# Ignore RMAN-06480: archivelog 'archivelog_file' cannot be found on disk
# and    RMAN-06485: changed archivelog expired
echo "Check $RMAN_BACKUP_LOG and $EXP_BACKUP_LOG" > $BACKUP_ERROR
egrep -a "ORA-"\|"error"\|"RMAN-" $RMAN_BACKUP_LOG | egrep -v "RMAN-06480" | egrep -v "RMAN-06485" > $BACKUP_ERROR
status_rman=$?

# Check for errors during export
egrep -a "ORA-"\|"error"\|"EXP-"  $EXP_BACKUP_LOG | egrep -v "EXP-00079" >> $BACKUP_ERROR
status_exp=$?

# Notify users if errors were found in either RMAN or export backup
if [ $status_rman -eq 0 -o $status_exp -eq 0 ];then
   mail -s "ORACLE ALERT => [`hostname`]: Database backup failed for - $ORACLE_SID!" $mail_list < $BACKUP_ERROR > /dev/null 2>&1
   exit 1
fi

mail -s "[`hostname`]: Database backup succeeded for - $ORACLE_SID" $mail_list < /dev/null > /dev/null 2>&1

# Cleanup and exit
rm -f $BACKUP_ERROR

exit 0
