#!/bin/sh
#**********************************************************************
#*          
#*  Name: all_backup_arch.sh
#*          
#**********************************************************************

# Loop through all databases listed in /etc/oratab
#for DB_ENTRY in `cat /etc/oratab | grep -v \# | grep -v \* | grep -v ^$`
for DB_ENTRY in `cat /etc/oratab | grep -v \# |grep -v ^EM13C |grep -v ^AGENT |grep -v \* | grep -v ^$`
#for DB_ENTRY in `cat /etc/oratab | grep -v \# |grep  -v \* | grep -v ^$`
do
   ORACLE_SID=`echo $DB_ENTRY | awk -F: {' print $1 '}`;
   export ORACLE_SID

   echo "Started archive backup of $ORACLE_SID at `date`"
   $HOME/dbrepos_cron_scripts/bkp/backup_arch.sh $ORACLE_SID
   echo "Finished archive backup of $ORACLE_SID at `date`"
done
#pk 3/1/2017 gzip /dump/ytrsprd1/rman/ora_*
#pk gzip /dump/ytrsprd2/rman/ora_*
