#!/bin/sh
#
# Set up the env
set -x
TNS_ADMIN=$ORACLE_HOME/network/admin
ORACLE_HOME=/usr/lib/oracle/12.2/client64
ORACLE_SID=PRODROTW
export ORACLE_HOME ORACLE_SID TNS_ADMIN
ORAENV_ASK=NO
LD_LIBRARY_PATH=/usr/lib/oracle/12.2/client64/lib
PATH=$PATH:$ORACLE_HOME/bin
export PATH LD_LIBRARY_PATH PATH ORAENV_ASK
export TMPFILE=/tmp/full_scans.txt
#

if [ "$#" -ne 1 ]; then
  echo "Usage: $0 <ORACLE_SID>"
  exit 1
fi

export ENCPWD=`cat /home/oracle/plprod_cron_scripts/encrypted_passwd | openssl enc -base64 -d`
###export MAILING_LIST="chak@appcard.com i2w2c4h4l5b5n7v5@appcard.slack.com ricardog@appcard.com t1p1k2d1l1v2m2d5@appcard.slack.com"
export MAILLIST="ricardog@appcard.com"

export ORACLE_SID=$1

export REPORT=/tmp/full_scans_$ORACLE_SID.rpt
HOST="$HOSTNAME"
echo "To: $MAILLIST" > $TMPFILE
###echo "From: $HOST " >> $TMPFILE
echo "Subject: Tables and Queries with full scans in database $ORACLE_SID on server $HOST" >> $TMPFILE
echo "Content-type: text/html" >> $TMPFILE
echo "MIME-Version: 1.0" >> $TMPFILE

### Only with Rows > 1000000 ###

sqlplus -s "appcard_master/$ENCPWD@$ORACLE_SID" << EOF
SET SERVEROUTPUT ON SIZE 100000
spool $REPORT

set verify off
set term off
set echo off
set pagesize 600
set feedback off
set linesize 100
set trimout on
set trimspool on
set heading on
set markup html on spool on PREFORMAT OFF ENTMAP OFF -
HEAD "<TITLE>Tables and Queries with full scans</TITLE> -
<STYLE type='text/css'> -
   BODY {background: white} -
   TH {background: white} -
   TD {background: white} -
</STYLE>" -
BODY 'TEXT="black"' -
TABLE "BORDER='1'"
ttitle left "<H><u> Verify SqlId Exec Plans for improvement </u></H>"
column name heading "TABLE NAME" format a32
column owner format a20

SELECT p.owner, p.NAME, t.num_rows, LTRIM (t.CACHE) cache,
DECODE (t.BUFFER_POOL, 'KEEP', 'Y          ', 'DEFAULT', 'N          ') buffer_keep,
s.blocks blocks, SUM (a.executions) executions, p.sql_id
FROM dba_tables t,
dba_segments s,
v$sqlarea a,
(SELECT DISTINCT address, object_owner owner, object_name NAME, sql_id
FROM v$sql_plan
WHERE operation = 'TABLE ACCESS' AND options = 'FULL') p
WHERE a.address = p.address
AND t.owner = s.owner
AND t.table_name = s.segment_name
AND t.table_name = p.NAME
AND t.owner = p.owner
AND t.owner NOT IN ('SYS', 'SYSTEM', 'AUDSYS', 'GSMADMIN_INTERNAL', 'DBSNMP', 'XDB', 'RDSADMIN')
AND t.num_rows > 1000000
HAVING SUM (a.executions) > 1
GROUP BY p.owner, p.NAME, t.num_rows, t.CACHE, t.BUFFER_POOL, s.blocks, p.sql_id
ORDER BY t.num_rows DESC, executions DESC;spool off
EXIT
EOF

cat $REPORT  >> $TMPFILE
/usr/sbin/sendmail -t < $TMPFILE

