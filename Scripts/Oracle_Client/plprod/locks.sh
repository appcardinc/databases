#!/bin/sh
#
# Set up the env
TNS_ADMIN=$ORACLE_HOME/network/admin
ORACLE_HOME=/usr/lib/oracle/12.2/client64
ORACLE_SID=PRODROTW
export ORACLE_HOME ORACLE_SID TNS_ADMIN
ORAENV_ASK=NO
LD_LIBRARY_PATH=/usr/lib/oracle/12.2/client64/lib
PATH=$PATH:$ORACLE_HOME/bin
export PATH LD_LIBRARY_PATH PATH ORAENV_ASK
#

if [ "$#" -ne 2 ]; then
  echo "Usage: $0 <ORACLE_SID> <Num_Locks_Threshold>" 
  exit 1
fi

export ENCPWD=`cat /home/oracle/plprod_cron_scripts/encrypted_passwd | openssl enc -base64 -d`
export MAILING_LIST="chak@appcard.com i2w2c4h4l5b5n7v5@appcard.slack.com ricardog@appcard.com t1p1k2d1l1v2m2d5@appcard.slack.com"
#export MAILING_LIST="ricardog@appcard.com"

export ORACLE_SID=$1
export MAX_LOCKS=$2

export REPORT=/tmp/max_locks_$ORACLE_SID.rpt
export REPORT2=/tmp/max_locks_2_$ORACLE_SID.rpt
export SERVER_NAME=`hostname`

sqlplus -s "appcard_master/$ENCPWD@$ORACLE_SID" << EOF
SET SERVEROUTPUT ON SIZE 100000
spool $REPORT

declare
v_lag number(20);
begin
select (
select count(*) from v\$lock where BLOCK > 0 
)
into v_lag
from dual;

if v_lag > $MAX_LOCKS
 then 
 dbms_output.put_line('Database Locks :  sessions Page DBA immediately');
 dbms_output.put_line('Locks are  = ' || v_lag);
 dbms_output.put_line('MAX_LOCKS  = ' || $MAX_LOCKS);
end if;  
end;
/

spool off
EXIT
EOF

sqlplus -s "appcard_master/$ENCPWD@$ORACLE_SID" << EOF
SET SERVEROUTPUT ON SIZE 100000
spool $REPORT2

set lines 1200
COLUMN owner FORMAT A20
COLUMN username FORMAT A20
COLUMN object_owner FORMAT A20
COLUMN object_name FORMAT A30
COLUMN locked_mode FORMAT A15

SELECT lo.session_id AS sid,
       s.serial#,
       NVL(lo.oracle_username, '(oracle)') AS username,
       o.owner AS object_owner,
       o.object_name,
       Decode(lo.locked_mode, 0, 'None',
                             1, 'Null (NULL)',
                             2, 'Row-S (SS)',
                             3, 'Row-X (SX)',
                             4, 'Share (S)',
                             5, 'S/Row-X (SSX)',
                             6, 'Exclusive (X)',
                             lo.locked_mode) locked_mode,
       lo.os_user_name
FROM   v\$locked_object lo
       JOIN dba_objects o ON o.object_id = lo.object_id
       JOIN v\$session s ON lo.session_id = s.sid
ORDER BY 1, 2, 3, 4;

spool off
EXIT
EOF

Response=`grep -c 'Page DBA immediately' $REPORT`
if [ $Response -gt 0 ]
then
    echo 'Database locks issue, please check' $1 | mail -s " DB Lock issue on $1" $MAILING_LIST 
    cat $REPORT2 | mail -s " Locked Objects on $1" $MAILING_LIST
fi
