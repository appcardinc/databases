#!/bin/sh
#
# refresh table pmt.missing_promotions_mv
#
# Set up the env
#set -x
TNS_ADMIN=$ORACLE_HOME/network/admin
###TNS_ADMIN=/u01/app/oracle/product/12.1.0/dbhome_1/network/admin
ORACLE_HOME=/usr/lib/oracle/12.2/client64
ORACLE_SID=PRODROTW
export ORACLE_HOME ORACLE_SID TNS_ADMIN
ORAENV_ASK=NO
LD_LIBRARY_PATH=/usr/lib/oracle/12.2/client64/lib
PATH=$PATH:$ORACLE_HOME/bin
export PATH LD_LIBRARY_PATH PATH ORAENV_ASK
#

if [ "$#" -ne 1 ]; then
  echo "Usage: $0 <ORACLE_SID>"
  exit 1
fi

export ENCPWD=`cat /home/oracle/plprod_cron_scripts/encrypted_passwd | openssl enc -base64 -d`
export MAIL_RECIPIENTS="chak@appcard.com smorales@appcard.com ricardog@appcard.com"
###export MAIL_RECIPIENTS="ricardog@appcard.com"

export ORACLE_SID=$1

export REPORT=/tmp/missing_promotions_mv_refresh_$ORACLE_SID.rpt
export SCRIPT_LOC=/home/oracle/plprod_cron_scripts
export SERVER_NAME=`hostname`

sqlplus -s "appcard_master/$ENCPWD@$ORACLE_SID" << EOF

set head off
set feedback off
spo $REPORT

select 'Refresh PMT.MISSING_PROMOTIONS_MV' from dual;

select 'Started: '||to_char(sysdate, 'YYYY-MON-DD HH24:MI:SS') from dual;

drop table PMT.MISSING_PROMOTIONS_MV;

create table PMT.MISSING_PROMOTIONS_MV nologging 
as
SELECT sp.store_id as store_id, sp.promotion_id, p.promotion_type as promotion_type, 
  p.start_date as start_date, p.end_date as end_date, bp.status_id as status_id, 
  l.run_code as run_code, l.last_run as last_run
FROM
(
  SELECT sp.store_id AS store_id, sp.promotion_id as promotion_id
  FROM pmt.promotions_by_store_mv sp
  MINUS
  SELECT pl.store_id as store_id, pl.promotion_id as promotion_id
  FROM COMMON.store_promo_list pl
    JOIN pmt.promotions p ON p.promotion_id = pl.promotion_id
  WHERE pl.on_jbrain = 1
    AND run_code = (SELECT run_code from common.latest_evaluation_mv)
) sp
JOIN pmt.promotions p ON p.promotion_id = sp.promotion_id
JOIN pmt.business_process bp ON bp.business_process_id = p.business_process_id
CROSS JOIN common.latest_evaluation_mv l
WHERE trunc(SYSDATE) BETWEEN trunc(p.start_date -  7) AND trunc(p.end_date)
  AND bp.status_id >= 8 
  AND bp.status_id != 9;

grant select on PMT.MISSING_PROMOTIONS_MV to public;        

select 'Ended: '||to_char(sysdate, 'YYYY-MON-DD HH24:MI:SS') from dual;

select 'Number of Records PMT.MISSING_PROMOTIONS_MV: '||count(1) from PMT.MISSING_PROMOTIONS_MV;

spo off;

EXIT
EOF

 
echo -e "Subject: Refresh PMT.MISSING_PROMOTIONS_MV on PRODROTW Oracle 12c AWS RDS\nTo: $MAIL_RECIPIENTS" | cat - $REPORT | /usr/sbin/sendmail $MAIL_RECIPIENTS 
