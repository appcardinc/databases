#!/bin/sh
#################################################################
# Script - prodrotw_gather_stats_select_tables.sh
# Owner - Dobler Consulting LLC                                 #
# Date - 02/06/2019                                             #
#################################################################
TNS_ADMIN=$ORACLE_HOME/network/admin
ORACLE_HOME=/usr/lib/oracle/12.2/client64
export ORACLE_HOME ORACLE_SID TNS_ADMIN
ORAENV_ASK=NO
LD_LIBRARY_PATH=/usr/lib/oracle/12.2/client64/lib
PATH=$PATH:$ORACLE_HOME/bin
export PATH LD_LIBRARY_PATH PATH ORAENV_ASK

if [ "$#" -ne 1 ]; then
  echo "Usage: $0 <ORACLE_SID>"
  exit 1
fi

export ORACLE_SID=$1

export ENCPWD=`cat /home/oracle/plprod_cron_scripts/encrypted_passwd | openssl enc -base64 -d`
#MAILLIST=ricardog@appcard.com
MAILLIST="smorales@appcard.com,chak@appcard.com,ricardog@appcard.com"
HOST="$HOSTNAME"

export REPORT="/tmp/${ORACLE_SID}_gather_stats_select_tables.rpt"
export SERVER_NAME=`hostname`

sqlplus -s "appcard_master/$ENCPWD@$ORACLE_SID" << EOF
set head off
set feedback off
spo $REPORT

select 'Start: '||to_char(sysdate, 'YYYY-MON-DD HH24:MI:SS')||' $ORACLE_SID: Oracle Statistics for Selected Tables' from dual;

select ' '||to_char(sysdate, 'YYYY-MON-DD HH24:MI:SS')||' - Gather Table Statistics: CDS.ENTITY' from dual;
EXEC DBMS_STATS.gather_table_stats('CDS', 'ENTITY',estimate_percent => dbms_stats.auto_sample_size, method_opt => 'for all columns size skewonly', cascade => TRUE, degree => 8);
select ' '||to_char(sysdate, 'YYYY-MON-DD HH24:MI:SS')||' - Gather Table Statistics: CDS.COMM_OPT_OUT' from dual;
EXEC DBMS_STATS.gather_table_stats('CDS', 'COMM_OPT_OUT',estimate_percent => dbms_stats.auto_sample_size, method_opt => 'for all columns size skewonly', cascade => TRUE, degree => 8);
select ' '||to_char(sysdate, 'YYYY-MON-DD HH24:MI:SS')||' - Gather Table Statistics: CDS.COMMUNICATION' from dual;
EXEC DBMS_STATS.gather_table_stats('CDS', 'COMMUNICATION',estimate_percent => dbms_stats.auto_sample_size, method_opt => 'for all columns size skewonly', cascade => TRUE, degree => 8);
select ' '||to_char(sysdate, 'YYYY-MON-DD HH24:MI:SS')||' - Gather Table Statistics: CDS.GLOBAL_DATA' from dual;
EXEC DBMS_STATS.gather_table_stats('CDS', 'GLOBAL_DATA',estimate_percent => dbms_stats.auto_sample_size, method_opt => 'for all columns size skewonly', cascade => TRUE, degree => 8);
select ' '||to_char(sysdate, 'YYYY-MON-DD HH24:MI:SS')||' - Gather Table Statistics: CDS.CONSUMER' from dual;
EXEC DBMS_STATS.gather_table_stats('CDS', 'CONSUMER',estimate_percent => dbms_stats.auto_sample_size, method_opt => 'for all columns size skewonly', cascade => TRUE, degree => 8);
select ' '||to_char(sysdate, 'YYYY-MON-DD HH24:MI:SS')||' - Gather Table Statistics: CDS.PERSON' from dual;
EXEC DBMS_STATS.gather_table_stats('CDS', 'PERSON',estimate_percent => dbms_stats.auto_sample_size, method_opt => 'for all columns size skewonly', cascade => TRUE, degree => 8);
select ' '||to_char(sysdate, 'YYYY-MON-DD HH24:MI:SS')||' - Gather Table Statistics: CDS.HOUSEHOLD' from dual;
EXEC DBMS_STATS.gather_table_stats('CDS', 'HOUSEHOLD',estimate_percent => dbms_stats.auto_sample_size, method_opt => 'for all columns size skewonly', cascade => TRUE, degree => 8);
select ' '||to_char(sysdate, 'YYYY-MON-DD HH24:MI:SS')||' - Gather Table Statistics: CDS.COMM_OPT_OUT' from dual;
EXEC DBMS_STATS.gather_table_stats('CDS', 'COMM_OPT_OUT',estimate_percent => dbms_stats.auto_sample_size, method_opt => 'for all columns size skewonly', cascade => TRUE, degree => 8);
select ' '||to_char(sysdate, 'YYYY-MON-DD HH24:MI:SS')||' - Gather Table Statistics: CDS.LOYALTY_HISTORY' from dual;
EXEC DBMS_STATS.gather_table_stats('CDS', 'LOYALTY_HISTORY',estimate_percent => dbms_stats.auto_sample_size, method_opt => 'for all columns size skewonly', cascade => TRUE, degree => 8);
select ' '||to_char(sysdate, 'YYYY-MON-DD HH24:MI:SS')||' - Gather Table Statistics: COMMON.STORE_PROMO_LIST' from dual;
EXEC DBMS_STATS.gather_table_stats('COMMON', 'STORE_PROMO_LIST',estimate_percent => dbms_stats.auto_sample_size, method_opt => 'for all columns size skewonly', cascade => TRUE, degree => 8);
select ' '||to_char(sysdate, 'YYYY-MON-DD HH24:MI:SS')||' - Gather Table Statistics: COMMON.LABEL' from dual;
EXEC DBMS_STATS.gather_table_stats('COMMON', 'LABEL',estimate_percent => dbms_stats.auto_sample_size, method_opt => 'for all columns size skewonly', cascade => TRUE, degree => 8);
select ' '||to_char(sysdate, 'YYYY-MON-DD HH24:MI:SS')||' - Gather Table Statistics: COMMON.GLOBAL_DATA_DEFINITION' from dual;
EXEC DBMS_STATS.gather_table_stats('COMMON', 'GLOBAL_DATA_DEFINITION',estimate_percent => dbms_stats.auto_sample_size, method_opt => 'for all columns size skewonly', cascade => TRUE, degree => 8);
select ' '||to_char(sysdate, 'YYYY-MON-DD HH24:MI:SS')||' - Gather Table Statistics: COMMON.STORE_VARIABLES' from dual;
EXEC DBMS_STATS.gather_table_stats('COMMON', 'STORE_VARIABLES',estimate_percent => dbms_stats.auto_sample_size, method_opt => 'for all columns size skewonly', cascade => TRUE, degree => 8);
select ' '||to_char(sysdate, 'YYYY-MON-DD HH24:MI:SS')||' - Gather Table Statistics: PMT.DATA_LIST' from dual;
EXEC DBMS_STATS.gather_table_stats('PMT', 'DATA_LIST',estimate_percent => dbms_stats.auto_sample_size, method_opt => 'for all columns size skewonly', cascade => TRUE, degree => 8);
select ' '||to_char(sysdate, 'YYYY-MON-DD HH24:MI:SS')||' - Gather Table Statistics: PMT.DEFINITION_LIST' from dual;
EXEC DBMS_STATS.gather_table_stats('PMT', 'DEFINITION_LIST',estimate_percent => dbms_stats.auto_sample_size, method_opt => 'for all columns size skewonly', cascade => TRUE, degree => 8);
select ' '||to_char(sysdate, 'YYYY-MON-DD HH24:MI:SS')||' - Gather Table Statistics: PMT.DATA_LIST_VALUE' from dual;
EXEC DBMS_STATS.gather_table_stats('PMT', 'DATA_LIST_VALUE',estimate_percent => dbms_stats.auto_sample_size, method_opt => 'for all columns size skewonly', cascade => TRUE, degree => 8);
select ' '||to_char(sysdate, 'YYYY-MON-DD HH24:MI:SS')||' - Gather Table Statistics: PMT.MISSING_PROMOTIONS_MV' from dual;
EXEC DBMS_STATS.gather_table_stats('PMT', 'MISSING_PROMOTIONS_MV',estimate_percent => dbms_stats.auto_sample_size, method_opt => 'for all columns size skewonly', cascade => TRUE, degree => 8);
select ' '||to_char(sysdate, 'YYYY-MON-DD HH24:MI:SS')||' - Gather Table Statistics: PMT.PROMOTIONS_BY_STORE_MV' from dual;
EXEC DBMS_STATS.gather_table_stats('PMT', 'PROMOTIONS_BY_STORE_MV',estimate_percent => dbms_stats.auto_sample_size, method_opt => 'for all columns size skewonly', cascade => TRUE, degree => 8);
select ' '||to_char(sysdate, 'YYYY-MON-DD HH24:MI:SS')||' - Gather Table Statistics: PMT.PROMOTIONS' from dual;
EXEC DBMS_STATS.gather_table_stats('PMT', 'PROMOTIONS',estimate_percent => dbms_stats.auto_sample_size, method_opt => 'for all columns size skewonly', cascade => TRUE, degree => 8);
select ' '||to_char(sysdate, 'YYYY-MON-DD HH24:MI:SS')||' - Gather Table Statistics: PMT.EXCLUDED_STORES' from dual;
EXEC DBMS_STATS.gather_table_stats('PMT', 'EXCLUDED_STORES',estimate_percent => dbms_stats.auto_sample_size, method_opt => 'for all columns size skewonly', cascade => TRUE, degree => 8);
select ' '||to_char(sysdate, 'YYYY-MON-DD HH24:MI:SS')||' - Gather Table Statistics: PMT.DELIVERY_CONFIRMATION' from dual;
EXEC DBMS_STATS.gather_table_stats('PMT', 'DELIVERY_CONFIRMATION',estimate_percent => dbms_stats.auto_sample_size, method_opt => 'for all columns size skewonly', cascade => TRUE, degree => 8);
select ' '||to_char(sysdate, 'YYYY-MON-DD HH24:MI:SS')||' - Gather Table Statistics: PMT.DEFINITION_BENEFIT' from dual;
EXEC DBMS_STATS.gather_table_stats('PMT', 'DEFINITION_BENEFIT',estimate_percent => dbms_stats.auto_sample_size, method_opt => 'for all columns size skewonly', cascade => TRUE, degree => 8);
select ' '||to_char(sysdate, 'YYYY-MON-DD HH24:MI:SS')||' - Gather Table Statistics: PMT.DEFINITION_VARIABLE' from dual;
EXEC DBMS_STATS.gather_table_stats('PMT', 'DEFINITION_VARIABLE',estimate_percent => dbms_stats.auto_sample_size, method_opt => 'for all columns size skewonly', cascade => TRUE, degree => 8);


select 'End: '||to_char(sysdate, 'YYYY-MON-DD HH24:MI:SS') from dual;

spo off;

EXIT
EOF

cat $REPORT | mail -s "Oracle $ORACLE_SID: Oracle Statistics for Selected Tables" $MAILLIST

