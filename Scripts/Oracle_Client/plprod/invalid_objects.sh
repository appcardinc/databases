#!/bin/sh
#################################################################
# Script - invalid_objects.sh                                   #
# Owner - Dobler Consulting LLC                                 #
# Date - 09/20/2018                                             #
#################################################################
TNS_ADMIN=$ORACLE_HOME/network/admin
ORACLE_HOME=/usr/lib/oracle/12.2/client64
ORACLE_SID=PRODROTW
export ORACLE_HOME ORACLE_SID TNS_ADMIN
ORAENV_ASK=NO
LD_LIBRARY_PATH=/usr/lib/oracle/12.2/client64/lib
PATH=$PATH:$ORACLE_HOME/bin
export PATH LD_LIBRARY_PATH PATH ORAENV_ASK

if [ "$#" -ne 1 ]; then
  echo "Usage: $0 <ORACLE_SID>" 
  exit 1
fi

export ORACLE_SID=$1

export ENCPWD=`cat /home/oracle/plprod_cron_scripts/encrypted_passwd | openssl enc -base64 -d`
#MAILLIST=ricardog@appcard.com
MAILLIST="smorales@appcard.com,chak@appcard.com,ricardog@appcard.com"
SPOOLFILE=/tmp/${ORACLE_SID}_invalid_objects.list
HOST="$HOSTNAME"
TMPFILE=/tmp/${ORACLE_SID}_invalid_objects.tmp

#source $PROFILE

if [ -f $TMPFILE ]; then
rm -f $TMPFILE
fi

if [ -f $SPOOLFILE ]; then
rm -f $SPOOLFILE
fi

cnt=`sqlplus -s "appcard_master/$ENCPWD@$ORACLE_SID" << EOF
set head off
set pagesize 0
set trimspool on
set feedback off
select count(*)
from 
   dba_objects 
where 
   status != 'VALID'
/
exit 
EOF`

if [ $cnt -gt 0 ]; then

echo "To: $MAILLIST" > $TMPFILE
echo "From: $HOST " >> $TMPFILE
echo "Subject: Invalid Objects Report for database $ORACLE_SID on server $HOST" >> $TMPFILE 
echo "Content-type: text/html" >> $TMPFILE 
echo "MIME-Version: 1.0" >> $TMPFILE
sqlplus -s "appcard_master/$ENCPWD@$ORACLE_SID" << EOF
 set feed off
 set verify off
 set term off
 set echo off
 set trimout on
 set trimspool on 
set markup html on spool on PREFORMAT OFF ENTMAP OFF -
 HEAD "<TITLE>Invalid Objects Report</TITLE> - <STYLE type='text/css'> -
   BODY {background: white} -
   TH {background: white} -
   TD {background: white} -
</STYLE>" -
BODY 'TEXT="black"' -
TABLE "BORDER='1'"

spool $SPOOLFILE
prompt <H2><center><u> List of invalid Objects in $ORACLE_SID</u></center></H2>
set heading on
set linesize 2000
set pagesize 10000
column Owner format a20 heading "Owner"
column Object_type  format a30 heading "Object Type"
column object_name  format a30 heading "Object Name"
column last_ddl_time format a15 heading "Last DDL Time"

select owner,   object_type, object_name, to_char(last_ddl_time,'mm/dd/yyyy hh24:mi:ss') last_ddl_time
from dba_objects 
where status != 'VALID'
and  owner not in ('SYS','SYSTEM','PUBLIC','CTXSYS')
order by   owner,   object_type, object_name
/
spool off
exit
EOF

cat $SPOOLFILE >> $TMPFILE
/usr/sbin/sendmail -t < $TMPFILE
else
    echo "No Invalid Objects found"
exit 0
fi

