#!/bin/sh
#
# refresh table pmt.promotions_by_store_mv
#
# Set up the env
#set -x
TNS_ADMIN=$ORACLE_HOME/network/admin
###TNS_ADMIN=/u01/app/oracle/product/12.1.0/dbhome_1/network/admin
ORACLE_HOME=/usr/lib/oracle/12.2/client64
ORACLE_SID=PRODROTW
export ORACLE_HOME ORACLE_SID TNS_ADMIN
ORAENV_ASK=NO
LD_LIBRARY_PATH=/usr/lib/oracle/12.2/client64/lib
PATH=$PATH:$ORACLE_HOME/bin
export PATH LD_LIBRARY_PATH PATH ORAENV_ASK
#

if [ "$#" -ne 1 ]; then
  echo "Usage: $0 <ORACLE_SID>"
  exit 1
fi

export ENCPWD=`cat /home/oracle/plprod_cron_scripts/encrypted_passwd | openssl enc -base64 -d`
export MAIL_RECIPIENTS="chak@appcard.com smorales@appcard.com ricardog@appcard.com"
###export MAIL_RECIPIENTS="ricardog@appcard.com"

export ORACLE_SID=$1

export REPORT=/tmp/promotions_by_store_mv_refresh_$ORACLE_SID.rpt
export SCRIPT_LOC=/home/oracle/plprod_cron_scripts
export SERVER_NAME=`hostname`

sqlplus -s "appcard_master/$ENCPWD@$ORACLE_SID" << EOF

set head off
set feedback off
spo $REPORT

select 'Refresh PMT.PROMOTIONS_BY_STORE_MV' from dual;

select 'Started: '||to_char(sysdate, 'YYYY-MON-DD HH24:MI:SS') from dual;

drop table PMT.TEMP_PROMOTIONS_BY_STORE_MV;

create table PMT.TEMP_PROMOTIONS_BY_STORE_MV nologging
as
  SELECT s.store_id AS store_id, p.promotion_id as promotion_id
  FROM common."STORE" s
    JOIN common.store_group sg ON sg.store_id = s.store_id AND sg.deleted IS NULL
    JOIN pmt.campaign_deliveries cd ON cd.group_id = sg.group_id AND cd.deleted IS NULL
    JOIN pmt.promotions p ON p.campaign_id = cd.campaign_id AND p.deleted IS NULL
    AND is_cancelled = 0
  WHERE s.load_promotions = 1
    AND s.deleted IS NULL
  MINUS
  SELECT s.store_id as store_id, p.promotion_id as promotion_id
  FROM common."STORE" s
    JOIN common.store_group sg ON sg.store_id = s.store_id AND sg.deleted IS NULL
    JOIN pmt.campaign_deliveries cd ON cd.group_id = sg.group_id AND cd.deleted IS NULL
    JOIN pmt.promotions p ON p.campaign_id = cd.campaign_id AND p.deleted IS NULL
    JOIN pmt.excluded_stores es ON es.store_id = s.store_id AND es.promotion_id = p.promotion_id AND es.deleted IS NULL;

create index PMT.TEMP_PROMS_BY_STORE_MV_IDX on PMT.TEMP_PROMOTIONS_BY_STORE_MV(promotion_id); 

drop table PMT.PROMOTIONS_BY_STORE_MV;

create table PMT.PROMOTIONS_BY_STORE_MV nologging 
as
SELECT sp.store_id as store_id, sp.promotion_id, p.campaign_id, p.promotion_type as promotion_type, 
  p.start_date as start_date, p.end_date as end_date, 
  l."NAME" AS status,
  bp.status_id as status_id,
  p.name,
  DBMS_LOB.SUBSTR(p.description, 1000, 1) as description
FROM
     PMT.TEMP_PROMOTIONS_BY_STORE_MV sp
JOIN pmt.promotions p ON p.promotion_id = sp.promotion_id
JOIN pmt.business_process bp ON bp.business_process_id = p.business_process_id
JOIN common.label l ON l.label_id = bp.status_id 
            AND l.label_group_id = 14 
            AND l.language_id = 'EN';

grant select on PMT.PROMOTIONS_BY_STORE_MV to public;        

select 'Ended: '||to_char(sysdate, 'YYYY-MON-DD HH24:MI:SS') from dual;

select 'Number of Records PMT.PROMOTIONS_BY_STORE_MV: '||count(1) from PMT.PROMOTIONS_BY_STORE_MV;

spo off;

EXIT
EOF

 
echo -e "Subject: Refresh PMT.PROMOTIONS_BY_STORE_MV on PRODROTW Oracle 12c AWS RDS\nTo: $MAIL_RECIPIENTS" | cat - $REPORT | /usr/sbin/sendmail $MAIL_RECIPIENTS 
