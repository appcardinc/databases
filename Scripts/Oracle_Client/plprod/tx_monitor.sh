#!/bin/sh
#
# Set up the env
TNS_ADMIN=$ORACLE_HOME/network/admin
ORACLE_HOME=/usr/lib/oracle/12.2/client64
ORACLE_SID=PRODROTW
export ORACLE_HOME ORACLE_SID TNS_ADMIN
ORAENV_ASK=NO
LD_LIBRARY_PATH=/usr/lib/oracle/12.2/client64/lib
PATH=$PATH:/usr/sbin:$ORACLE_HOME/bin
export PATH LD_LIBRARY_PATH PATH ORAENV_ASK

if [ "$#" -ne 1 ]; then
  echo "Usage: $0 <ORACLE_SID>"
  exit 1
fi

export ENCPWD=`cat /home/oracle/plprod_cron_scripts/encrypted_passwd | openssl enc -base64 -d`
export MAILING_LIST="smorales@appcard.com,dlocke@appcard.com,lrecker@appcard.com,edo@appcard.com,tomer@appcard.com,elainea@appcard.com,techsupport@appcard.com,prologicsupport@appcard.com,ricardog@appcard.com,bgadjev@appcard.com"

export ORACLE_SID=$1

export REPORT=/tmp/tlog_activity_check.html

sqlplus -s "appcard_master/$ENCPWD@$ORACLE_SID" << EOF
SET SERVEROUTPUT ON SIZE 1000000
spool $REPORT

declare 
  l_message clob;

  procedure print_clob( p_clob in clob ) is
      v_offset number default 1;
      v_chunk_size number := 10000;
  begin
      loop
          exit when v_offset > dbms_lob.getlength(p_clob);
          dbms_output.put_line( dbms_lob.substr( p_clob, v_chunk_size, v_offset ) );
          v_offset := v_offset +  v_chunk_size;
      end loop;
  end print_clob;

begin
  l_message := NULL;
  COMMON.TX_MONITOR.VERIFY_TX_ACTIVITY(SYSTIMESTAMP, 0.5, l_message);
  IF l_message IS NOT NULL THEN
    print_clob(l_message);
  END IF;
END;
/

spool off
EXIT
EOF

resp=$(wc -l < "$REPORT")
if [ $resp -gt 3 ]
then
    a=`mktemp`
    echo "MIME-Version: 1.0" > $a
    echo "Content-Type: text/html" >> $a
    echo "Subject: Database Monitor Warning - Data Missing From Stores" >> $a

    echo -e '<p style="color:red">Warning! Data Missing From Monitored Tables!</p><br>' >> $a
    cat $REPORT | grep -v '^PL' >> $a

    cat $a | sendmail -F "TX Monitor" $MAILING_LIST
fi
