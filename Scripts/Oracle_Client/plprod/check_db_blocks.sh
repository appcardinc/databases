#!/bin/sh
#################################################################
# Script - check_db_blocks.sh                                   #
# Owner - Dobler Consulting LLC                                 #
# Date - 09/20/2018                                             #
#################################################################
# Set up the env
TNS_ADMIN=$ORACLE_HOME/network/admin
ORACLE_HOME=/usr/lib/oracle/12.2/client64
ORACLE_SID=PRODROTW
export ORACLE_HOME ORACLE_SID TNS_ADMIN
ORAENV_ASK=NO
LD_LIBRARY_PATH=/usr/lib/oracle/12.2/client64/lib
PATH=$PATH:$ORACLE_HOME/bin
export PATH LD_LIBRARY_PATH PATH ORAENV_ASK

if [ "$#" -ne 1 ]; then
  echo "Usage: $0 <ORACLE_SID>" 
  exit 1
fi

export ORACLE_SID=$1

export ENCPWD=`cat /home/oracle/plprod_cron_scripts/encrypted_passwd | openssl enc -base64 -d`
MAILLIST="smorales@appcard.com,chak@appcard.com,ricardog@appcard.com,i2w2c4h4l5b5n7v5@appcard.slack.com,t1p1k2d1l1v2m2d5@appcard.slack.com"
SPOOLFILE=/tmp/${ORACLE_SID}_block.sql
HOST="$HOSTNAME"
TMPFILE=/tmp/${ORACLE_SID}_block.tmp

#source $PROFILE

if [ -f $TMPFILE ]; then
rm -f $TMPFILE
fi

if [ -f $SPOOLFILE ]; then
rm -f $SPOOLFILE
fi

lmins=`sqlplus -s "appcard_master/$ENCPWD@$ORACLE_SID" << EOF
set head off
set feedback off
select count(*) 
from v\\$lock w, v\\$lock h 
where w.id1=h.id1 and w.id2=h.id2 and w.request=6 and h.lmode in (3,6) and w.ctime/20 > 1;
exit;
EOF`

if [ $lmins -ge 1 ]; then
echo "To: $MAILLIST" > $TMPFILE
echo "From: $HOST " >> $TMPFILE
echo "Subject: Blocks found in database $ORACLE_SID on server $HOST" >> $TMPFILE
echo "Content-type: text/html" >> $TMPFILE
echo "MIME-Version: 1.0" >> $TMPFILE

sqlplus -s "appcard_master/$ENCPWD@$ORACLE_SID" << EOF
set feed off
set verify off
set term off
set echo off
set trimout on
set trimspool on
set markup html on spool on PREFORMAT OFF ENTMAP OFF -
HEAD "<TITLE>Lock Report</TITLE> -
<STYLE type='text/css'> -
   BODY {background: white} -
   TH {background: white} -
   TD {background: white} -
</STYLE>" -
BODY 'TEXT="black"' -
TABLE "BORDER='1'"
spool $SPOOLFILE
/*
select l1.sid "Waiting SID", l2.sid "Holding SID" 
from v\$lock l1, v\$lock l2 
where l1.id1=l2.id1 and l1.id2=l2.id2 and l1.request=6 and l2.lmode in (3,6) and l1.ctime/60 > 1
/
*/
set lines 180
ttitle left "<H><u> Blocking/Waiting Session Details </u></H>"
set heading on
SELECT vs.username,
 	vs.osuser,
 	vh.sid blocking_sid,
        vs.serial# blocking_serial#,
 	vs.status blocking_user_status,
 	vs.module blocking_module,
 	vs.program blocking_program,
 	jrh.job_name blocking_job,
 	vsw.username waiting_dbuser,
 	vsw.osuser waiting_osuser,
 	vw.sid waiting_sid,
    vsw.serial# waiting_serial#,
    vsw.status waiting_user_status,
    vsw.module waiting_user_module,
 	vsw.program waiting_program,
 	jrw.job_name waiting_jobname
 FROM v\$lock vh, v\$lock vw, v\$session vs, v\$session vsw, dba_scheduler_running_jobs jrh, dba_scheduler_running_jobs jrw
	WHERE     (vh.id1, vh.id2) IN (SELECT id1, id2
 	FROM v\$lock
 	WHERE request = 0
 INTERSECT
 	SELECT id1, id2
 	FROM v\$lock
 	WHERE lmode = 0)
 	AND vh.id1 = vw.id1
 	AND vh.id2 = vw.id2
 	AND vh.request = 0
 	AND vw.lmode = 0
 	AND vh.sid = vs.sid
 	AND vw.sid = vsw.sid
 	AND vh.sid = jrh.session_id(+)
 	AND vw.sid = jrw.session_id(+)

/*
column sid heading SID for 999999
column serial# heading SERIAL# for 99999999
column Object_Name heading "Object Name" for a30
column "Event" for a35 heading Event
column module for a40 heading Module
column client_identifier for a18 heading "User"
SELECT level,
       LPAD(' ', (level-1)*2, ' ') || NVL(s.username, '(oracle)') AS username,
       s.osuser,
       s.sid,
       s.serial#,
       s.blocking_session,
       trunc(s.last_call_et/60,0) "Mins in State",
       s.status,
       o.object_name,
       t.sql_text,
       s.module,
       s.machine,
       s.program,
       TO_CHAR(s.logon_Time,'DD-MON-YYYY HH24:MI:SS') AS logon_time
FROM   v\$session s
LEFT OUTER JOIN dba_objects o
       ON (object_id = row_wait_obj#)
  LEFT OUTER JOIN v\$sql t
       USING (sql_id)
WHERE  level > 1
OR     EXISTS (SELECT 1
               FROM   v\$session
               WHERE  blocking_session = s.sid)
CONNECT BY PRIOR s.sid = s.blocking_session
START WITH s.blocking_session IS NULL
*/
/
spool off
exit
EOF
cat $SPOOLFILE >> $TMPFILE
/usr/sbin/sendmail -t < $TMPFILE
#rm -f $TMPFILE
else
    echo "No database blocks found in $ORACLE_SID "
    exit
fi


