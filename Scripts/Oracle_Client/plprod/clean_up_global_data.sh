#!/bin/sh
#
# cleanup global data table 
#
# Set up the env
TNS_ADMIN=$ORACLE_HOME/network/admin
ORACLE_HOME=/usr/lib/oracle/12.2/client64
ORACLE_SID=PRODROTW
export ORACLE_HOME ORACLE_SID TNS_ADMIN
ORAENV_ASK=NO
LD_LIBRARY_PATH=/usr/lib/oracle/12.2/client64/lib
PATH=$PATH:$ORACLE_HOME/bin
export PATH LD_LIBRARY_PATH PATH ORAENV_ASK
#

if [ "$#" -ne 1 ]; then
  echo "Usage: $0 <ORACLE_SID>" 
  exit 1
fi

export ENCPWD=`cat /home/oracle/plprod_cron_scripts/encrypted_passwd | openssl enc -base64 -d`
export MAILING_LIST="chak@appcard.com ricardog.appcard.com"

export ORACLE_SID=$1

export REPORT=/tmp/cleanup_global_data_$ORACLE_SID.rpt
export SERVER_NAME=`hostname`

sqlplus -s "appcard_master/$ENCPWD@$ORACLE_SID" << EOF
SET SERVEROUTPUT ON SIZE 100000
set lines 180
set pages 0

spool $REPORT

DECLARE
  l_logical_delete NUMBER := 21;
  l_logical_pdc_delete NUMBER := 7;
  l_physical_delete NUMBER := 39;
  l_commit_count NUMBER := 0;
BEGIN
  -- Logically delete global data definitions for Standard Promotions
  common.global_data_definition_dao.logically_delete_expired(l_logical_delete, 1);
  commit;
  
  -- Logically delete global data definitions for PDC Promotions
  common.global_data_definition_dao.logically_delete_expired(l_logical_pdc_delete, 2);
  commit;
  
  -- Logically delete global data definitions for TPR Promotions
  common.global_data_definition_dao.logically_delete_expired(l_logical_delete, 3);
  commit;
  
  -- Logically delete global data
  l_commit_count := 0;
  FOR rec IN
    (SELECT gd."ID" 
      FROM cds.global_data gd
        JOIN common.global_data_definition gdd USING (variable_id)
      WHERE gdd.deleted IS NOT NULL
        AND gd.deleted IS NULL)
  LOOP
    cds.global_data_dao.delete_global_data(rec."ID");
    l_commit_count := l_commit_count + 1;
    
    IF l_commit_count >= 1000 THEN
      commit;
      l_commit_count := 0;
    END IF;
  END LOOP;
  commit;
  
  -- Physically delete global data (must be done before global data definition
  -- due to key relationship
  l_commit_count := 0;
  FOR rec IN
    (SELECT "ID"
      FROM cds.global_data gd
      WHERE EXISTS (
                    SELECT NULL 
                    FROM common.global_data_definition gdd 
                    WHERE gdd.deleted < SYSDATE - l_physical_delete
                      AND gdd.variable_id = gd.variable_id
                   )
    )
  LOOP
    cds.global_data_dao.physically_delete_global_data(rec."ID");
    l_commit_count := l_commit_count + 1;
    
    IF l_commit_count >= 1000 THEN
      commit;
      l_commit_count := 0;
    END IF;
  END LOOP;
  commit;
  
  -- Physically delete global data definition
  --common.global_data_definition_dao.delete_records(l_physical_delete);
  -- Sam said to comment out this delete
  commit;
END;
/

select 'Num Of Records In The Table CDS.GLOBAL_DATA on Oracle DB Instance $ORACLE_SID: '||count(1) from cds.global_data; 

spool off
EXIT
EOF

cat $REPORT | mail -s "CleanUp Global Data Table on Oracle DB Instance $1 [$(date +'%d/%m/%Y %H:%M:%S %Z')]" $MAILING_LIST
