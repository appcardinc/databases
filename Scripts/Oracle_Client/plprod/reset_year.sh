#!/bin/bash
#################################################################
# Script - reset_year.sh                                 #
# Owner - DBA - Appcard                                         #
# Date - 12/16/2019                                             #
#################################################################
###set -x
TNS_ADMIN=$ORACLE_HOME/network/admin
ORACLE_HOME=/usr/lib/oracle/12.2/client64
ORACLE_SID=PRODROTW
export ORACLE_HOME ORACLE_SID TNS_ADMIN
ORAENV_ASK=NO
LD_LIBRARY_PATH=/usr/lib/oracle/12.2/client64/lib
PATH=$PATH:$ORACLE_HOME/bin
export PATH LD_LIBRARY_PATH PATH ORAENV_ASK

if [ "$#" -ne 1 ]; then
  echo "Usage: $0 <ORACLE_SID>" 
  exit 1
fi

export ORACLE_SID=$1

export ENCPWD=`cat /home/oracle/plprod_cron_scripts/encrypted_passwd | openssl enc -base64 -d`
###MAILLIST="ricardog@appcard.com,i2w2c4h4l5b5n7v5@appcard.slack.com"
MAILLIST="smorales@appcard.com,ricardog@appcard.com,dlocke@appcard.com"
HOST="$HOSTNAME"
TMPFILE=/tmp/${ORACLE_SID}_reset_year.tmp
$SPOOLFILE=/tmp/${ORACLE_SID}_reset_year_out.tmp

if [ -f $TMPFILE ]; then
rm -f $TMPFILE
fi

if [ -f $SPOOLFILE ]; then
rm -f $SPOOLFILE
fi

echo "To: $MAILLIST" > $TMPFILE
echo "From: appcard@appcard.com" >> $TMPFILE
echo "Subject:  Year to date savings global variable reset script in $ORACLE_SID" >> $TMPFILE
echo "Content-type: text/html" >> $TMPFILE
echo "MIME-Version: 1.0" >> $TMPFILE

sqlplus -s "appcard_master/$ENCPWD@$ORACLE_SID" << EOF
set feed off
set verify off
set term off
set echo off
set trimout on
set trimspool on 
set markup html on
set pagesize 1200
set serveroutput on

spool /tmp/PRODROTW_reset_year_out.tmp

DECLARE
  l_commit_count NUMBER := 0;
  l_commit_limit NUMBER := 500;
  l_timestamp TIMESTAMP;
  l_rows NUMBER := 0;
BEGIN
  FOR rec IN
  (
    SELECT id FROM cds.global_data
    WHERE variable_id = 41
      AND value > 0
      AND deleted is null
      AND created < '01-JAN-'||TO_CHAR(TRUNC(sysdate), 'YYYY')
  )
  LOOP
    l_timestamp := systimestamp;
    UPDATE cds.global_data
      SET value = '0', updated = l_timestamp
      WHERE id = rec.id;
      
    l_commit_count := l_commit_count + 1;
    l_rows := l_rows + 1;
      
    IF l_commit_count >= l_commit_limit THEN
      COMMIT;
      l_commit_count := 0;
    END IF;
  END LOOP;
  COMMIT;

      DBMS_OUTPUT.PUT_LINE(l_rows || ' rows updated');

END;
/
exit
EOF

cat /tmp/PRODROTW_reset_year_out.tmp >> $TMPFILE

/usr/sbin/sendmail -t < $TMPFILE
exit
