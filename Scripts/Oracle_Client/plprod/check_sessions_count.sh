#!/bin/sh
#################################################################
# Script - check_sessions_count.sh                                   #
# Owner - Dobler Consulting LLC                                 #
# Date - 08/25/2018                                             #
#################################################################
set -x
TNS_ADMIN=$ORACLE_HOME/network/admin
ORACLE_HOME=/usr/lib/oracle/12.2/client64
ORACLE_SID=PRODROTW
export ORACLE_HOME ORACLE_SID TNS_ADMIN
ORAENV_ASK=NO
LD_LIBRARY_PATH=/usr/lib/oracle/12.2/client64/lib
PATH=$PATH:$ORACLE_HOME/bin
export PATH LD_LIBRARY_PATH PATH ORAENV_ASK

if [ "$#" -ne 1 ]; then
  echo "Usage: $0 <ORACLE_SID>" 
  exit 1
fi

export ORACLE_SID=$1

export ENCPWD=`cat /home/oracle/plprod_cron_scripts/encrypted_passwd | openssl enc -base64 -d`
###MAILLIST="ricardog@appcard.com,i2w2c4h4l5b5n7v5@appcard.slack.com"
MAILLIST="smorales@appcard.com,chak@appcard.com,ricardog@appcard.com,i2w2c4h4l5b5n7v5@appcard.slack.com,t1p1k2d1l1v2m2d5@appcard.slack.com"
SPOOLFILE=/tmp/${ORACLE_SID}_session.sql
HOST="$HOSTNAME"
TMPFILE=/tmp/${ORACLE_SID}_session.tmp

#source $PROFILE

#if [ `ps -ef|grep -i $ORACLE_SID |grep pmon|grep -v grep|wc -l` -eq 0 ];then echo "DB is down"
#exit
#fi



if [ -f $TMPFILE ]; then
rm -f $TMPFILE
fi

if [ -f $SPOOLFILE ]; then
rm -f $SPOOLFILE
fi

lmins=`sqlplus -s "appcard_master/$ENCPWD@$ORACLE_SID" << EOF
set head off
set feedback off
select count(*)
from v\\$session
/
exit 
EOF`

longrun=0
if [ $lmins -gt 5000 ]; then
longrun=`sqlplus -s "appcard_master/$ENCPWD@$ORACLE_SID" << EOF
set head off
set feedback off
select count(*)
from v\\$session
where last_call_et > 5
and status = 'ACTIVE'
and username not in ('SYS','SYSTEM')
; 
exit 
EOF`
fi

if [ $longrun -gt 10 ] 
then

echo "To: $MAILLIST" > $TMPFILE
echo "From: $HOST " >> $TMPFILE
echo "Subject: Warning Alert: High number of active sessions found in $ORACLE_SID on server $HOST" >> $TMPFILE
echo "Content-type: text/html" >> $TMPFILE
echo "MIME-Version: 1.0" >> $TMPFILE

sqlplus -s "appcard_master/$ENCPWD@$ORACLE_SID" << EOF
 set feed off
 set verify off
 set term off
 set echo off
 set trimout on
 set trimspool on 
 set markup html on

spool $SPOOLFILE
SET MARKUP HTML ON PREFORMAT OFF ENTMAP OFF -
HEAD "<TITLE>Active Sessions Report</TITLE> - <STYLE type='text/css'> -
   BODY {background: white} -
   TH {background: white} -
   TD {background: white} -
</STYLE>" -
BODY 'TEXT="black"' -
TABLE "BORDER='1'"

select 
   ses.username,
   ses.machine,
   ses.program,
   ses.status,
   ses.last_call_et,
   sql.hash_value,
   sql.sql_text
from 
   v\$session ses, 
   v\$sql sql
where ses.sql_hash_value = sql.hash_value
and   ses.username != 'SYS'
and   ses.status = 'ACTIVE'
and   last_call_et > 5
/

set lines 180 
prompt <H><u> Resource Utilization </u></H>
prompt
set heading on

select resource_name, current_utilization, max_utilization, limit_value 
    from v\$resource_limit 
    where resource_name in ('sessions', 'processes')
/
spool off
exit
EOF

cat $SPOOLFILE >> $TMPFILE

/usr/sbin/sendmail -t < $TMPFILE
#rm -f $TMPFILE 
else
    echo "Database is running normal $ORACLE_SID "
fi
exit
