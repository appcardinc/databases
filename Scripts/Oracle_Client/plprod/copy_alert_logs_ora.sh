#!/bin/sh
#
# Set up the env
TNS_ADMIN=$ORACLE_HOME/network/admin
ORACLE_HOME=/usr/lib/oracle/12.2/client64
ORACLE_SID=PRODROTW
export ORACLE_HOME ORACLE_SID TNS_ADMIN
ORAENV_ASK=NO
LD_LIBRARY_PATH=/usr/lib/oracle/12.2/client64/lib
PATH=$PATH:$ORACLE_HOME/bin
export PATH LD_LIBRARY_PATH PATH ORAENV_ASK
#

if [ "$#" -ne 1 ]; then
  echo "Usage: $0 <ORACLE_SID>" 
  exit 1
fi

export ENCPWD=`cat encrypted_passwd | openssl enc -base64 -d`
export MAILING_LIST="dmitryr@appcard.com"

export ORACLE_SID=$1

THE_DATE=$(date +"%Y%m%d%H%M")

export REPORT=/tmp/alert_log_$ORACLE_SID_$THE_DATE.rpt
export SERVER_NAME=`hostname`

sqlplus -s "appcard_master/$ENCPWD@$ORACLE_SID" << EOF > $REPORT 
set pages 0
set lines 1200
select message_text from alertlog;
EOF

