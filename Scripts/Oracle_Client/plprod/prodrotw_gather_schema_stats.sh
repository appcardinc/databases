#!/bin/sh
#################################################################
# Script - prodrotw_gather_schema_stats.sh
# Owner - Dobler Consulting LLC                                 #
# Date - 02/06/2019                                             #
#################################################################
TNS_ADMIN=$ORACLE_HOME/network/admin
ORACLE_HOME=/usr/lib/oracle/12.2/client64
export ORACLE_HOME ORACLE_SID TNS_ADMIN
ORAENV_ASK=NO
LD_LIBRARY_PATH=/usr/lib/oracle/12.2/client64/lib
PATH=$PATH:$ORACLE_HOME/bin
export PATH LD_LIBRARY_PATH PATH ORAENV_ASK

if [ "$#" -ne 1 ]; then
  echo "Usage: $0 <ORACLE_SID>"
  exit 1
fi

export ORACLE_SID=$1

export ENCPWD=`cat /home/oracle/plprod_cron_scripts/encrypted_passwd | openssl enc -base64 -d`
#MAILLIST=ricardog@appcard.com
MAILLIST="smorales@appcard.com,chak@appcard.com,ricardog@appcard.com"
HOST="$HOSTNAME"

export REPORT="/tmp/${ORACLE_SID}_gather_schema_stats.rpt"
export SERVER_NAME=`hostname`

sqlplus -s "appcard_master/$ENCPWD@$ORACLE_SID" << EOF
set head off
set feedback off
spo $REPORT

select 'Oracle $ORACLE_SID: Oracle Statistics Collection for Schemas' from dual;

select 'Started: '||to_char(sysdate, 'YYYY-MON-DD HH24:MI:SS') from dual;

select ' '||to_char(sysdate, 'YYYY-MON-DD HH24:MI:SS')||' - Gather Schema Statistics: CDS' from dual;

exec dbms_stats.gather_schema_stats(ownname=>'CDS', estimate_percent=>dbms_stats.auto_sample_size, method_opt=>'for all columns size repeat', degree=>4);

select ' '||to_char(sysdate, 'YYYY-MON-DD HH24:MI:SS')||' - Gather Schema Statistics: PMT' from dual;

exec dbms_stats.gather_schema_stats(ownname=>'PMT', estimate_percent=>dbms_stats.auto_sample_size, method_opt=>'for all columns size repeat', degree=>4);

select ' '||to_char(sysdate, 'YYYY-MON-DD HH24:MI:SS')||' - Gather Schema Statistics: TLOG' from dual;

exec dbms_stats.gather_schema_stats(ownname=>'TLOG', estimate_percent=>dbms_stats.auto_sample_size, method_opt=>'for all columns size repeat', degree=>4);

select 'End: '||to_char(sysdate, 'YYYY-MON-DD HH24:MI:SS') from dual;

exec dbms_stats.gather_schema_stats(ownname=>'COMMON', estimate_percent=>dbms_stats.auto_sample_size, method_opt=>'for all columns size repeat', degree=>4);

select ' '||to_char(sysdate, 'YYYY-MON-DD HH24:MI:SS')||' - Gather Schema Statistics: PMT' from dual;

exec dbms_stats.gather_schema_stats(ownname=>'MPS', estimate_percent=>dbms_stats.auto_sample_size, method_opt=>'for all columns size repeat', degree=>4);

select ' '||to_char(sysdate, 'YYYY-MON-DD HH24:MI:SS')||' - Gather Schema Statistics: PMT' from dual;

exec dbms_stats.gather_schema_stats(ownname=>'TPR_COMMON', estimate_percent=>dbms_stats.auto_sample_size, method_opt=>'for all columns size repeat', degree=>4);

select ' '||to_char(sysdate, 'YYYY-MON-DD HH24:MI:SS')||' - Gather Schema Statistics: PMT' from dual;

exec dbms_stats.gather_schema_stats(ownname=>'TPR_J', estimate_percent=>dbms_stats.auto_sample_size, method_opt=>'for all columns size repeat', degree=>4);

select ' '||to_char(sysdate, 'YYYY-MON-DD HH24:MI:SS')||' - Gather Schema Statistics: PMT' from dual;

spool off;

EXIT
EOF

cat $REPORT | mail -s "Oracle $ORACLE_SID: Oracle Statistics Collection for Schemas" $MAILLIST

