#!/bin/bash
#################################################################
# Script - kill_inactive_24h.sh                                 #
# Owner - DBA - Appcard                                         #
# Date - 12/16/2019                                             #
#################################################################
###set -x
TNS_ADMIN=$ORACLE_HOME/network/admin
ORACLE_HOME=/usr/lib/oracle/12.2/client64
ORACLE_SID=PRODROTW
export ORACLE_HOME ORACLE_SID TNS_ADMIN
ORAENV_ASK=NO
LD_LIBRARY_PATH=/usr/lib/oracle/12.2/client64/lib
PATH=$PATH:$ORACLE_HOME/bin
export PATH LD_LIBRARY_PATH PATH ORAENV_ASK

if [ "$#" -ne 1 ]; then
  echo "Usage: $0 <ORACLE_SID>" 
  exit 1
fi

export ORACLE_SID=$1

export ENCPWD=`cat /home/oracle/plprod_cron_scripts/encrypted_passwd | openssl enc -base64 -d`
###MAILLIST="ricardog@appcard.com,i2w2c4h4l5b5n7v5@appcard.slack.com"
MAILLIST="smorales@appcard.com,chak@appcard.com,ricardog@appcard.com,t1p1k2d1l1v2m2d5@appcard.slack.com,eliyahu@appcard.com,dev@appcard.com"
HOST="$HOSTNAME"
TMPFILE=/tmp/${ORACLE_SID}_24hsession.tmp
TMPKILL=/tmp/${ORACLE_SID}_kill.sql
$SPOOLFILE=/tmp/${ORACLE_SID}_out.tmp

if [ -f $TMPFILE ]; then
rm -f $TMPFILE
fi

if [ -f $SPOOLFILE ]; then
rm -f $SPOOLFILE
fi

if [ -f $TMPKILL ]; then
rm -f $TMPKILL
fi

lmins=`sqlplus -s "appcard_master/$ENCPWD@$ORACLE_SID" << EOF
set head off
set feedback off
select count(*)
from v\\$session
where status in ('INACTIVE', 'IDLE') 
and LAST_CALL_ET > 86400
and username not in ('SYS', 'SYSTEM', 'DBSNMP')
/
exit
EOF`

if [ $lmins -gt 0 ];
then

echo "To: $MAILLIST" > $TMPFILE
echo "From: $HOST " >> $TMPFILE
echo "Subject:  Sessions killed due to being idle or inactive > 24h in $ORACLE_SID" >> $TMPFILE
echo "Content-type: text/html" >> $TMPFILE
echo "MIME-Version: 1.0" >> $TMPFILE

sqlplus -s "appcard_master/$ENCPWD@$ORACLE_SID" << EOF
set feed off
set verify off
set term off
set echo off
set trimout on
set trimspool on 
set markup html on
set pagesize 1200

spool /tmp/PRODROTW_out.tmp
SET MARKUP HTML ON PREFORMAT OFF ENTMAP OFF -
HEAD "<TITLE>Inactive Sessions Report</TITLE> - <STYLE type='text/css'> -
   BODY {background: white} -
   TH {background: white} -
   TD {background: white} -
</STYLE>" -
BODY 'TEXT="black"' -
TABLE "BORDER='1'"

select
   username,
   machine,
   program,
   status,
   last_call_et
from v\$session
where status in ('INACTIVE', 'IDLE')
and LAST_CALL_ET > 86400
and username not in ('SYS', 'SYSTEM', 'DBSNMP')
order by last_call_et desc
/
spool off
exit
EOF

cat /tmp/PRODROTW_out.tmp >> $TMPFILE

sqlplus -s "appcard_master/$ENCPWD@$ORACLE_SID" << EOF
set head off
set linesize 100
set pagesize 0
set echo off
set feedback off
spool $TMPKILL
select 'exec rdsadmin.rdsadmin_util.kill(sid=> ' || sid || ', serial=> ' || SERIAL# || ', method=> ''IMMEDIATE'');'
from V\$SESSION
where status in ('INACTIVE', 'IDLE')
and LAST_CALL_ET > 86400
and username not in ('SYS', 'SYSTEM', 'DBSNMP')
order by last_call_et desc
/
spool off
spool prodrotw_killed.log
@$TMPKILL
exit
EOF

/usr/sbin/sendmail -t < $TMPFILE
fi
exit
