#
#
# ***** NOTE:  The server time is UTC. Please schedule the jobs according to this time
#
#

*/5 * * * * /home/oracle/plprod_cron_scripts/locks.sh PRODROTW 3
05 05 * * * /home/oracle/plprod_cron_scripts/clean_up_global_data.sh PRODROTW
#58 08 * * * /home/oracle/plprod_cron_scripts/clean_up_global_data.sh PRODROTW
29-59/30 0-3,12-23 * * * /home/oracle/plprod_cron_scripts/tx_monitor.sh PRODROTW

#Check blocking sessions every 5 minutes
*/5 * * * * /home/oracle/plprod_cron_scripts/check_db_blocks.sh PRODROTW

#Check high number of sessions sessions every 5 minutes
*/5 * * * * /home/oracle/plprod_cron_scripts/check_sessions_count.sh PRODROTW

#Top ten expensive queries
00 04 * * * /home/oracle/plprod_cron_scripts/top_ten_exp_queries.sh PRODROTW

#Top twenty objects in the database by size
00 05 * * 1 /home/oracle/plprod_cron_scripts/top_twenty_objects_by_size.sh PRODROTW

#Invalid objects in the database
00 04 * * * /home/oracle/plprod_cron_scripts/invalid_objects.sh PRODROTW

#Fragmented objects in the database
30 04 * * * /home/oracle/plprod_cron_scripts/fragmented_objects.sh PRODROTW

#Gather stats for selected tables in the database UTC TIME
30 05 * * 0-4,6 /home/oracle/plprod_cron_scripts/prodrotw_gather_stats_select_tables.sh PRODROTW

#Gather stats for selected schemas in the database UTC TIME
30 05 * * 5 /home/oracle/plprod_cron_scripts/prodrotw_gather_schema_stats.sh PRODROTW

# RMG - 12/18/2019 - Kill inactive sessions running > 24h
05 01 * * * /home/oracle/plprod_cron_scripts/kill_inactive_24h.sh PRODROTW

# RMG - 01/02/2020 - per Debbie/Sam Request
15 00 1 1 * /home/oracle/plprod_cron_scripts/reset_year.sh PRODROTW


# RMG - 01/13/2020 - refreshes of PMT tables transferred from a jboss server

00 09 * * * /home/oracle/plprod_cron_scripts/refresh_PROMOTION_UPDATED_MV.sh PRODROTW > /tmp/refresh_PROMOTION_UPDATED_MV.trc 2>&1
20 09 * * * /home/oracle/plprod_cron_scripts/refresh_PROMOTIONS_BY_STORE_MV.sh PRODROTW > /tmp/refresh_PROMOTIONS_BY_STORE_MV.trc 2>&1
40 09 * * * /home/oracle/plprod_cron_scripts/refresh_MISSING_PROMOTIONS_MV.sh PRODROTW > /tmp/refresh_MISSING_PROMOTIONS_MV.trc 2>&1

### DBREPOS scripts ### RMG 10/08/2019
#
#Remove DBREPOS archivelogs for now until backups are in place - RMG 10/01/2019
#### RMG ###00 * * * * /usr/bin/find /u01/app/oracle/fast_recovery_area -type f -mtime +1 -exec rm {} \; > /u01/app/oracle/admin/DBREPOS/logs/purge_arch.log 2>&1
00 02,22 * * * /home/oracle/dbrepos_cron_scripts/tblspc_check.sh
#
### BACKUPS
### INCREMENTAL LEVEL 0 - DAILY
00 22 * * * /home/oracle/dbrepos_cron_scripts/bkp/all_backup_db.sh > $HOME/ops/all_backup_db.log 2>&1
### ARCHIVELOGS - EVERY 4H EXCEPT DURING THE LEVEL 0 BACKUP
00 02,06,10,14,18 * * * /home/oracle/dbrepos_cron_scripts/bkp/all_backup_arch.sh > $HOME/ops/all_backup_arch.log 2>&1
#
### CLEAN UP OLD LOGS
35 23 * * * /usr/bin/find /u01/app/oracle/admin/DBREPOS/adump/ -type f -mtime +1 -exec rm {} \; > $HOME/ops/purge_audit.log 2>&1
45 23 * * * /usr/bin/find /u01/app/oracle/diag/rdbms/dbrepos/DBREPOS/trace/ -name "*.trc" -mtime +1 -exec rm {} \; > $HOME/ops/purge_trace_trc.log 2>&1
45 23 * * * /usr/bin/find /u01/app/oracle/diag/rdbms/dbrepos/DBREPOS/trace/ -name "*.trm" -mtime +1 -exec rm {} \; > $HOME/ops/purge_trace_trm.log 2>&1
55 23 * * * /usr/bin/find /dump/DBREPOS/rman/ ./ -name "*.log" -mtime +3 -exec /bin/rm -f {} \;
57 23 * * * /usr/bin/find /dump/DBREPOS/rman/ ./ -name "*.ctl" -mtime +2 -exec /bin/rm -f {} \;
