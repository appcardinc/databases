#!/bin/bash
#######################################################################################
# Purpose: This script track  sessions running for longer than 5 min and report them 
#
# Usage:   PG_long_run_5min.sh
#
#               no parameters to be passed
#
# Change log:
# mm/dd/yyyy    Name                     Description of the change.
#
#
########################################################################################

### set -e   exit running if any line fails
### set -u   Treat unset variables as an error when substituting

#To turn on tracing export the environment variable SCRIPT_TRACE=T
case ${SCRIPT_TRACE:-""} in
  T) set -x;;
esac
set -x

. $HOME/dba/scripts/PGENV.env

export SCRIPTDIR=$HOME/dba/scripts
export LOGDIR=$HOME/dba/logs
###export MAILLIST=ricardog@appcard.com
export MAILLIST=ricardog@appcard.com,chak@appcard.com,devops@appcard.com,eliyahu@appcard.com,amichay@appcard.com
export TMPFILE=$LOGDIR/long5.txt
export sdate=`date +"%Y-%m-%d %H:%M:%S"`
export PSQL=/usr/bin/psql

echo $PGHOST
echo $PGPORT
echo $PGDATABASE
echo $PGUSER

if [ -f $TMPFILE ]; then
 rm $TMPFILE 
fi

if [ -f $LOGDIR/pg_run_5min.html ]; then
 rm $LOGDIR/pg_run_5min.html
fi

$PSQL \
   -X \
   -x \
   -H \
   -o $LOGDIR/pg_run_5min.html \
   -c "SELECT pid, usename, client_addr, pg_stat_activity.application_name, pg_stat_activity.query_start, now() - pg_stat_activity.query_start AS duration, query, state FROM pg_stat_activity where now() - query_start > interval '5 minute' AND state = 'active' and usename NOT IN ('rdsadmin','reports')" \
  --field-separator ' ' \
  --quiet \

if [ -s $LOGDIR/pg_run_5min.html ]
 then
 cat $LOGDIR/pg_run_5min.html |grep usename
  if [ $? = 0 ]
   then
    echo "To: $MAILLIST" > $TMPFILE
    echo "From: appcard@appcard.com" >> $TMPFILE
    echo "Subject: PG - Queries running for longer than 5 min - $sdate" >> $TMPFILE 
    echo "MIME-Version: 1.0" >> $TMPFILE
    echo "Content-Type: text/html" >> $TMPFILE
    echo "Content-Disposition: inline" >> $TMPFILE
    cat $LOGDIR/pg_run_5min.html >>  $TMPFILE 
    /usr/sbin/sendmail -t < $TMPFILE
  else
    exit
  fi
fi
exit
