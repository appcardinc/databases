#!/bin/bash
#######################################################################################
# Purpose: This script track  sessions blocking other session and report them 
#
# Usage:   PG_locks.sh
#
#               no parameters to be passed
#
# Change log:
# mm/dd/yyyy    Name                     Description of the change.
#
#
########################################################################################
#To turn on tracing export the environment variable SCRIPT_TRACE=T
case ${SCRIPT_TRACE:-""} in
  T) set -x;;
esac
### set -e   exit running if any line fails
### set -u   Treat unset variables as an error when substituting
set -x

 . $HOME/dba/scripts/PGENV.env
export LOGDIR="$HOME/dba/logs"
export SCRIPTDIR="$HOME/dba/scripts"
###export MAILLIST=ricardog@appcard.com
export MAILLIST=ricardog@appcard.com,chak@appcard.com,devops@appcard.com,eliyahu@appcard.com,amichay@appcard.com,dev@appcard.com
export TMPFILE=$LOGDIR/locks.txt
export sdate=`date +"%Y-%m-%d %H:%M:%S"`
export PSQL=/usr/bin/psql

echo $PGHOST
echo $PGPORT
echo $PGDATABASE
echo $PGUSER

if [ -f $TMPFILE ]; then
 rm $TMPFILE 
fi

if [ -f $LOGDIR/pg_locks.html ]; then
 rm $LOGDIR/pg_locks.html
fi

if [ -f $LOGDIR/pg_locks2.html ]; then
 rm $LOGDIR/pg_locks2.html
fi


$PSQL \
   -X \
   -x \
   -H \
   -o $LOGDIR/pg_locks.html \
   -c "SELECT usename, pid, client_addr, application_name, wait_event_type, wait_event, query_start, pg_blocking_pids(pid) AS blocked_by, query FROM pg_stat_activity WHERE now() - query_start > interval '30 seconds' AND state <> 'idle' and wait_event <> 'ClientRead' and wait_event IS NOT NULL and wait_event_type='Lock';" \
  --field-separator ' ' \
  --quiet \

if [ -s $LOGDIR/pg_locks.html ]
 then
 cat $LOGDIR/pg_locks.html |grep usename
  if [ $? = 0 ]
   then
     $PSQL \
        -X \
        -x \
        -H \
        -o $LOGDIR/pg_locks2.html \
        -c "SELECT psa.pid, psa.usename, psa.client_addr,  psa.application_name, psa.query_start, psa.state, psa.query FROM pg_stat_activity psa, (SELECT DISTINCT unnest(pg_blocking_pids(pid)) AS blocking_pid FROM pg_stat_activity WHERE now() - query_start > interval '30 seconds' and state <> 'idle' and wait_event <> 'ClientRead' and wait_event IS NOT NULL and wait_event_type='Lock') bpids WHERE psa.pid = bpids.blocking_pid;" \
        --field-separator ' ' \
        --quiet \

    echo "To: $MAILLIST" > $TMPFILE
    echo "From: appcard@appcard.com" >> $TMPFILE
    echo "Subject: PG - Blocking locks - $sdate" >> $TMPFILE 
    echo "MIME-Version: 1.0" >> $TMPFILE
    echo "Content-Type: text/html" >> $TMPFILE
    echo "Content-Disposition: inline" >> $TMPFILE
    cat $LOGDIR/pg_locks.html >> $TMPFILE 
    echo "<br>" >> $TMPFILE
    echo "<br>" >> $TMPFILE
    echo "*****  DETAILS FROM BLOCKERS *****" >> $TMPFILE
    echo "<br>" >> $TMPFILE
    echo "<br>" >> $TMPFILE
    cat $LOGDIR/pg_locks2.html >> $TMPFILE
    /usr/sbin/sendmail -t < $TMPFILE 
   else
    exit
   fi
fi
exit
