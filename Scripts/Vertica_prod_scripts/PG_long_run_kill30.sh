#!/bin/bash
#######################################################################################
# Purpose: This script track  sessions running for longer than 30 min and kill them 
#
# Usage:   PG_long_run_kill30.sh
#
#               no parameters to be passed
#
# Change log:
# mm/dd/yyyy    Name                     Description of the change.
#
#
########################################################################################

### set -e   exit running if any line fails
### set -u   Treat unset variables as an error when substituting

#To turn on tracing export the environment variable SCRIPT_TRACE=T
###export SCRIPT_TRACE=T
case ${SCRIPT_TRACE:-""} in
  T) set -x;;
esac

. $HOME/dba/scripts/PGENV.env

export SCRIPTDIR="/home/vertica_prod/dba/scripts"
export LOGDIR="/home/vertica_prod/dba/logs"
export TMPFILE=$LOGDIR/longkill30.txt
###export MAILLIST=ricardog@appcard.com
###export MAILLIST=ricardog@appcard.com,chak@appcard.com
export MAILLIST=ricardog@appcard.com,chak@appcard.com,devops@appcard.com,eliyahu@appcard.com,amichay@appcard.com,dev@appcard.com
export sdate=`date +"%Y-%m-%d %H:%M:%S"`
export PSQL=/usr/bin/psql

echo $PGHOST
echo $PGPORT
echo $PGDATABASE
echo $PGUSER

if [ -f $SCRIPTDIR/pg_long_run_kill30.sql ]; then 
 rm $SCRIPTDIR/pg_long_run_kill30.sql
fi

if [ -f $LOGDIR/pg_long_run_kill30.html ]; then
 rm $LOGDIR/pg_long_run_kill30.html
fi

if [ -f $LOGDIR/pg_long_killed30.log ]; then
 rm $LOGDIR/pg_long_killed30.log
fi

if [ -f $TMPFILE ]; then
 rm $TMPFILE
fi

$PSQL \
    -X \
    -o $SCRIPTDIR/pg_long_run_kill30.sql \
    -c "SELECT 'SELECT pg_terminate_backend(' || pid || ');' FROM pg_stat_activity where now() - query_start > interval '30 minute' AND state = 'idle in transaction' " \
    --no-align \
    -t \
    --field-separator ' ' \
    --quiet \

if [ -s $SCRIPTDIR/pg_long_run_kill30.sql ]
then
  $PSQL \
   -X \
   -x \
   -H \
   -o $LOGDIR/pg_long_run_kill30.html \
   -c "SELECT pid, usename, client_addr, pg_stat_activity.application_name, pg_stat_activity.query_start, now() - pg_stat_activity.query_start AS duration, query, state FROM pg_stat_activity where now() - query_start > interval '30 minute' AND state = 'idle in transaction'" \
  --field-separator ' ' \
  --quiet \

cd $SCRIPTDIR

$PSQL -o $LOGDIR/pg_long_killed30.log -f 'pg_long_run_kill30.sql'

###echo "******************"
###echo "these sessions would be killed (idle in transaction)" > $LOGDIR/pg_long_killed30.log
###echo "******************"

 if [ -s $LOGDIR/pg_long_run_kill30.html ]
 then
 cat $LOGDIR/pg_long_run_kill30.html |grep usename
   if [ $? = 0 ]
    then
    echo "To: $MAILLIST" > $TMPFILE
    echo "From: appcard@appcard.com" >> $TMPFILE
    echo "Subject: PG - List of queries killed due to running longer than 30 min in idle in transaction status - $sdate" >> $TMPFILE
    echo "MIME-Version: 1.0" >> $TMPFILE
    echo "Content-Type: text/html" >> $TMPFILE
    echo "Content-Disposition: inline" >> $TMPFILE
    cat $LOGDIR/pg_long_run_kill30.html >> $TMPFILE
    /usr/sbin/sendmail -t < $TMPFILE
    else
   exit 
  fi
 else
  exit  
 fi
fi
exit
