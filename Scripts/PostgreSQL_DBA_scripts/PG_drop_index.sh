#!/bin/sh
#################################################################################
# Purpose: This script submit analyze to Postgres tables 
#
# Usage:   PG_drop_index.sh
#
#               no parameters to be passed
#
# Change log:
# mm/dd/yyyy    Name                     Description of the change.
#
#
#################################################################################
#To turn on tracing export the environment variable SCRIPT_TRACE=T
export SCRIPT_TRACE=T
case ${SCRIPT_TRACE:-""} in
  T) set -x;;
esac
. /home/ricardog/rmg/PGENV.env

echo $PGHOST
echo $PGPORT
echo $PGDATABASE
echo $PGUSER
echo $PGPASSWORD

maillist=ricardog@appcard.com

sdate=`date +"%m-%d-%Y"`

export start=`date +"%m-%d-%Y-%H:%M:%S"`
export stime=$(date +%s)

echo 'start:' $start

cd /home/ricardog/rmg

cat /home/ricardog/rmg/indexes.lst | while read IDX_NAME; do

echo $IDX_NAME

psql -o pg_drop_idx_$IDX_NAME.log -f 'statemidx.sql' -v index=$IDX_NAME &

done

export end=`date +"%m-%d-%Y-%H:%M:%S"`
export etime=$(date +%s)
export DIFF=$(( $etime - $stime ))

echo 'end_time:' $end
echo 'Indexes have been deleted'
echo 'elapsed:' $DIFF

exit
