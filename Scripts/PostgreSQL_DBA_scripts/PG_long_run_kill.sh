#!/bin/bash
#######################################################################################
# Purpose: This script track  sessions running for longer than 30 min and kill them 
#
# Usage:   pg_long_run_kill.sh
#
#               no parameters to be passed
#
# Change log:
# mm/dd/yyyy    Name                     Description of the change.
#
#
########################################################################################
#To turn on tracing export the environment variable SCRIPT_TRACE=T
export SCRIPT_TRACE=T
case ${SCRIPT_TRACE:-""} in
  T) set -x;;
esac
set -x
. /home/ricardog/rmg/PGENV.env

### set -e   exit running if any line fails
### set -u   Treat unset variables as an error when substituting 

echo $PGHOST
echo $PGPORT
echo $PGDATABASE
echo $PGUSER
echo $PGPASSWORD

export MAILLIST=ricardog@appcard.com
export TMPFILE=$LOGDIR/mail_head.txt

export sdate=`date +"%y-%m-%d %H:%M:%S"`

PSQL=/usr/bin/psql
export LOGDIR="/home/ricardog/rmg"

if [ -f $LOGDIR/pg_long_run_kill.sql ]; then 
 rm $LOGDIR/pg_long_run_kill.sql
fi

if [ -f $LOGDIR/pg_long_run_kill.log ]; then
 rm $LOGDIR/pg_long_run_kill.log
fi

if [ -f $LOGDIR/pg_long_killed.log ]; then
 rm $LOGDIR/pg_long_killed.log
fi

if [ -f $TMPFILE ]; then
 rm $TMPFILE
fi

if [ -f $LOGDIR/final_mail.txt ]; then
 rm $LOGDIR/final_mail.txt
fi

$PSQL \
    -X \
    -o $LOGDIR/pg_long_run_kill.sql \
    -c "SELECT 'SELECT pg_cancel_backend(' || pid || ');' FROM pg_stat_activity where now() - query_start > interval '30 minute' AND state = 'active' AND usename NOT IN ('appcard', 'rdsadmin')" \
    --no-align \
    -t \
    --field-separator ' ' \
    --quiet \

if [ -s $LOGDIR/pg_long_run_kill.sql ]
then
  $PSQL \
   -X \
   -x
   -o $LOGDIR/pg_long_run_kill.log \
   -c "SELECT pid, usename, now() - pg_stat_activity.query_start AS duration, query, state FROM pg_stat_activity where now() - query_start > interval '30 minute' AND state = 'active' and usename NOT IN ('appcard', 'rdsadmin')" \
  --field-separator ' ' \
  --quiet \

$PSQL -o $LOGDIR/pg_long_killed.log -f 'pg_long_run_kill.sql'

 echo "To: $MAILLIST" > $TMPFILE
 echo "From: $HOST " >> $TMPFILE
 echo "Subject: PG - List of queries killed due to running longer than 30 min - $sdate" >> $TMPFILE
 cat $TMPFILE pg_long_run_kill.log > $LOGDIR/final_mail.txt
 /usr/sbin/sendmail -t < $LOGDIR/final_mail.txt

fi
exit
