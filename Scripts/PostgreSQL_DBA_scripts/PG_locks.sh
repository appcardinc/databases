#!/bin/bash
#######################################################################################
# Purpose: This script track  sessions blocking other session and report them 
#
# Usage:   PG_locks.sh
#
#               no parameters to be passed
#
# Change log:
# mm/dd/yyyy    Name                     Description of the change.
#
#
########################################################################################
#To turn on tracing export the environment variable SCRIPT_TRACE=T
export SCRIPT_TRACE=T
case ${SCRIPT_TRACE:-""} in
  T) set -x;;
esac
set -x
### set -e   exit running if any line fails
### set -u   Treat unset variables as an error when substituting

. /home/ricardog/rmg/PGENV.env
export LOGDIR="/home/ricardog/rmg"
export MAILLIST=ricardog@appcard.com
###export MAILLIST=ricardog@appcard.com,chak@appcard.com
export TMPFILE=$LOGDIR/mail_head.txt
export sdate=`date +"%Y-%m-%d %H:%M:%S"`
export PSQL=/usr/bin/psql

echo $PGHOST
echo $PGPORT
echo $PGDATABASE
echo $PGUSER

if [ -f $TMPFILE ]; then
 rm $TMPFILE 
fi

if [ -f $LOGDIR/pg_locks.log ]; then
 rm $LOGDIR/pg_locks.log
fi

if [ -f $LOGDIR/final_mail.txt ]; then
 rm $LOGDIR/final_mail.txt
fi

$PSQL \
   -X \
   -x \
   -o $LOGDIR/pg_locks.log \
   -c "SELECT usename, pid, wait_event_type, wait_event, pg_blocking_pids(pid) AS blocked_by, query FROM pg_stat_activity WHERE state <> 'idle' and wait_event <> 'ClientRead' and wait_event IS NOT NULL;" \
  --field-separator ' ' \
  --quiet \

if [ -s $LOGDIR/pg_locks.log ]
 then
  cat $LOGDIR/pg_locks.log |grep "(0 rows)"
  if [ $? = 0 ]
    then exit
  else
    echo "To: $MAILLIST" > $TMPFILE
    echo "From: appcard-prod-sched1" >> $TMPFILE
    echo "Subject: PG - Blocking locks - $sdate" >> $TMPFILE 
    cat $TMPFILE pg_locks.log > $LOGDIR/final_mail.txt
    /usr/sbin/sendmail -t < $LOGDIR/final_mail.txt
  fi
fi
exit
