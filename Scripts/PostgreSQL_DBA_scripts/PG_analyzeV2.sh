#!/bin/sh
#################################################################################
# Purpose: This script submit analyze to Postgres tables

#
# Usage:   pg_analyze.sh
#
#               no parameters to be passed
#
# Change log:
# mm/dd/yyyy    Name                     Description of the change.
#
# Generate a list of tables without statistics:
# select relname from pg_stat_all_tables where schemaname = 'public' and last_analyze is null;
# OR
# generate a list of tables for the public schema, sorted by table name:
# select relname from pg_stat_all_tables where schemaname = 'public' order by relname;
#
# use this to create a list: psql -o public_tables.lst -f 'pub_tables.sql'
#
# add the above sql to pub_tables.sql
#
#################################################################################
#To turn on tracing export the environment variable SCRIPT_TRACE=T
export SCRIPT_TRACE=T
case ${SCRIPT_TRACE:-""} in
  T) set -x;;
esac
. /home/ricardog/rmg/PGENV_prod_rtdb_o.env

echo $PGHOST
echo $PGPORT
echo $PGDATABASE
echo $PGUSER
echo $PGPASSWORD

maillist=ricardog@appcard.com

sdate=`date +"%m-%d-%Y"`

export start=`date +"%m-%d-%Y-%H:%M:%S"`
echo 'Starting the analyzes:'
echo $start

cd /home/ricardog/rmg

cat /home/ricardog/rmg/tables_vac2.lst | while read TBL_NAME; do

echo $TBL_NAME

psql  -f 'statemV2.sql' -v table=$TBL_NAME > pg_$TBL_NAME.log  2>&1  &

done

export end=`date +"%m-%d-%Y-%H:%M:%S"`
export etime=$(date +%s)
export DIFF=$(( $stime - $etime ))

echo 'Analyzes have been submited'
echo $end

exit
