#!/bin/sh
###
###   RUN this script as ./PG_analyze_new_cluster.sh  or from crontab
###   
echo 'This script will generate minimal optimizer statistics rapidly'
echo 'so your system is usable, and then gather statistics twice more'
echo 'with increasing accuracy.  When it is done, your system will'
echo 'have the default level of optimizer statistics.'
echo
echo 'If you have used ALTER TABLE to modify the statistics target for'
echo 'any tables, you might want to remove them and restore them after'
echo 'running this script because they will delay fast statistics generation.'
echo
echo 'If you would like default statistics as quickly as possible, cancel'
echo 'this script and run:'
echo '  vacuumdb --all --analyze-only'
echo
#set -x

export start=`date +"%m-%d-%Y-%H:%M:%S"` 
export stime=$(date +%s)
echo 'starting the analyze:'
echo $start

vacuumdb -h dev1-rtdb.appcard.com -d AppCardDB -p 5432 -U rtdbmasteruser -W -j 20 --analyze-in-stages --verbose 

export end=`date +"%m-%d-%Y-%H:%M:%S"` 
export etime=$(date +%s)   
export DIFF=$(( $stime - $etime ))

echo 'Execution time:'
echo $DIFF
echo 'Done'
exit
