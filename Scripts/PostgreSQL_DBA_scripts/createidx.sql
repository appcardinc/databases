CREATE INDEX CONCURRENTLY identities_iden_name_type_idx
ON public.identities USING btree
(identity_name) TABLESPACE pg_default WHERE (identity_type = 'card' );

CREATE INDEX CONCURRENTLY merchants_merch_tz_id_idx 
ON public.merchants USING btree 
(merchant_id, tz_id) TABLESPACE pg_default;

\q
