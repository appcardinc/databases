#!/bin/bash
#######################################################################################
# Purpose: This script track  sessions blocking other session and report them 
#
# Usage:   PG_missing_idx.sh
#
#               no parameters to be passed
#
# Change log:
# mm/dd/yyyy    Name                     Description of the change.
#
#
########################################################################################
#To turn on tracing export the environment variable SCRIPT_TRACE=T
case ${SCRIPT_TRACE:-""} in
  T) set -x;;
esac
### set -e   exit running if any line fails
### set -u   Treat unset variables as an error when substituting
set -x

 . $HOME/rmg/scripts/PGENV.env
export LOGDIR="$HOME/rmg/logs"
export SCRIPTDIR="$HOME/rmg/scripts"
export MAILLIST=ricardog@appcard.com
###export MAILLIST=ricardog@appcard.com,chak@appcard.com,devops@appcard.com,eliyahu@appcard.com,amichay@appcard.com,dev@appcard.com
export TMPFILE=$LOGDIR/miss_idx.txt
export sdate=`date +"%Y-%m-%d %H:%M:%S"`
export PSQL=/usr/bin/psql

echo $PGHOST
echo $PGPORT
echo $PGDATABASE
echo $PGUSER

if [ -f $TMPFILE ]; then
 rm $TMPFILE 
fi

if [ -f $LOGDIR/pg_miss_idx.html ]; then
 rm $LOGDIR/pg_miss_idx.html
fi

$PSQL \
   -X \
   -x \
   -H \
   -o $LOGDIR/pg_miss_idx.html \
   -c "select relname, seq_scan, idx_scan, seq_scan - idx_scan as too_much_seq, CASE WHEN seq_scan - coalesce(idx_scan,0) > 0 THEN 'Missing Index' ELSE 'OK' END as miss_idx, pg_relation_size(relname::regclass) as rel_size,seq_scan,idx_scan from pg_stat_all_tables where schemaname='public' and pg_relation_size(relname::regclass) > 80000 order by miss_idx,too_much_seq desc;" \
  --field-separator ' ' \
  --quiet \

if [ -s $LOGDIR/pg_miss_idx.html ]
 then
 cat $LOGDIR/pg_miss_idx.html |grep Missing
  if [ $? = 0 ]
   then
    echo "To: $MAILLIST" > $TMPFILE
    echo "From: appcard@appcard.com" >> $TMPFILE
    echo "Subject: PG - Tables missing indexes under public in $PGHOST - $sdate" >> $TMPFILE
    echo "MIME-Version: 1.0" >> $TMPFILE
    echo "Content-Type: text/html" >> $TMPFILE
    echo "Content-Disposition: inline" >> $TMPFILE
    cat $LOGDIR/pg_miss_idx.html >> $TMPFILE 
    /usr/sbin/sendmail -t < $TMPFILE 
   else
    exit
   fi
fi
exit
