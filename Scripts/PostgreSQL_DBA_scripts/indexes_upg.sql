CREATE INDEX CONCURRENTLY identities_user_exp_idx
ON public.identities USING btree
(user_id) TABLESPACE pg_default WHERE expire_date IS NULL;

CREATE INDEX CONCURRENTLY communication_state_iden_state_idx 
ON public.communication_state  USING btree 
(identity_id) TABLESPACE pg_default where (state ->> 'admin_optin')::BOOLEAN is FALSE;

DROP INDEX CONCURRENTLY identities_id_idx;

analyze my_merchants;

analyze identities;

analyze communication_state;

