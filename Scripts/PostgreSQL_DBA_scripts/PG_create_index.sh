#!/bin/sh
#################################################################################
# Purpose: This script submit analyze to Postgres tables 
#
# Usage:   PG_create_index.sh
#
#               no parameters to be passed
#
# Change log:
# mm/dd/yyyy    Name                     Description of the change.
#
#
#################################################################################
#To turn on tracing export the environment variable SCRIPT_TRACE=T
export SCRIPT_TRACE=T
case ${SCRIPT_TRACE:-""} in
  T) set -x;;
esac
. /home/ricardog/rmg/PGENV.env

echo $PGHOST
echo $PGPORT
echo $PGDATABASE
echo $PGUSER
echo $PGPASSWORD

maillist=ricardog@appcard.com

sdate=`date +"%m-%d-%Y"`

export start=`date +"%m-%d-%Y-%H:%M:%S"`
export stime=$(date +%s)

echo 'start:' $start

cd /home/ricardog/rmg

psql -o pg_create_idx.log -f 'createidx.sql' 

export end=`date +"%m-%d-%Y-%H:%M:%S"`
export etime=$(date +%s)
export DIFF=$(( $etime - $stime ))

echo 'end_time:' $end
echo 'Indexes have been created'
echo 'elapsed:' $DIFF

exit
