#!/bin/bash
#######################################################################################
# Purpose: This script reports the Top 10 higher consumers queries retrieved from the pg_stat_statements system view 
#
# Usage:   PG_top_10_queries.sh
#
#               no parameters to be passed
#
# Change log:
# mm/dd/yyyy    Name                     Description of the change.
#
#
########################################################################################
#To turn on tracing export the environment variable SCRIPT_TRACE=T
case ${SCRIPT_TRACE:-""} in
  T) set -x;;
esac
### set -e   exit running if any line fails
### set -u   Treat unset variables as an error when substituting
set -x

 . $HOME/rmg/scripts/PGENV.env
export LOGDIR="$HOME/rmg/logs"
export SCRIPTDIR="$HOME/rmg/scripts"
export MAILLIST=ricardog@appcard.com
###export MAILLIST=ricardog@appcard.com,chak@appcard.com,devops@appcard.com,eliyahu@appcard.com,amichay@appcard.com,dev@appcard.com
export TMPFILE=$LOGDIR/top10.txt
export sdate=`date +"%Y-%m-%d %H:%M:%S"`
export PSQL=/usr/bin/psql

echo $PGHOST
echo $PGPORT
echo $PGDATABASE
echo $PGUSER

if [ -f $TMPFILE ]; then
 rm $TMPFILE 
fi

if [ -f $LOGDIR/pg_top10.html ]; then
 rm $LOGDIR/pg_top10.html
fi

$PSQL \
   -X \
   -x \
   -H \
   -o $LOGDIR/pg_top10.html \
   -c "SELECT userid, queryid, query, total_time, min_time, max_time, calls FROM pg_stat_statements ORDER BY total_time DESC LIMIT 10; " \
  --field-separator ' ' \
  --quiet \

if [ -s $LOGDIR/pg_top10.html ]
 then
 cat $LOGDIR/pg_top10.html |grep userid
  if [ $? = 0 ]
   then
    echo "To: $MAILLIST" > $TMPFILE
    echo "From: appcard@appcard.com" >> $TMPFILE
    echo "Subject: PG - Top 10 Queries - $sdate" >> $TMPFILE
    echo "MIME-Version: 1.0" >> $TMPFILE
    echo "Content-Type: text/html" >> $TMPFILE
    echo "Content-Disposition: inline" >> $TMPFILE
    cat $LOGDIR/pg_top10.html >> $TMPFILE 
    /usr/sbin/sendmail -t < $TMPFILE 
   else
    exit
   fi
fi
exit
