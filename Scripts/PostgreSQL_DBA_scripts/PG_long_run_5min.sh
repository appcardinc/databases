#!/bin/bash
#######################################################################################
# Purpose: This script track  sessions running for longer than 5 min and report them 
#
# Usage:   PG_long_run_5min.sh
#
#               no parameters to be passed
#
# Change log:
# mm/dd/yyyy    Name                     Description of the change.
#
#
########################################################################################
#To turn on tracing export the environment variable SCRIPT_TRACE=T
export SCRIPT_TRACE=T
case ${SCRIPT_TRACE:-""} in
  T) set -x;;
esac
set -x
. /home/ricardog/rmg/PGENV.env

### set -e   exit running if any line fails
### set -u   Treat unset variables as an error when substituting 

echo $PGHOST
echo $PGPORT
echo $PGDATABASE
echo $PGUSER
echo $PGPASSWORD

export LOGDIR="/home/ricardog/rmg"
export MAILLIST=ricardog@appcard.com
export TMPFILE=$LOGDIR/mail_head.txt 
export sdate=`date +"%Y-%m-%d %H:%M:%S"`

PSQL=/usr/bin/psql

if [ -f $TMPFILE ]; then
 rm $TMPFILE 
fi

if [ -f $LOGDIR/pg_run_5min.log ]; then
 rm $LOGDIR/pg_run_5min.log
fi

if [ -f $LOGDIR/final_mail.txt ]; then
 rm $LOGDIR/final_mail.txt
fi

$PSQL \
   -X \
   -x \
   -o $LOGDIR/pg_run_5min.log \
   -c "SELECT pid, usename, now() - pg_stat_activity.query_start AS duration, query, state FROM pg_stat_activity where now() - query_start > interval '5 minute' AND state = 'active' and usename NOT IN ('rdsadmin')" \
  --field-separator ' ' \
  --quiet \

if [ -s $LOGDIR/pg_run_5min.log ]
 then
  cat $LOGDIR/pg_run_5min.log |grep "(0 rows)"
  if [ $? = 0 ]
    then exit
  else
    echo "To: $MAILLIST" > $TMPFILE
    echo "From: appcard-prod-sched1" >> $TMPFILE
    echo "Subject: PG - Queries running for longer than 5 min - $sdate" >> $TMPFILE 
    cat $TMPFILE pg_run_5min.log > $LOGDIR/final_mail.txt
    /usr/sbin/sendmail -t < $LOGDIR/final_mail.txt
  fi
fi
exit
