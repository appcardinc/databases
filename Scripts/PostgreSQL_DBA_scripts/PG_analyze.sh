#!/bin/sh
#################################################################################
# Purpose: This script submit analyze to Postgres tables 
#
# Usage:   pg_analyze.sh
#
#               no parameters to be passed
#
# Change log:
# mm/dd/yyyy    Name                     Description of the change.
#
#
#################################################################################
#To turn on tracing export the environment variable SCRIPT_TRACE=T
export SCRIPT_TRACE=T
case ${SCRIPT_TRACE:-""} in
  T) set -x;;
esac
. /home/ricardog/rmg/PGENV_dbmon.env

echo $PGHOST
echo $PGPORT
echo $PGDATABASE
echo $PGUSER
echo $PGPASSWORD

maillist=ricardog@appcard.com

sdate=`date +"%m-%d-%Y"`

export start=`date +"%m-%d-%Y-%H:%M:%S"`
echo 'Starting the analyzes:'
echo $start

cat /home/ricardog/rmg/prod_monit.lst | while read TBL_NAME; do

echo $TBL_NAME

psql -o pg_analyze_$TBL_NAME.log -f 'statem.sql' -v table=$TBL_NAME &

done

export end=`date +"%m-%d-%Y-%H:%M:%S"`
export etime=$(date +%s)
export DIFF=$(( $stime - $etime ))

echo 'Analyzies have been submited'
echo $end

exit
