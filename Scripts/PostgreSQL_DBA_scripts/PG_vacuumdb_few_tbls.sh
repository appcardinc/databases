#!/bin/sh
###
###   RUN this script as ./PG_analyze_new_cluster.sh  or from crontab
###
echo 'This script will generate minimal optimizer statistics rapidly'
echo 'so your system is usable, and then gather statistics twice more'
echo 'with increasing accuracy.  When it is done, your system will'
echo 'have the default level of optimizer statistics.'
echo
echo 'If you have used ALTER TABLE to modify the statistics target for'
echo 'any tables, you might want to remove them and restore them after'
echo 'running this script because they will delay fast statistics generation.'
echo
echo 'If you would like default statistics as quickly as possible, cancel'
echo 'this script and run:'
echo '  vacuumdb --all --analyze-only'
echo
#set -x

export start=`date +"%m-%d-%Y-%H:%M:%S"`
export stime=$(date +%s)
echo 'Starting the analyzes:'
echo $start

vacuumdb -h prod-rtdb-test.c40ku8gr3zsv.us-east-1.rds.amazonaws.com -d AppCardDB -p 5432 -U rtdbmasteruser -W -j 20 --analyze-only -t shopping_history -t offers_history -t benefit_transactions -t identities -t  device_raw_data_690000000_700000000 -t receipts_320000000_330000000 -t  shopping_history_items_201910 -t shopping_history_items_201911 -t shopping_history_items_201912  --verbose

export end=`date +"%m-%d-%Y-%H:%M:%S"`
export etime=$(date +%s)
export DIFF=$(( $stime - $etime ))

echo 'Completed the analyzes'
echo $etime
echo 'Execution time:'
echo $DIFF
echo 'Done'
exit
