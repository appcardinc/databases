--Delete from cds.global_data, - 629,645 records

DECLARE
	l_client_id NUMBER := 1619;
	l_commit_count NUMBER := 0;
	l_commit_limit NUMBER := 1000;
	l_scope_id NUMBER := 3;

	TYPE t_array IS TABLE OF VARCHAR(20);
	id_array t_array;

	CURSOR global_data_cur IS
		SELECT DISTINCT gd.new_entity_id FROM cds.global_data gd 
		  JOIN cds.entity ec ON ec.entity_id = gd.new_entity_id and ec.scope = l_scope_id 
		  JOIN cds.consumer c ON c.consumer_id = ec.scope_entity_id and c.enrolled_client_id = l_client_id;

BEGIN
	OPEN global_data_cur;
		FETCH global_data_cur BULK COLLECT INTO id_array;
	CLOSE global_data_cur;

	IF id_array.COUNT > 0 THEN
		FOR rec IN id_array.FIRST .. id_array.LAST
		LOOP
			DELETE FROM cds.global_data gd
			 WHERE gd.new_entity_id = id_array(rec);
		
			l_commit_count := l_commit_count + SQL%ROWCOUNT;

			IF l_commit_count >= l_commit_limit THEN
				COMMIT;
				l_commit_count := 0;
			END IF;
		END LOOP;
		COMMIT;
	END IF;
END;
/
