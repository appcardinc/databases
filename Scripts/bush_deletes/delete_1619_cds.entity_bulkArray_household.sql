--Delete from cds.entity for consumers, - 1,764,933 records

DECLARE
	l_client_id NUMBER := 1619;
	l_commit_count NUMBER := 0;
	l_commit_limit NUMBER := 1000;
	l_scope_id NUMBER := 11;

	TYPE t_array IS TABLE OF VARCHAR(20);
	id_array t_array;

--NOTE: THERE IS A DEFECT IN BELOW SELECT (COPY PASTE ERROR). THE JOIN SHOULD BE ON THE C.HOUSEHOLD_ID NOT THE C.CONSUMER_ID!!!!
--THIS CAUSED THE INCORRECT ENTITY RECORDS TO BE DELETED AND WE HAD TO RECREATE THEM. BELOW I'VE COMMENTED OUT THE INCORRECT
--SELECT AND ADDED THE CORRECT SELECT!
--	CURSOR entity_cur IS
--		SELECT DISTINCT eh.entity_id FROM cds.entity eh 
--		  JOIN cds.consumer c ON c.consumer_id = eh.scope_entity_id and c.enrolled_client_id = l_client_id
--		 WHERE eh.scope = l_scope_id;
	CURSOR entity_cur IS
		SELECT DISTINCT eh.entity_id FROM cds.entity eh 
		  JOIN cds.consumer c ON c.household_id = eh.scope_entity_id and c.enrolled_client_id = l_client_id
		 WHERE eh.scope = l_scope_id;

BEGIN
	OPEN entity_cur;
		FETCH entity_cur BULK COLLECT INTO id_array;
	CLOSE entity_cur;

	IF id_array.COUNT > 0 THEN
		FOR rec IN id_array.FIRST .. id_array.LAST
		LOOP
			DELETE FROM cds.entity eh
			 WHERE eh.entity_id = id_array(rec);
		
			l_commit_count := l_commit_count + SQL%ROWCOUNT;

			IF l_commit_count >= l_commit_limit THEN
				COMMIT;
				l_commit_count := 0;
			END IF;
		END LOOP;
		COMMIT;
	END IF;
END;
/
