-- Tlog.tx_data count for Key Foods = 115966
--Note: Any records whose created date is lt the sysdate date will be deleted. To restrict the number of records deleted, simply change the date being passed in
BEGIN 
	tlog.tlog_utilities.clear_tlogs_by_client_fast(1619, SYSDATE); 
END;
/
