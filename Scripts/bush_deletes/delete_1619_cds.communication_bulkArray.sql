--Delete from cds.communication, - 1762576 records
DECLARE
	l_client_id NUMBER := 1619;
	l_commit_count NUMBER := 0;
	l_commit_limit NUMBER := 1000;

	TYPE t_array IS TABLE OF VARCHAR(20);
	id_array t_array;

	CURSOR communication_cur IS
		SELECT DISTINCT comm.consumer_id FROM cds.communication comm 
		  JOIN cds.consumer c ON c.consumer_id = comm.consumer_id AND c.enrolled_client_id = l_client_id;

BEGIN
	OPEN communication_cur;
		FETCH communication_cur BULK COLLECT INTO id_array;
	CLOSE communication_cur;

	IF id_array.COUNT > 0 THEN
		FOR rec IN id_array.FIRST .. id_array.LAST
		LOOP
			DELETE FROM cds.communication comm
			 WHERE comm.consumer_id = id_array(rec);
		
			l_commit_count := l_commit_count + SQL%ROWCOUNT;

			IF l_commit_count >= l_commit_limit THEN
				COMMIT;
				l_commit_count := 0;
			END IF;
		END LOOP;
		COMMIT;
	END IF;
END;
/
