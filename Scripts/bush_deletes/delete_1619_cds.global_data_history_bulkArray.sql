--NOTE: BE SURE INDEXES ARE CREATED FIRST!!!!
--Delete from cds.global_data_history, - 2,530,394 records

DECLARE
	l_client_id NUMBER := 1619;
	l_commit_count NUMBER := 0;
	l_commit_limit NUMBER := 1000;

	TYPE t_array IS TABLE OF VARCHAR(20);
	id_array t_array;

	CURSOR global_data_history_cur IS
		SELECT DISTINCT gdh.consumer_id FROM cds.global_data_history gdh
		 WHERE client_id = l_client_id;

BEGIN
	OPEN global_data_history_cur;
		FETCH global_data_history_cur BULK COLLECT INTO id_array;
	CLOSE global_data_history_cur;

	IF id_array.COUNT > 0 THEN
		FOR rec IN id_array.FIRST .. id_array.LAST
		LOOP
			DELETE FROM cds.global_data_history gdh
			 WHERE gdh.consumer_id = id_array(rec);
		
			l_commit_count := l_commit_count + SQL%ROWCOUNT;

			IF l_commit_count >= l_commit_limit THEN
				COMMIT;
				l_commit_count := 0;
			END IF;
		END LOOP;
		COMMIT;
	END IF;
END;
/
