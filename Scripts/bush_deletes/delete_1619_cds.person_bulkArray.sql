--Delete from cds.person, - 1,442,894 records

DECLARE
	l_client_id NUMBER := 1619;
	l_counter NUMBER := 0;
	l_counter_audit NUMBER := 0;
	person_array cds.T_NUMBER_ARRAY;
	l_limit NUMBER := 6000;

BEGIN
	-- collect person into array
	SELECT p.person_id
	  BULK COLLECT INTO person_array
	  FROM cds.person p
	  JOIN cds.consumer c on c.person_id = p.person_id and c.enrolled_client_id = l_client_id;
	
	IF person_array.COUNT > 0 THEN
	  -- delete from person
	  LOOP
		DELETE FROM cds.person 
		 WHERE person_id IN (SELECT COLUMN_VALUE 
						FROM TABLE(CAST(person_array AS cds.T_NUMBER_ARRAY))
					  )
		   AND ROWNUM < l_limit;
		l_counter := l_counter + SQL%ROWCOUNT;
		EXIT WHEN SQL%ROWCOUNT < l_limit - 1;
		COMMIT;

	  END LOOP;
	  COMMIT;
	
	END IF;
END;
/
