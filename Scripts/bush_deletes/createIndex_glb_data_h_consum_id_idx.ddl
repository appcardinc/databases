  CREATE INDEX "CDS"."GLB_DATA_H_CONSUM_ID_IDX" ON "CDS"."GLOBAL_DATA_HISTORY" ("CONSUMER_ID") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT)
  TABLESPACE "CDS_IDX" ;
