--Delete from cds.household, - 1,764,933 records

DECLARE
	l_client_id NUMBER := 1619;
	l_counter NUMBER := 0;
	l_counter_audit NUMBER := 0;
	household_array cds.T_NUMBER_ARRAY;
	l_limit NUMBER := 6000;

BEGIN
	-- collect household into array
	SELECT household_id
	  BULK COLLECT INTO household_array
	  FROM cds.household
	  WHERE enrolled_client_id = l_client_id;
	
	IF household_array.COUNT > 0 THEN
	  -- delete from household
	  LOOP
		DELETE FROM cds.household 
		 WHERE household_id IN (SELECT COLUMN_VALUE 
						FROM TABLE(CAST(household_array AS cds.T_NUMBER_ARRAY))
					  )
		   AND ROWNUM < l_limit;
		l_counter := l_counter + SQL%ROWCOUNT;
		EXIT WHEN SQL%ROWCOUNT < l_limit - 1;
		COMMIT;

	  END LOOP;
	  COMMIT;
	
	END IF;
END;
/
