--Delete from cds.consumer, - 1,764,933 records

DECLARE
	l_client_id NUMBER := 1619;
	l_counter NUMBER := 0;
	l_counter_audit NUMBER := 0;
	consumer_array cds.T_NUMBER_ARRAY;
	l_limit NUMBER := 6000;

BEGIN
	-- collect consumer into array
	SELECT c.consumer_id
	  BULK COLLECT INTO consumer_array
	  FROM cds.consumer c
	 WHERE c.enrolled_client_id = l_client_id;
	
	IF consumer_array.COUNT > 0 THEN
	  -- delete from consumer
	  LOOP
		DELETE FROM cds.consumer 
		 WHERE consumer_id IN (SELECT COLUMN_VALUE 
						FROM TABLE(CAST(consumer_array AS cds.T_NUMBER_ARRAY))
					  )
		   AND ROWNUM < l_limit;
		l_counter := l_counter + SQL%ROWCOUNT;
		EXIT WHEN SQL%ROWCOUNT < l_limit - 1;
		COMMIT;
	  END LOOP;
	  COMMIT;
	
	END IF;
END;
/
