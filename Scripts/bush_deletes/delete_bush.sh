#!/bin/bash
#################################################################
# Script - delete_bush.sh                                       #
# Delete all data for a customer from the database              #
# Owner - DBA - Appcard                                         #
# Date - 05/07/2020                                             #
#################################################################
set -x
ORACLE_HOME=/u01/app/oracle/product/12.1.0/dbhome_1
TNS_ADMIN=$ORACLE_HOME/network/admin
ORACLE_SID=PRODROTS
export ORACLE_HOME ORACLE_SID TNS_ADMIN
ORAENV_ASK=NO
###LD_LIBRARY_PATH=/usr/lib/oracle/12.2/client64/lib
PATH=$PATH:$ORACLE_HOME/bin
export PATH LD_LIBRARY_PATH PATH ORAENV_ASK

if [ "$#" -ne 1 ]; then
  echo "Usage: $0 <ORACLE_SID>" 
  exit 1
fi

export ORACLE_SID=$1

export ENCPWD=`cat /home/oracle/plprod_cron_scripts/encrypted_passwd | openssl enc -base64 -d`
MAILLIST="ricardog@appcard.com"
###MAILLIST="smorales@appcard.com,ricardog@appcard.com,dlocke@appcard.com"
HOST="$HOSTNAME"

sqlplus "appcard_master/$ENCPWD@$ORACLE_SID" << EOF

set echo on
set verify off
set time on
set timing on
set trimout on
set trimspool on 
set pagesize 1200
set serveroutput on
set termout on

spool /tmp/PRODROTW_bush_delete_out.tmp

@delete_1619_cds.loyalty_card_validation.sql
@delete_1619_cds.comm_opt_out_bulkArray.sql
@delete_1619_cds.communication_bulkArray.sql
@delete_1619_cds.global_data_history_bulkArray.sql
@delete_1619_cds.global_data_bulkArray_consumer.sql
@delete_1619_cds.global_data_bulkArray_household.sql
@delete_1619_cds.entity_bulkArray_consumer.sql
@delete_1619_cds.entity_bulkArray_household.sql
@delete_1619_cds.loyalty_history_procedureCall.sql
@delete_1619_cds.consumer_bulkArray.sql
@delete_1619_cds.person_bulkArray.sql
@delete_1619_cds.household_bulkArray.sql
@delete_1619_tlog_data_procedureCall.sql

spool off

exit
EOF

cat /tmp/PRODROTW_bush_delete_out.tmp | mail -s "Delete Customer data for Bush from Database: $ORACLE_SID" $MAILLIST

exit
