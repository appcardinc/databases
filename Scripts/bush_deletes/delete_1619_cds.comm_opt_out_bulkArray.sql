--Delete from cds.comm_opt_out, - 516,583 records
DECLARE
	l_client_id NUMBER := 1619;
	l_commit_count NUMBER := 0;
	l_commit_limit NUMBER := 1000;

	TYPE t_array IS TABLE OF VARCHAR(20);
	id_array t_array;

	CURSOR comm_opt_out_cur IS
		SELECT DISTINCT o.consumer_id FROM cds.comm_opt_out o 
		  JOIN cds.consumer c ON c.consumer_id = o.consumer_id AND c.enrolled_client_id = l_client_id;

BEGIN
	OPEN comm_opt_out_cur;
		FETCH comm_opt_out_cur BULK COLLECT INTO id_array;
	CLOSE comm_opt_out_cur;

	IF id_array.COUNT > 0 THEN
		FOR rec IN id_array.FIRST .. id_array.LAST
		LOOP
			DELETE FROM cds.comm_opt_out o
			 WHERE o.consumer_id = id_array(rec);

			l_commit_count := l_commit_count + SQL%ROWCOUNT;

			IF l_commit_count >= l_commit_limit THEN
				COMMIT;
				l_commit_count := 0;
			END IF;
		END LOOP;
		COMMIT;
	END IF;
END;
/
