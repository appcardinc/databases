--Delete from cds.entity for consumers, - 1,764,933 records

DECLARE
	l_client_id NUMBER := 1619;
	l_commit_count NUMBER := 0;
	l_commit_limit NUMBER := 1000;
	l_scope_id NUMBER := 3;

	TYPE t_array IS TABLE OF VARCHAR(20);
	id_array t_array;

	CURSOR entity_cur IS
		SELECT DISTINCT ec.entity_id FROM cds.entity ec 
		  JOIN cds.consumer c ON c.consumer_id = ec.scope_entity_id and c.enrolled_client_id = l_client_id
		 WHERE ec.scope = l_scope_id;

BEGIN
	OPEN entity_cur;
		FETCH entity_cur BULK COLLECT INTO id_array;
	CLOSE entity_cur;

	IF id_array.COUNT > 0 THEN
		FOR rec IN id_array.FIRST .. id_array.LAST
		LOOP
			DELETE FROM cds.entity ec
			 WHERE ec.entity_id = id_array(rec);
		
			l_commit_count := l_commit_count + SQL%ROWCOUNT;

			IF l_commit_count >= l_commit_limit THEN
				COMMIT;
				l_commit_count := 0;
			END IF;
		END LOOP;
		COMMIT;
	END IF;
END;
/
