#!/bin/sh

##############################################################################################################
#
# Name:
#          GLOBAL_DATA Physical Deletes Process
#
# Purpose: 
#          The GLOBAL_DATA Physical Deletes Process gets set of records to be deleted according 
#          to defined criteria and deletes corresponding records 
#          from the table CDS.GLOBAL_DATA on Oracle JGS Production DB.
#          The GLOBAL_DATA Physical Deletes can run explicitly or can be scheduled via scheduler.
#
# Description:
#          The GLOBAL_DATA Physical Deletes Process gets set of records to be deleted
#          according to defined criteria and deletes corresponding records
#          from the table CDS.GLOBAL_DATA on Oracle JGS Production DB.
#
#          On each its run the script checks that there is no any other instances running. 
#
#          On each its run the script is checking that there is no any blocking locks 
#          on the database.
#
#          On each its run script sends start message, end message and run trace details by email.
#
#          The script has a number of internal parameters that help to control its flow:
#
#           MAILING_LIST - a list of email recepients who gets mails messages originated by this script
#           MAX_RECS_TO_PROCESS - maximum records to process by its single run
#           COMMIT_EVERY_N_RECS - how many records processed to commit at once
#           MAX_RUNTIME_SEC - maximum time in seconds for the script to process data
#           MAX_LOCKED_OBJECTS - maximum number of blocking locks on the DB allowed for the process to run
#           GRACE_PERIOD_GLOBAL_DATA_DAYS - grace period to keep logically deleted recs in the table GLOBAL_DATA
# 
# Version:
#          1.0 
#
# Usage: 
#          new_gd_physical_deletes.sh
#
# Date: 
#          18-Jan-2018
#
##############################################################################################################

#
# Set up the env
ORACLE_BASE=/app/oracle
ORACLE_HOME=/app/oracle/product/10.2.0/orajgs01
ORACLE_SID=orajgs01
export ORACLE_BASE ORACLE_HOME ORACLE_SID
ORAENV_ASK=NO
OA_PRD=$ORACLE_BASE/admin/orajgs01
LD_LIBRARY_PATH=$ORACLE_HOME/lib:/usr/lib
PATH=$PATH:$ORACLE_HOME/bin:/usr/sbin
export OA_PRD LD_LIBRARY_PATH PATH ORAENV_ASK
export REPORT=/tmp/new_gd_physical_deletes_$ORACLE_SID.rpt
export MAILING_LIST="dmitryr@appcard.com edo@appcard.com syamaguchi@appcard.com emoto@greenstamp.co.jp jb-support-nw@greenstamp.co.jp ricardog@appcard.com"
#export MAILING_LIST="dmitryr@appcard.com"
export MAX_RECS_TO_PROCESS=200000
export COMMIT_EVERY_N_RECS=200
export MAX_RUNTIME_SEC=3000
export MAX_LOCKED_OBJECTS=3
export GRACE_PERIOD_GLOBAL_DATA_DAYS=1

nOfRunningThisScript=`ps -eaf | grep new_gd_physical_deletes.sh | grep -v grep | grep -v $$ | wc -l`
echo $nOfRunningThisScript

if [ "$nOfRunningThisScript" -ge 2 ]; then
	echo "Too many instances of running process."
        exit
else
	echo "Start the process."
fi

echo "Oracle JGS: GD Physical Deletes Started :: $(date)" | mail -s "Oracle JGS: GD Physical Deletes Started :: $(date)" $MAILING_LIST

$ORACLE_HOME/bin/sqlplus -s /nolog <<EOF
connect / as sysdba

SET SERVEROUT ON
set head off
set feedback off
set lines 100

spo $REPORT

DECLARE

  type t_rec is record
  (
    var_id number,
    gd_id number,
    row_id rowid
  );

  type t_tab is table of t_rec index by pls_integer;
  recs_to_process t_tab;
  v_locked_objects number;

  C1 BINARY_INTEGER; C2 BINARY_INTEGER; C3 BINARY_INTEGER;
  t1 BINARY_INTEGER; t2 BINARY_INTEGER; t3 BINARY_INTEGER; t4 BINARY_INTEGER;
BEGIN
  t1 := dbms_utility.get_time;

  SELECT /*+ parallel(gd, 4) */
	gdd.variable_id,
	gd.id,
        gd.rowid
  BULK COLLECT INTO 
	recs_to_process
  FROM 
	common.global_data_definition gdd, 
        cds.global_data gd
  WHERE 
        gdd.variable_id = gd.variable_id 
  AND 
        nvl(gd.deleted,TIMESTAMP'2100-01-01 00:00:00') < sysdate
  AND 
        gdd.deleted >=  (SYSDATE - 60)      
  AND 
        gdd.deleted < (SYSDATE - $GRACE_PERIOD_GLOBAL_DATA_DAYS); 

  C1:= SQL%ROWCOUNT; -- Num Records To Process

  dbms_output.put_line('>');

  dbms_output.put_line('Run Parameters:');

  dbms_output.put_line('Maximum Records To Process '|| $MAX_RECS_TO_PROCESS || '.');

  dbms_output.put_line('Grace Period To Keep Logically Deleted Records In The Table GLOBAL_DATA '|| $GRACE_PERIOD_GLOBAL_DATA_DAYS || ' Day(s).');

  dbms_output.put_line('Do Commit Every '|| $COMMIT_EVERY_N_RECS || ' Records.');

  dbms_output.put_line('Maximum Runtime ' || $MAX_RUNTIME_SEC || ' Seconds.');

  dbms_output.put_line('Num GLOBAL_DATA Records Waiting To Be Processed '|| C1 || '.');

  IF (C1 > 0) THEN

  t2 := dbms_utility.get_time;

  C2:=0;

  dbms_output.put_line('>');

  dbms_output.put_line('Start To Process.');

  -- Physically delete cds.global_data

  dbms_output.put_line('Deleting GLOBAL_DATA Records.');

  FOR i IN recs_to_process.FIRST..recs_to_process.LAST
  LOOP
   -- dbms_output.put_line(i||': [variable_id: '||recs_to_process(i).var_id||', global_data_id: '||recs_to_process(i).gd_id||'].');

    DELETE 
      from cds.global_data 
    WHERE 
      rowid = recs_to_process(i).row_id;

    C2:= C2 + SQL%ROWCOUNT; -- count records deleted

    IF (MOD(C2,$COMMIT_EVERY_N_RECS) = 0)
    THEN
     COMMIT;
     -- dbms_output.put_line('Every '||$COMMIT_EVERY_N_RECS||' Records Commit.');
    END IF;

    IF (C2 >= $MAX_RECS_TO_PROCESS) 
    THEN
     EXIT; 
    END IF;

    t4 := dbms_utility.get_time;

    IF ((t4 - t1)/100 > $MAX_RUNTIME_SEC)
    THEN
     dbms_output.put_line('Max Runtime '||$MAX_RUNTIME_SEC||' Sec. Reached.');
     EXIT;
    END IF;

    select count(1) into v_locked_objects
    from v\$lock where BLOCK > 0;

    IF (v_locked_objects > $MAX_LOCKED_OBJECTS)
    THEN
     dbms_output.put_line('Max Number Of Locked Objects Reached '||$MAX_LOCKED_OBJECTS||'.');
     EXIT;
    END IF;

  END LOOP;

  COMMIT;

  -- dbms_output.put_line('Final Commit.');

  dbms_output.put_line('End To Process.');

  t3 := dbms_utility.get_time;

  dbms_output.put_line('>');
  dbms_output.put_line('Execution Time Statistics:');
  dbms_output.put_line('Getting '|| C1 ||' Records To Delete From The GLOBAL_DATA in ' || TO_CHAR(round((t2 - t1)/100, 2)) ||' Sec.');
  dbms_output.put_line('Deleted '|| C2 ||' Records From The GLOBAL_DATA in ' || TO_CHAR(round((t3 - t2)/100, 2)) ||' Sec.');
  dbms_output.put_line('>');

  ELSE

    dbms_output.put_line('Nothing To Delete.');

  END IF;

END;
/

spo off;

exit
EOF
#############################################

cat $REPORT | mail -s "Oracle JGS: GD Physical Deletes Trace :: $(date)" $MAILING_LIST

echo "Oracle JGS: GD Physical Deletes Ended :: $(date)" | mail -s "Oracle JGS: GD Physical Deletes Ended :: $(date)" $MAILING_LIST 

echo "End the process."


