ALTER INDEX "TLOG"."PK_TX_ITEM" RENAME TO "PK_TX_ITEM_O";
ALTER INDEX "TLOG"."TX_ITEM_TX_ID_IDX" RENAME TO "TX_ITEM_TX_ID_IDX_O";
ALTER INDEX "TLOG"."TX_ITEM_OPERATION_IDX" RENAME TO "TX_ITEM_OPERATION_IDX_O";
ALTER INDEX "TLOG"."TX_ITEM_UPC_IDX" RENAME TO "TX_ITEM_UPC_IDX_O";
ALTER INDEX "TLOG"."TX_ITEM_UPC_OP_IDX" RENAME TO "TX_ITEM_UPC_OP_IDX_O";

ALTER INDEX "TLOG"."PK_TX_ITEM_N" RENAME TO "PK_TX_ITEM";
ALTER INDEX "TLOG"."TX_ITEM_TX_ID_IDX_N" RENAME TO "TX_ITEM_TX_ID_IDX";
ALTER INDEX "TLOG"."TX_ITEM_OPERATION_IDX_N" RENAME TO "TX_ITEM_OPERATION_IDX";
ALTER INDEX "TLOG"."TX_ITEM_UPC_IDX_N" RENAME TO "TX_ITEM_UPC_IDX";
ALTER INDEX "TLOG"."TX_ITEM_UPC_OP_IDX_N" RENAME TO "TX_ITEM_UPC_OP_IDX";

alter table "TLOG"."TX_ITEM_OLD" rename constraint PK_TX_ITEM to PK_TX_ITEM_O;

alter table "TLOG"."TX_ITEM" rename constraint PK_TX_ITEM_N to PK_TX_ITEM;

drop trigger "TLOG"."TRG_TX_ITEM_INSERT";

drop trigger "TLOG"."TRG_TX_ITEM_INSERT_N";

CREATE OR REPLACE TRIGGER "TLOG"."TRG_TX_ITEM_INSERT"
BEFORE INSERT ON tlog.TX_ITEM
REFERENCING OLD AS OLD NEW AS NEW
FOR EACH ROW
/*******************************************************************************
Copyright 2013 by ProLogic
All Rights Reserved.
Artisan      Date      Comments
============ ========= ========================================================
fherrmann    2013May21 Creation
*******************************************************************************/

DECLARE
  l_client_id tx_data.client_id%TYPE;
BEGIN
  SELECT client_id
    INTO l_client_id
    FROM tx_data
    WHERE tx_id = :NEW.tx_id;

  IF l_client_id = 1004 THEN
    :NEW.sales_amt := 0;
  END IF;
END;
/
ALTER TRIGGER "TLOG"."TRG_TX_ITEM_INSERT" ENABLE;

