alter table tlog.tx_item_old drop constraint fk_tx_item_tx_data;
alter table tlog.tx_item drop constraint fk_tx_item_tx_data_N;
alter table tlog.tx_item add constraint FK_TX_ITEM_TX_DATA foreign key (tx_id) references "TLOG"."TX_DATA" ("TX_ID") ENABLE;
alter table tlog.tx_trigger_item drop constraint fk_tx_trigger_item_tx_item;
alter table tlog.tx_trigger_item add constraint fk_tx_trigger_item_tx_item foreign key (tx_item_id) references tlog.tx_item(tx_item_id) enable;
