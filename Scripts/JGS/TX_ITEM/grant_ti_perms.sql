Set heading off;
set feedback off;
set echo off;
set pagesize 200;
set linesize 120;
set time off;
set timing off;
set verify off;
spool run_grants.sql

select 'spool TI_reorg_&1' || '.log append' from dual;

select 'GRANT ' || PRIVILEGE || ' ON ' || OWNER || '.' || TABLE_NAME || ' TO ' || GRANTEE || ';' from dba_tab_privs where owner='TLOG' and table_name like 'TX_ITEM%' order by grantee;

select 'spool off' from dual;

spool off;

set heading on;
set feedback on;
set echo on;

@run_grants.sql
exit
