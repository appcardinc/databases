Set heading off;
set feedback off;
set echo off;
set pagesize 200;
set linesize 120;
set time off;
set timing off;
set verify off;
spool run_invalids.sql

select 'spool GD_reorg_&1' || '.log append' from dual;

select   'ALTER ' || OBJECT_TYPE || ' ' ||   OWNER || '.' || OBJECT_NAME || ' COMPILE;'
from
   dba_objects
where
   status = 'INVALID'
and object_type in ('PACKAGE', 'FUNCTION','PROCEDURE', 'TRIGGER', 'VIEW', 'MATERIALIZED VIEW')
and owner not in ('PUBLIC', 'SYS', 'CTXSYS') order by owner, object_name;

select   'ALTER PACKAGE ' || OWNER || '.' || OBJECT_NAME || ' COMPILE BODY;'
from
   dba_objects
where
   status = 'INVALID'
and object_type = 'PACKAGE BODY'
and owner not in ('PUBLIC', 'SYS', 'CTXSYS') order by owner, object_name;

select 'spool off' from dual;

spool off;

set heading on;
set feedback on;
set echo on;

@run_invalids.sql
exit
