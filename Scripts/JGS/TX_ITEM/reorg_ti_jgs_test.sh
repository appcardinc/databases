#!/bin/sh
#################################################################
# Script - reorg_ti_jgs.sh                                      #
# Owner - Appcard                                               #
# Author - Ricardo Morgado Goncalves, DBA                       #                
# Date - 04/23/2020                                             #
#################################################################
#set -x
ORACLE_BASE=/app/oracle
ORACLE_HOME=/app/oracle/product/10.2.0/orajgs01
ORACLE_SID=orajgs01
export ORACLE_BASE ORACLE_HOME ORACLE_SID ORAENV_ASK=NO
OA_PRD=$ORACLE_BASE/admin/orajgs01
LD_LIBRARY_PATH=$ORACLE_HOME/lib:/usr/lib
PATH=$PATH:$ORACLE_HOME/bin:/usr/sbin

export MAILLIST=ricardog@appcard.com
#export MAILLIST="smorales@appcard.com,chak@appcard.com,ricardog@appcard.com"
HOST="$HOSTNAME"
export sdate=`date +%Y%m%d`
echo "today is:" $sdate

if [ -f ti_count.lst ]; then
 rm ti_count.lst
fi
if [ -f ti_count_end.lst ]; then
 rm ti_count_end.lst
fi
if [ -f ti_tx_id.lst ]; then
 rm ti_tx_id.lst
fi
if [ -f ti_tx_id_end.lst ]; then
 rm ti_tx_id_end.lst
fi
if [ -f run_invalids.sql ]; then
 rm run_invalids.sql
fi
if [ -f run_grants.sql ]; then
 rm run_grants.sql
fi

echo "************************************************************************************************"
echo "*  You are about to start the JGS tlog.tx_item reorg to  defragment  and  improve  performance *" 
echo "*  Have you commented out the all_backup_db.sh job from crontab?                               *"
echo "************************************************************************************************"

echo " "
while true; do
echo " "
read -p 'do you want to continue "y" or "n": ' yn
    case $yn in
        [Yy]* ) break;;
        [Nn]* ) exit;;
        * ) echo 'Please answer yes or no: ';;
    esac
done
echo " "
echo "********* STEP 1 - Disable all scheduled jobs *********"
sleep 3
sqlplus -s /nolog << EOF
connect / as sysdba

set echo on
set time on
set timing on
spool TI_reorg_$sdate.log

/*
BEGIN
      DBMS_SCHEDULER.disable(name=>'"CDS"."TLOG_60day_Clear"', force => TRUE);
END;
/
*/

select OWNER,JOB_NAME, ENABLED from DBA_SCHEDULER_JOBS where owner='CDS';
 
exit
EOF

echo " "
while true; do
echo "***** Have the scheduled cds job been successfully disabled? *****"
echo " "
read -p 'do you want to continue "y" or "n": ' yn
    case $yn in
        [Yy]* ) break;;
        [Nn]* ) exit;;
        * ) echo 'Please answer yes or no: ';;
    esac
done
echo " "
echo "********* STEP 2 - verify and delete last month's table **********"
sleep 3
sqlplus -s /nolog << EOF
connect / as sysdba
set echo on
set time on
set timing on
set serveroutput on
spool TI_reorg_$sdate.log append

select table_name from dba_tables where owner='TLOG' and table_name='TX_ITEM_OLD';
/* drop table tlog.TX_ITEM_OLD cascade constraints purge; */
set heading off
select 'table tlog.TX_ITEM_OLD has been deleted' from dual;
select table_name from dba_tables where owner='TLOG' and table_name='TX_ITEM_OLD';
exit
EOF

echo " "
while true; do
echo "***** Make sure the table tlog.TX_ITEM_OLD has been deleted, latest command should show 0 rows *****"
echo " "
read -p 'do you want to continue "y" or "n": ' yn
    case $yn in
        [Yy]* ) break;;
        [Nn]* ) exit;;
        * ) echo 'Please answer yes or no: ';;
    esac
done
echo " "
echo "********* STEP 3 - generate the list of invalids before the reorg *********"
sleep 3
sqlplus -s /nolog << EOF
connect / as sysdba
set echo on
set pagesize 100
set linesize 300
spool ti_inv_before_$sdate.log
column  object_name format a40
select owner, object_type, object_name  from dba_objects where status='INVALID' and owner not in ('PUBLIC', 'SYS', 'CTXSYS') order by 1,2,3;
spool off
exit
EOF

echo " "
while true; do
echo "***** Has the list of invalids been generated? If there are no invalids you can proceed as well ******"
echo " " 
read -p 'do you want to continue "y" or "n": ' yn
    case $yn in
        [Yy]* ) break;;
        [Nn]* ) exit;;
        * ) echo 'Please answer yes or no: ';;
    esac
done
echo " "
echo "********* STEP 4 - retrieve current size of table and verify the tablespace can accomodate a copy of tlog.TX_ITEM *********"
sleep 3
sqlplus -s /nolog << EOF
connect / as sysdba
set echo on
set time on
set timing on
spool TI_reorg_$sdate.log append
column tbl_size new_value tbl
select round(bytes/1024/1024/1024,2) as tbl_size from dba_segments where segment_name = 'TX_ITEM' and owner = 'TLOG';
set verify off
select sum(bytes)/1024/1024/1024 "Space avail. in Tablespace GB" from dba_free_space where tablespace_name = 'TLOG_DAT' having sum(bytes) > &tbl*1024*1024*1024;
exit
EOF

echo " "
while true; do
echo "********************************************************************************************"
echo "* Confirm space available in tablespace can accomodate the copy of the table. If Yes, just *"
echo "* continue. If No, then before proceeding, add space to  tablespace. To  add  space,  open *"
echo "* another sql session, log as sysdba and run  a command  similar  to  shown  below.  Check *"
echo "* the latest existing datafile name with toad or a query. Make sure to update the datafile *"
echo "* name with the next number.                                                               *"
echo "* ALTER TABLESPACE "TLOG_DAT" ADD DATAFILE '/oradata/orajgs01/TLOG_DAT_XX.dbf' SIZE 20000M    *"
echo "********************************************************************************************"
echo " "
read -p 'do you want to continue "y" or "n": ' yn
    case $yn in
        [Yy]* ) break;;
        [Nn]* ) exit;;
        * ) echo 'Please answer yes or no: ';;
    esac
done
echo " "
echo "********* STEP 5  - rows count and tx_id for future inserts *********"
sleep 3
sqlplus -s /nolog << EOF
connect / as sysdba
set echo on
set time on
set timing on
spool TI_reorg_$sdate.log append

select 'be patient, this will take few minutes to complete' "warning" from dual;

spool off
set echo off
set time off
set timing off
set feedback off
set heading off

spool ti_count.lst
select count(*) from tlog.tx_item;
spool off
spool ti_tx_id.lst
select max(tx_id) from tlog.tx_item; 
spool off

exit
EOF
export ti_count=`cat $HOME/rmg/REORG/ti_count.lst`
export ti_tx_id=`cat $HOME/rmg/REORG/ti_tx_id.lst`
echo " "
echo "tx_item count: " $ti_count
echo " "
echo "max tx_id:     " $ti_tx_id
echo " "

while true; do
echo "***** Have rows count and max tx_id been retrieved? ******"
echo " " 
read -p 'do you want to continue "y" or "n": ' yn
    case $yn in
        [Yy]* ) break;;
        [Nn]* ) exit;;
        * ) echo 'Please answer yes or no: ';;
    esac
done
echo " "
echo "********* STEP 6 - create the new table as a copy of the original *********"
sleep 3
sqlplus -s /nolog << EOF
connect / as sysdba
set echo on
set time on
set timing on
spool TI_reorg_$sdate.log append

select 'be patient, this will take few minutes to complete' "warning" from dual;
select 'table tlog.tx_item_new has been created' "Message" from dual;
/* create table tlog.tx_item_new tablespace tlog_dat as select * from tlog.tx_item; */
commit;
select table_name from dba_tables where table_name='tx_item_new' and owner='TLOG';
exit
EOF

echo " "
while true; do
echo "***** Has the table been successfully created? ******"
echo " " 
read -p 'do you want to continue "y" or "n": ' yn
    case $yn in
        [Yy]* ) break;;
        [Nn]* ) exit;;
        * ) echo 'Please answer yes or no: ';;
    esac
done
echo " "
echo "********* STEP 7 - Create existing indexes, trigger, PK and FK for the new table *********"
sleep 3
sqlplus -s /nolog << EOF
connect / as sysdba
set echo on
set time on
set timing on
spool TI_reorg_$sdate.log append
select 'be patient, this will take a while to complete' "warning" from dual;
/* @create_ti_indexes.sql */
select 'tx_item indexes, trigger, PK and FK have been created' "message" from dual;
exit
EOF

echo " "
while true; do
echo "***** Confirm indexes, trigger, PK and FK constraints have been created ******"
echo " "
read -p 'do you want to continue "y" or "n": ' yn
    case $yn in
        [Yy]* ) break;;
        [Nn]* ) exit;;
        * ) echo 'Please answer yes or no: ';;
    esac
done
echo " "
echo "********* STEP 8 - lock table tlog.tx_item in exclusive mode  *********"
sleep 3
sqlplus -s /nolog << EOF
connect / as sysdba
set echo on
set time on
set timing on
spool TI_reorg_$sdate.log append

/* lock table tlog.tx_item in exclusive mode nowait; */

select 'verify it is locked:' "message" from dual;

select session_id,oracle_username,os_user_name,locked_mode from v\$locked_object where object_id in
(select object_id from dba_objects where object_name IN ('TX_ITEM') and owner like 'TLOG');

exit
EOF
echo " "
while true; do
echo "***** Has the table been successfully locked? *****"
echo " "
read -p 'do you want to continue "y" or "n": ' yn
    case $yn in
        [Yy]* ) break;;
        [Nn]* ) exit;;
        * ) echo 'Please answer yes or no: ';;
    esac
done
echo " "
echo "********* STEP 9  - retrieve once again rows count and tx_id for resynching the tables *********"
sleep 3
sqlplus -s /nolog << EOF
connect / as sysdba
set echo on
set time on
set timing on
spool TI_reorg_$sdate.log append

select 'be patient, this will take few minutes to complete' "warning" from dual;

spool off
set echo off
set time off
set timing off
set feedback off
set heading off

spool ti_count_end.lst
select count(*) from tlog.tx_item;
spool off
spool ti_tx_id_end.lst
select max(tx_id) as MAX_TX_ID from tlog.tx_item;
spool off

exit
EOF
export ti_tx_id_end=`cat $HOME/rmg/REORG/ti_tx_id_end.lst`
export ti_count_end=`cat $HOME/rmg/REORG/ti_count_end.lst`

echo " "
echo "********* STEP 10 - Verify if new rows were inserted during the process *********"
echo " "
echo "start counts: " $ti_count
echo "end   counts: " $ti_count_end
echo " "
echo "start max tx id:  " $ti_tx_id
echo "end   max tx id:  " $ti_tx_id_end
sleep 3
echo " "
while true; do
echo "***** Are there new rows to be inserted? Is end max tx > start max tx?  ******"
echo " "
read -p 'type "y" for rows to be inserted or "n" to skip : ' yn
    case $yn in
        [Yy]* ) echo " "; echo '*** Rows will be inserted ***';
sqlplus -s /nolog << EOF
connect / as sysdba

set echo on
set time on
set timing on
spool TI_reorg_$sdate.log append

select 'Rows being inserted, please be patient' "message" from dual;

/* INSERT INTO TLOG.tx_item_NEW select * from tlog.tx_item 
where tx_id > $ti_tx_id and tx_id not in (select tx_id from TLOG.tx_item_NEW where tx_id > $ti_tx_id);
commit;
*/

select 'validating final counts to make sure tables are in sync. Please be patient' "message" from dual;

Select count(*) "rows from TX_ITEM" from TLOG.TX_ITEM;

Select count(*) "rows from TX_ITEM_NEW" from TLOG.TX_ITEM_NEW;

exit
EOF

break;;

        [Nn]* ) echo " "; echo '*** NO Rows will be inserted. But rows count will be retrieved and must match ***';
sqlplus -s /nolog << EOF
connect / as sysdba

set echo on
set time on
set timing on
spool TI_reorg_$sdate.log append
 
select 'validating final counts to make sure tables are in sync. Please be patient' "message" from dual;

Select count(*) "rows from TX_ITEM" from TLOG.TX_ITEM;

Select count(*) "rows from TX_ITEM_NEW" from TLOG.TX_ITEM_NEW;

exit
EOF

break;;
        * ) echo 'Please answer yes or no: ';;
    esac
done

echo " "
while true; do
echo "***** Verify the counts match and we are ready for renaming the tables. Before proceeding, checkpoint with Sam Morales and get confirmation ******"
echo " "
read -p 'do you want to continue "y" or "n": ' yn
    case $yn in
        [Yy]* ) break;;
        [Nn]* ) exit;;
        * ) echo 'Please answer yes or no: ';;
    esac
done
echo " "
echo "********* STEP 11 - Rename tables and verify it is unlocked  *********"
sleep 3
sqlplus -s /nolog << EOF
connect / as sysdba
set echo on
set time on
set timing on
spool TI_reorg_$sdate.log append

/* alter table TLOG.TX_ITEM rename to TX_ITEM_OLD; */

/* alter table TLOG.TX_ITEM_NEW rename to TX_ITEM; */

commit;

select 'Validating if table is unlocked now' "message" from dual;

select session_id,oracle_username,os_user_name,locked_mode from v\$locked_object where object_id in
(select object_id from dba_objects where object_name IN ('TX_ITEM') and owner like 'TLOG');

exit
EOF

echo " "
while true; do
echo "***** Have the tables been successfully renamed and unlocked?  ******"
echo " "
read -p 'do you want to continue "y" or "n": ' yn
    case $yn in
        [Yy]* ) break;;
        [Nn]* ) exit;;
        * ) echo 'Please answer yes or no: ';;
    esac
done
echo " "
echo "********* STEP 12 - Drop/recreate constraints *********"
sleep 3
sqlplus -s /nolog << EOF
connect / as sysdba

set echo on
set time on
set timing on
spool TI_reorg_20200317.log append

/* @rename_ti_const.sql */

select 'constraints have been dropped and recreated' "message" from dual;

exit
EOF

echo " "
echo "***** Have the constraints been successfully recreated? ******"
echo " "
read -p 'do you want to continue "y" or "n": ' yn
    case $yn in
        [Yy]* ) break;;
        [Nn]* ) exit;;
        * ) echo 'Please answer yes or no: ';;
    esac
echo " "
echo "********* STEP 13 - Validate all object counts match and dependencies are correct *********"
sleep 3
sqlplus -s /nolog << EOF
connect / as sysdba

set echo on
set time on
set timing on
set linesize 100
spool TI_reorg_$sdate.log append
column table_name format a40
column name format a40
column referenced_name format a38
column type format a15

select count(*) "indexes",table_name from dba_indexes where table_name IN ('TX_ITEM','TX_ITEM_OLD') group by table_name;
select count(*) "constraints",table_name from dba_constraints where table_name IN ('TX_ITEM','TX_ITEM_OLD') group by table_name;
select count(*) "columns",table_name from dba_tab_columns where table_name IN ('TX_ITEM','TX_ITEM_OLD') group by table_name;

select name "dependencies", type, referenced_name from dba_dependencies where referenced_name IN ('TX_ITEM','TX_ITEM_OLD') order by 1,2,3;
 
exit
EOF

echo " "
while true; do
echo "***** Do all objects match and dependencies are consistent?  ******"
echo " "
read -p 'do you want to continue "y" or "n": ' yn
    case $yn in
        [Yy]* ) break;;
        [Nn]* ) exit;;
        * ) echo 'Please answer yes or no: ';;
    esac
done
echo " "
echo "********* STEP 14 - Rename indexes, trigger and constraints *********"
sleep 3
sqlplus -s /nolog << EOF
connect / as sysdba

set echo on
set time on
set timing on
spool TI_reorg_$sdate.log append

select 'be patient, this will take a while to complete' "warning" from dual;

/* @rename_ti_indexes.sql */

select 'tx_item indexes trigger and constraints have been renamed' "message" from dual;
 
exit
EOF

echo " "
while true; do
echo "***** Have indexes, trigger and constraints been successfully renamed?  ******"
echo " "
read -p 'do you want to continue "y" or "n": ' yn
    case $yn in
        [Yy]* ) break;;
        [Nn]* ) exit;;
        * ) echo 'Please answer yes or no: ';;
    esac
done
echo " "
echo "********* STEP 15 - Recompile invalid objects  *********"
sleep 3
sqlplus -s /nolog << EOF
connect / as sysdba

set echo on
set time on
set timing on

select 'be patient, this will take a while to complete' "warning" from dual;

@recompile_ti_objects.sql $sdate

select 'database objects have been recompiled' "message" from dual;
 
exit
EOF

echo " "
while true; do
echo "***** Have all objects been successfully recompiled?                        ******"
echo "***** If there are errors they can be addressed after the reorg is complete ******"
echo " "
read -p 'do you want to continue "y" or "n": ' yn
    case $yn in
        [Yy]* ) break;;
        [Nn]* ) exit;;
        * ) echo 'Please answer yes or no: ';;
    esac
done
echo " "
echo "********* STEP 16 - Regenerate the list of invalids and certify no new invalids exist *********"
sleep 3
sqlplus -s /nolog << EOF
connect / as sysdba

set echo on
set pagesize 100
set linesize 300
spool ti_inv_after_$sdate.log
column  object_name format a40
select owner, object_type, object_name  from dba_objects where status='INVALID' and owner not in ('PUBLIC', 'SYS', 'CTXSYS') order by 1,2,3;
spool off
 
exit
EOF

echo " "
while true; do
echo "***** Have the list of invalids been successfully regenerated? Compare before and after lists       ******"
echo "***** if there are new invalids they will need to be  addressed  after  the  reorg  completes       ******"
echo " "
read -p 'do you want to continue "y" or "n": ' yn
    case $yn in
        [Yy]* ) break;;
        [Nn]* ) exit;;
        * ) echo 'Please answer yes or no: ';;
    esac
done
echo " "
echo "********* STEP 17 - Grant permissions *********"
sleep 3
sqlplus -s /nolog << EOF
connect / as sysdba

set echo on
set time on
set timing on

@grant_ti_perms.sql $sdate

select 'Permissions have been granted' "message" from dual;

exit
EOF

echo " "
while true; do
echo "***** Have permissions been successfully granted? ******"
echo " "
read -p 'do you want to continue "y" or "n": ' yn
    case $yn in
        [Yy]* ) break;;
        [Nn]* ) exit;;
        * ) echo 'Please answer yes or no: ';;
    esac
done
echo " "
echo "********* STEP 18 - Final validation for all object counts and dependencies *********"
sleep 3
sqlplus -s /nolog << EOF
connect / as sysdba

set echo on
set time on
set timing on
set linesize 100
spool TI_reorg_$sdate.log append
column table_name format a40
column name format a40
column referenced_name format a38
column type format a15

select count(*) "indexes",table_name from dba_indexes where table_name IN ('TX_ITEM','TX_ITEM_OLD') group by table_name;
select count(*) "constraints",table_name from dba_constraints where table_name IN ('TX_ITEM','TX_ITEM_OLD') group by table_name;
select count(*) "columns",table_name from dba_tab_columns where table_name IN ('TX_ITEM','TX_ITEM_OLD') group by table_name;

select name "dependencies", type, referenced_name from dba_dependencies where referenced_name IN ('TX_ITEM','TX_ITEM_OLD') order by 1,2,3;

exit
EOF
echo " "
while true; do
echo "***** Are objects final counts correct and dependencies consistent?  ******"
echo " "
read -p 'do you want to continue "y" or "n": ' yn
    case $yn in
        [Yy]* ) break;;
        [Nn]* ) exit;;
        * ) echo 'Please answer yes or no: ';;
    esac
done
echo " "
echo "********* STEP 19 - Run stats for table tx_item, including indexes  *********"
sleep 3
sqlplus -s /nolog << EOF
connect / as sysdba

set echo on
set time on
set timing on
spool TI_reorg_$sdate.log append

select 'be patient, this will take a while to complete' "warning" from dual;
/*
EXEC DBMS_STATS.gather_table_stats('CDS', 'TX_ITEM', estimate_percent => dbms_stats.auto_sample_size, method_opt => 'for all columns size skewonly', cascade => TRUE, degree => 6);
*/ 
exit
EOF
echo " "
while true; do
echo "***** Has the stats completed successfully?  ******"
echo " "
read -p 'do you want to continue "y" or "n": ' yn
    case $yn in
        [Yy]* ) break;;
        [Nn]* ) exit;;
        * ) echo 'Please answer yes or no: ';;
    esac
done
echo " "
echo "********* STEP 20 - Re-enable the scheduled jobs *********"
echo " "
sleep 3
sqlplus -s /nolog << EOF
connect / as sysdba

set echo on
set time on
set timing on
spool TI_reorg_$sdate.log append

/*
BEGIN
      DBMS_SCHEDULER.enable(name=>'"CDS"."TLOG_60day_Clear"');
END;
/
*/
select OWNER,JOB_NAME, ENABLED from DBA_SCHEDULER_JOBS where owner='CDS';
exit
EOF
echo " "
while true; do
echo "***** Has the job been re-enabled successfully?  ******"
echo " "
read -p 'do you want to continue "y" or "n": ' yn
    case $yn in
        [Yy]* ) break;;
        [Nn]* ) exit;;
        * ) echo 'Please answer yes or no: ';;
    esac
done
echo " "
echo "********* STEP 21 - Make sure to re-enable the script all_backup_db.sh from crontab *********"
echo "********* reschedule the full backup to run ASAP                                    *********" 
echo "********* If there are new invalid objects, make sure to address them               *********"
sleep 3
echo "***********************************************************************************"
echo "*                                                                                 *"
echo "*    CONGRATULATIONS, YOU HAVE COMPLETED THE $sdate JGS REORG FOR TX_ITEM!      *" 
echo "*                                                                                 *"
echo "***********************************************************************************"
sleep 2
cat TI_reorg_$sdate.log | mail -s "JGS - TX_ITEM REORG LOG on $sdate" $MAILLIST

exit
