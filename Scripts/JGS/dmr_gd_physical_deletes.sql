SET SERVEROUT ON

declare

 idx number;

begin

dbms_output.put_line('Started');

FOR idx IN 1..2
LOOP

dbms_output.put_line('Now processing iteration #'||idx);

delete from cds.global_data g  
 where g.deleted is not null 
 and (g.id, g.variable_id) in (select /*+ parallel(gdd, 6) */ gd.id, gdd.variable_id 
                           from common.global_data_definition gdd, cds.global_data gd 
                           where 
                           		gdd.variable_id = gd.variable_id 
                           	 and 
                           	 	gd.deleted is not null 
                           	 and 
                           	 	gdd.deleted > (SYSDATE - 30) 
                           	 and 
                           	 	gdd.deleted < (SYSDATE - 1) 
                           	 and 
                           	 	rownum <= 10);

commit;

dbms_output.put_line('Iteration #'||idx||' has been processed successfully!');

END LOOP;

dbms_output.put_line('Ended');

end;
