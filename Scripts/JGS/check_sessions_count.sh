#!/bin/sh
#################################################################
# Script - check_sessions_count.sh                                   #
# Owner - Dobler Consulting LLC                                 #
# Date - 08/25/2018                                             #
#################################################################
ORACLE_BASE=/app/oracle
ORACLE_HOME=/app/oracle/product/10.2.0/orajgs01
ORACLE_SID=orajgs01
export ORACLE_BASE ORACLE_HOME ORACLE_SID ORAENV_ASK=NO
OA_PRD=$ORACLE_BASE/admin/orajgs01
LD_LIBRARY_PATH=$ORACLE_HOME/lib:/usr/lib
PATH=$PATH:$ORACLE_HOME/bin:/usr/sbin

#MAILLIST=jearnest@doblerllc.com
MAILLIST="ProLogicSupport@doblerllc.com,smorales@appcard.com,chak@appcard.com,ricardog@appcard.com"
SPOOLFILE=/tmp/${ORACLE_SID}_session.sql
HOST="$HOSTNAME"
TMPFILE=/tmp/${ORACLE_SID}_session.tmp

#source $PROFILE

#if [ `ps -ef|grep -i $ORACLE_SID |grep pmon|grep -v grep|wc -l` -eq 0 ];then echo "DB is down"
#exit
#fi



if [ -f $TMPFILE ]; then
rm -f $TMPFILE
fi

if [ -f $SPOOLFILE ]; then
rm -f $SPOOLFILE
fi

lmins=`sqlplus -s "/ as sysdba" << EOF
set head off
set feedback off
select count(*)
from v\\$session
/
exit 
EOF`

longrun=0
if [ $lmins -gt 600 ]; then
longrun=`sqlplus -s "/ as sysdba" << EOF
set head off
set feedback off
select count(*)
from v\\$session
where last_call_et > 5
and status = 'ACTIVE'
and username not in ('SYS','SYSTEM')
; 
exit 
EOF`
fi

if [ $longrun -gt 10 ] 
then

echo "To: $MAILLIST" > $TMPFILE
echo "From: $HOST " >> $TMPFILE
echo "Subject: Warning Alert: High number of active sessions found in $ORACLE_SID on server $HOST" >> $TMPFILE
echo "Content-type: text/html" >> $TMPFILE
echo "MIME-Version: 1.0" >> $TMPFILE

sqlplus -s /nolog << EOF
connect / as sysdba
 set feed off
 set verify off
 set term off
 set echo off
 set trimout on
 set trimspool on 
 set markup html on

spool $SPOOLFILE
SET MARKUP HTML ON PREFORMAT OFF ENTMAP OFF -
HEAD "<TITLE>Active Sessions Report</TITLE> - <STYLE type='text/css'> -
   BODY {background: white} -
   TH {background: white} -
   TD {background: white} -
</STYLE>" -
BODY 'TEXT="black"' -
TABLE "BORDER='1'"

select 
   ses.username,
   ses.machine,
   ses.program,
   ses.status,
   ses.last_call_et,
   sql.hash_value,
   sql.sql_text
from 
   v\$session ses, 
   v\$sql sql
where ses.sql_hash_value = sql.hash_value
and   ses.username != 'SYS'
and   ses.status = 'ACTIVE'
and   last_call_et > 5
/

set lines 180 
prompt <H><u> Resource Utilization </u></H>
prompt
set heading on

select resource_name, current_utilization, max_utilization, limit_value 
    from v\$resource_limit 
    where resource_name in ('sessions', 'processes')
/
spool off
exit
EOF

cat $SPOOLFILE >> $TMPFILE

/usr/sbin/sendmail -t < $TMPFILE
#rm -f $TMPFILE 
else
    echo "Database is running normal $ORACLE_SID "
fi
exit
