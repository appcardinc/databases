#!/bin/sh
#
# Set up the env
ORACLE_BASE=/app/oracle
ORACLE_HOME=/app/oracle/product/10.2.0/orajgs01
ORACLE_SID=orajgs01
export ORACLE_BASE ORACLE_HOME ORACLE_SID
ORAENV_ASK=NO
OA_PRD=$ORACLE_BASE/admin/orajgs01
LD_LIBRARY_PATH=$ORACLE_HOME/lib:/usr/lib
PATH=$PATH:$ORACLE_HOME/bin:/usr/sbin
export OA_PRD LD_LIBRARY_PATH PATH ORAENV_ASK
#

export ORACLE_SID=$1
export MAX_LOCKS=$2

export REPORT=/tmp/max_arch_lag_$ORACLE_SID.rpt
export REPORT2=/tmp/max_arch_lag_2_$ORACLE_SID.rpt
export SERVER_NAME=`hostname`

sqlplus -s "/ as sysdba" << EOF
SET SERVEROUTPUT ON SIZE 100000
spool $REPORT

declare
v_lag number(20);
begin
select (
select count(*) from v\$lock where BLOCK > 0 
)
into v_lag
from dual;

if v_lag > $MAX_LOCKS
 then 
 dbms_output.put_line('Database Locks :  sessions Page DBA immediately');
 dbms_output.put_line('Locks are  = ' || v_lag);
end if;  
end;
/

spool off
EXIT
EOF

sqlplus -s "/ as sysdba" << EOF
SET SERVEROUTPUT ON SIZE 100000
spool $REPORT2

set lines 1200
COLUMN owner FORMAT A20
COLUMN username FORMAT A20
COLUMN object_owner FORMAT A20
COLUMN object_name FORMAT A30
COLUMN locked_mode FORMAT A15

SELECT lo.session_id AS sid,
       s.serial#,
       NVL(lo.oracle_username, '(oracle)') AS username,
       o.owner AS object_owner,
       o.object_name,
       Decode(lo.locked_mode, 0, 'None',
                             1, 'Null (NULL)',
                             2, 'Row-S (SS)',
                             3, 'Row-X (SX)',
                             4, 'Share (S)',
                             5, 'S/Row-X (SSX)',
                             6, 'Exclusive (X)',
                             lo.locked_mode) locked_mode,
       lo.os_user_name
FROM   v\$locked_object lo
       JOIN dba_objects o ON o.object_id = lo.object_id
       JOIN v\$session s ON lo.session_id = s.sid
ORDER BY 1, 2, 3, 4;

spool off
EXIT
EOF


Response=`grep -c 'Page DBA immediately' $REPORT`
if [ $Response -gt 0 ]
then
    echo 'Database locks issue, please check' $1 | mail -s " DB Lock issue on $1" dba@shsolutions.com padepu@shsolutions.com dmitryr@appcard.com z9o8w5q8u5t5g6z2@appcard.slack.com
    cat $REPORT2 | mail -s " Locked Objects on $1" dba@shsolutions.com padepu@shsolutions.com dmitryr@appcard.com z9o8w5q8u5t5g6z2@appcard.slack.com ricardog@appcard.com
fi
