#!/bin/sh
#################################################################
# Script - top_ten_exp_queries.sh                                   #
# Owner - Dobler Consulting LLC                                 #
# Date - 09/11/2018                                             #
#################################################################
ORACLE_BASE=/app/oracle
ORACLE_HOME=/app/oracle/product/10.2.0/orajgs01
ORACLE_SID=orajgs01
export ORACLE_BASE ORACLE_HOME ORACLE_SID ORAENV_ASK=NO
OA_PRD=$ORACLE_BASE/admin/orajgs01
LD_LIBRARY_PATH=$ORACLE_HOME/lib:/usr/lib
PATH=$PATH:$ORACLE_HOME/bin:/usr/sbin

#MAILLIST=jearnest@doblerllc.com
MAILLIST="ProLogicSupport@doblerllc.com,smorales@appcard.com,chak@appcard.com,ricardog@appcard.com"
SPOOLFILE=/tmp/${ORACLE_SID}_top_ten_exp_queries.sql
HOST="$HOSTNAME"
TMPFILE=/tmp/${ORACLE_SID}_top_ten_exp_queries.tmp

#source $PROFILE


if [ -f $TMPFILE ]; then
rm -f $TMPFILE
fi

if [ -f $SPOOLFILE ]; then
rm -f $SPOOLFILE
fi

echo "To: $MAILLIST" > $TMPFILE
echo "From: $HOST " >> $TMPFILE
echo "Subject: Top Ten Expensive Queries for database $ORACLE_SID on server $HOST" >> $TMPFILE
echo "Content-type: text/html" >> $TMPFILE
echo "MIME-Version: 1.0" >> $TMPFILE
 sqlplus -s /nolog << EOF 
connect / as sysdba
 set feed off
 set verify off
 set term off
 set echo off
 set trimout on
 set trimspool on 
spool $SPOOLFILE
 set markup html on PREFORMAT OFF ENTMAP OFF - 
 HEAD "<TITLE>Top Ten Expensive Queries</TITLE> - 
 <STYLE type='text/css'> -
   BODY {background: white} -
   TH {background: white} -
   TD {background: white} -
</STYLE>" -
BODY 'TEXT="black"' -
TABLE "BORDER='1'"

prompt <H2><center><u>Top Ten Expensive Queries for $ORACLE_SID</u><center></H2>
set linesize 300
set pagesize 10000
column sql_text heading "SQL Text"
column disk_reads  format 99,999,999,999
column buffer_gets  format 99,999,999,999
column executions  format 99,999
column sorts  format 99,999,999,999
column first_load_time format a25
column last_load_time format a25
column module format a20

SELECT *
FROM   (SELECT a.sql_text sql_text,
               a.sql_id,
               Trunc(a.disk_reads/Decode(a.executions,0,1,a.executions)) reads_per_execution, 
               a.buffer_gets, 
               a.disk_reads, 
               a.executions, 
               a.sorts,
               a.first_load_time,
               to_char(a.last_load_time,'DD/MM/YYYY HH24:MI:SS') as last_load_time,
               a.module
        FROM   v\$sqlarea a
        ORDER BY 3 DESC)
WHERE  rownum <= 10
/
spool off
exit
EOF

cat $SPOOLFILE >> $TMPFILE
/usr/sbin/sendmail -t < $TMPFILE
