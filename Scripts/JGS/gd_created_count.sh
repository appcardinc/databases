#!/bin/sh
#
# Set up the env
ORACLE_BASE=/app/oracle
ORACLE_HOME=/app/oracle/product/10.2.0/orajgs01
ORACLE_SID=orajgs01
export ORACLE_BASE ORACLE_HOME ORACLE_SID
ORAENV_ASK=NO
OA_PRD=$ORACLE_BASE/admin/orajgs01
LD_LIBRARY_PATH=$ORACLE_HOME/lib:/usr/lib
PATH=$PATH:$ORACLE_HOME/bin:/usr/sbin
export OA_PRD LD_LIBRARY_PATH PATH ORAENV_ASK
#

export ORACLE_SID=$1
export MAX_LOCKS=$2

export REPORT=/tmp/gd_created_count_$ORACLE_SID.rpt
export GD_Count=/tmp/gd_created_count.rpt
export SERVER_NAME=`hostname`

sqlplus -s "/ as sysdba" << EOF
SET SERVEROUTPUT ON SIZE 100000
SET FEEDBACK OFF
spool $REPORT

declare
v_count number(20);
l_date2   DATE := SYSDATE - 6;
begin
select (
select count(*) from cds.global_data where created > SYSDATE -7
)
into v_count
from dual;
dbms_output.put_line('Global Data Weekly Created Count From ' || l_date2 || ' TO ' || SYSDATE || ' = ' || v_count);
end;
/

spool off
EXIT
EOF

GD_Count=`grep -c 'Global' $REPORT`

echo $GD_Count
    
echo 'JGS GD Created Count' | mail -s " JGS DB Global Data Created" dmitryr@appcard.com edo@appcard.com blaurence@prologicretail.com jdunn@prologicretail.com syamaguchi@prologicretail.com asmith@prologicretail.com smorales@appcard.com ricardog@appcard.com < $REPORT

# echo 'JGS GD Created Count' | mail -s " JGS DB Global Data Created" dmitryr@appcard.com < $REPORT
