#!/bin/sh
#**********************************************************************
#*
#*  Name: backup_db.sh
#*
#**********************************************************************

if [ $# -ne 1 ];then
   echo 'Usage:$0 SID'
   exit 1;
fi

ORACLE_SID=`echo $1 | tr A-Z a-z`
export ORACLE_SID

# Set environment to this ORACLE_SID
. $HOME/.bash_profile_1 $ORACLE_SID
. $HOME/.maillist

# Skip if it is a physical standby
sqlplus -s "/ as sysdba" <<-EOF > /$HOME/ops/database_role
set verify off
set head off
select database_role from v\$database;
EOF

status=`grep 'PRIMARY' /$HOME/ops/database_role >/dev/null 2>&1; echo $?`
if [  $status -eq 1 ]; then
   rm -f /tmp/database_role
   exit 2
fi

export DATE=`date '+%m%d%Y'`

export EXP_BACKUP_DIR="/dump/$ORACLE_SID/export"
export RMAN_BACKUP_DIR="/dump/$ORACLE_SID/rman"

export EXP_BACKUP_LOG=$EXP_BACKUP_DIR/export_backup_$DATE.log
export RMAN_BACKUP_LOG=$RMAN_BACKUP_DIR/rman_backup_$DATE.log

export EXP_FILE=$EXP_BACKUP_DIR/export_$DATE.dmp
export TRACE_CONTROLFILE=$RMAN_BACKUP_DIR/trace_controlfile_$DATE.ctl
export BACKUP_ERROR=/tmp/backup_error_$$

# Cleanup backup directories
rm /dump/orajgs01/rman/ora_backup_*
#rm -f $RMAN_BACKUP_DIR/*log $RMAN_BACKUP_DIR/*ctl $EXP_BACKUP_DIR/*

# Create backup directories, if they do not exist
#mkdir -p $EXP_BACKUP_DIR $RMAN_BACKUP_DIR
rm $EXP_BACKUP_DIR/*.*

echo "Started  RMAN   backup of $ORACLE_SID at `date`"
rman target / <<-EOF > $RMAN_BACKUP_LOG
LIST INCARNATION;

CONFIGURE DEVICE TYPE DISK PARALLELISM 2;
CONFIGURE RETENTION POLICY TO REDUNDANCY 1;
CONFIGURE RETENTION POLICY TO RECOVERY WINDOW OF 2 DAYS;
CONFIGURE CHANNEL DEVICE TYPE DISK FORMAT '$RMAN_BACKUP_DIR/ora_backup_db_%d_S_%s_P_%p_T_%t';

CONFIGURE CONTROLFILE AUTOBACKUP ON;
CONFIGURE CONTROLFILE AUTOBACKUP FORMAT FOR DEVICE TYPE DISK TO '$RMAN_BACKUP_DIR/ora_cf%F';

# Except archivelogs from RMAN recovery/restore/delete
# Mark expired all archivelogs that are missing
# and delete all expired
CHANGE ARCHIVELOG ALL AVAILABLE;
CROSSCHECK BACKUP;
crosscheck archivelog all;
DELETE NOPROMPT EXPIRED ARCHIVELOG ALL;
CHANGE ARCHIVELOG ALL UNAVAILABLE;

# Delete backup
#DELETE NOPROMPT BACKUP DEVICE TYPE DISK;
#DELETE NOPROMPT EXPIRED BACKUP;
#DELETE NOPROMPT OBSOLETE RECOVERY WINDOW OF 2 DAYS;
DELETE NOPROMPT FORCE OBSOLETE;
DELETE NOPROMPT EXPIRED BACKUP;
DELETE NOPROMPT BACKUP DEVICE TYPE DISK COMPLETED BEFORE 'SYSDATE-1';

# Backup database and archivelog
BACKUP DATABASE FILESPERSET 1 PLUS ARCHIVELOG;

# List backup information
LIST ARCHIVELOG ALL;
LIST BACKUP OF CONTROLFILE;
LIST BACKUP SUMMARY;
LIST BACKUP;

# Reports
REPORT SCHEMA;
REPORT UNRECOVERABLE;
REPORT NEED BACKUP;

# Validate backups
#BACKUP CHECK LOGICAL VALIDATE DATABASE ARCHIVELOG ALL;

# Validate the restore of the backup
#RESTORE DATABASE VALIDATE;

EOF

echo "Finished RMAN   backup of $ORACLE_SID at `date`"

sqlplus -s "/ as sysdba" <<-EOF > /dev/null 2>&1
ALTER DATABASE BACKUP CONTROLFILE TO TRACE AS '$TRACE_CONTROLFILE' REUSE;
EOF

echo "Started  EXPORT backup of $ORACLE_SID at `date`"
# Full export
exp backup_admin/backup_admin file=$EXP_FILE log=$EXP_BACKUP_LOG full=y statistics=none direct=y recordlength=65535 > /dev/null 2>&1
#pk gzip $EXP_FILE
#pk gzip /dump/orajgs01/rman/ora_*
echo "Finished EXPORT backup of $ORACLE_SID at `date`"

# Check for errors during RMAN backup
# Ignore RMAN-06480: archivelog 'archivelog_file' cannot be found on disk
# and    RMAN-06485: changed archivelog expired
echo "Check $RMAN_BACKUP_LOG and $EXP_BACKUP_LOG" > $BACKUP_ERROR
egrep -a "ORA-"\|"error"\|"RMAN-" $RMAN_BACKUP_LOG | egrep -v "RMAN-06480" | egrep -v "RMAN-06485" > $BACKUP_ERROR
status_rman=$?

# Check for errors during export
egrep -a "ORA-"\|"error"\|"EXP-"  $EXP_BACKUP_LOG | egrep -v "EXP-00079" >> $BACKUP_ERROR
status_exp=$?

# Notify users if errors were found in either RMAN or export backup
if [ $status_rman -eq 0 -o $status_exp -eq 0 ];then
   mail -s "ORACLE ALERT => [`hostname`]: Database backup failed for - $ORACLE_SID!" $mail_list < $BACKUP_ERROR > /dev/null 2>&1
   exit 1
fi

mail -s "[`hostname`]: Database backup succeeded for - $ORACLE_SID" $mail_list < /dev/null > /dev/null 2>&1

# Cleanup and exit
rm -f $BACKUP_ERROR

exit 0
