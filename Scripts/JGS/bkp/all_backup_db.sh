#!/bin/sh
#**********************************************************************
#*          
#*  Name: all_backup_db.sh
#*          
#**********************************************************************

# Loop through all databases listed in /etc/oratab
for DB_ENTRY in `cat /etc/oratab | grep -v \# | grep -v \* | grep -v ^$`
do
   ORACLE_SID=`echo $DB_ENTRY | awk -F: {' print $1 '}`;
   export ORACLE_SID

   echo "Started  backup of $ORACLE_SID at `date`"
   $HOME/scripts/bkp/backup_db.sh $ORACLE_SID
   echo "Finished backup of $ORACLE_SID at `date`"
done
