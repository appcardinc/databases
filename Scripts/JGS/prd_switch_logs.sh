#!/bin/sh

#
# Set up the env
ORACLE_BASE=/app/oracle
ORACLE_HOME=/app/oracle/product/10.2.0/orajgs01
ORACLE_SID=orajgs01
export ORACLE_BASE ORACLE_HOME ORACLE_SID
ORAENV_ASK=NO
OA_PRD=$ORACLE_BASE/admin/orajgs01
LD_LIBRARY_PATH=$ORACLE_HOME/lib:/usr/lib
PATH=$PATH:$ORACLE_HOME/bin:/usr/sbin
export OA_PRD LD_LIBRARY_PATH PATH ORAENV_ASK
# .oraenv
#
#
# ALL TABLESPACE AND DATAFILES
#
#
$ORACLE_HOME/bin/sqlplus -s /nolog <<EOF
connect / as sysdba
alter system switch logfile;
exit
EOF
#############################################

