#!/bin/sh
#################################################################
# Script - reorg_gd_jgs.sh                                      #
# Owner - Appcard                                               #
# Author - Ricardo Morgado Goncalves, DBA                       #                
# Date - 04/17/2020                                             #
#################################################################
#set -x
ORACLE_BASE=/app/oracle
ORACLE_HOME=/app/oracle/product/10.2.0/orajgs01
ORACLE_SID=orajgs01
export ORACLE_BASE ORACLE_HOME ORACLE_SID ORAENV_ASK=NO
OA_PRD=$ORACLE_BASE/admin/orajgs01
LD_LIBRARY_PATH=$ORACLE_HOME/lib:/usr/lib
PATH=$PATH:$ORACLE_HOME/bin:/usr/sbin

export MAILLIST=ricardog@appcard.com
#export MAILLIST="smorales@appcard.com,chak@appcard.com,ricardog@appcard.com"
HOST="$HOSTNAME"
export sdate=`date +%Y%m%d`
echo "today is:" $sdate

if [ -f gd_date.lst ]; then
 rm gd_date.lst
fi
if [ -f gd_max.lst ]; then
 rm gd_max.lst
fi
if [ -f run_invalids.sql ]; then
 rm run_invalids.sql
fi
if [ -f run_grants.sql ]; then
 rm run_grants.sql
fi

echo "***********************************************************************************************"
echo "*  You are about to start the JGS cds.global_data reorg to defragment and improve performance *" 
echo "*  Have you commented out following jobs from crontab?                                        *"
echo "*  new_gd_physical_deletes.sh  and all_backup_db.sh                                           *"
echo "***********************************************************************************************"

echo " "
while true; do
echo " "
read -p 'do you want to continue "y" or "n": ' yn
    case $yn in
        [Yy]* ) break;;
        [Nn]* ) exit;;
        * ) echo 'Please answer yes or no: ';;
    esac
done
echo " "
echo "********* STEP 1 - verify and delete last month's table **********"
sleep 3
sqlplus -s /nolog << EOF
connect / as sysdba
set echo on
set time on
set timing on
set serveroutput on
spool GD_reorg_$sdate.log

select table_name from dba_tables where owner='CDS' and table_name='GLOBAL_DATA_OLD';
/* drop table cds.GLOBAL_DATA_OLD cascade constraints purge; */
set heading off
select 'table cds.GLOBAL_DATA_OLD has been deleted' from dual;
select table_name from dba_tables where owner='CDS' and table_name='GLOBAL_DATA_OLD';
exit
EOF

echo " "
while true; do
echo "***** Make sure the table cds.GLOBAL_DATA_OLD has been deleted, latest command should show 0 rows *****"
echo " "
read -p 'do you want to continue "y" or "n": ' yn
    case $yn in
        [Yy]* ) break;;
        [Nn]* ) exit;;
        * ) echo 'Please answer yes or no: ';;
    esac
done
echo " "
echo "********* STEP 2 - generate the list of invalids before the reorg *********"
sleep 3
sqlplus -s /nolog << EOF
connect / as sysdba
set echo on
set pagesize 100
set linesize 300
spool gd_inv_before_$sdate.log
column  object_name format a40
select owner, object_type, object_name  from dba_objects where status='INVALID' and owner not in ('PUBLIC', 'SYS', 'CTXSYS') order by 1,2,3;
spool off
exit
EOF

echo " "
while true; do
echo "***** Has the list of invalids been generated? If there are no invalids you can proceed as well ******"
echo " " 
read -p 'do you want to continue "y" or "n": ' yn
    case $yn in
        [Yy]* ) break;;
        [Nn]* ) exit;;
        * ) echo 'Please answer yes or no: ';;
    esac
done
echo " "
echo "********* STEP 3 - retrieve current size of table and verify the tablespace can accomodate a copy of cds.GLOBAL_DATA *********"
sleep 3
sqlplus -s /nolog << EOF
connect / as sysdba
set echo on
set time on
set timing on
spool GD_reorg_$sdate.log append
column tbl_size new_value tbl
select round(bytes/1024/1024/1024,2) as tbl_size from dba_segments where segment_name = 'GLOBAL_DATA' and owner = 'CDS';
set verify off
select sum(bytes)/1024/1024/1024 "Space avail. in Tablespace GB" from dba_free_space where tablespace_name = 'CDS_DAT' having sum(bytes) > &tbl*1024*1024*1024;
exit
EOF

echo " "
while true; do
echo "********************************************************************************************"
echo "* Confirm space available in tablespace can accomodate the copy of the table. If Yes, just *"
echo "* continue. If No, then before proceeding, add space to  tablespace. To  add  space,  open *"
echo "* another sql session, log as sysdba and run  a command  similar  to  shown  below.  Check *"
echo "* the latest existing datafile name with toad or a query. Make sure to update the datafile *"
echo "* name with the next number.                                                               *"
echo "* ALTER TABLESPACE "CDS_DAT" ADD DATAFILE '/oradata/orajgs01/CDS_DAT_XX.dbf' SIZE 20000M     *"
echo "********************************************************************************************"
echo " "
read -p 'do you want to continue "y" or "n": ' yn
    case $yn in
        [Yy]* ) break;;
        [Nn]* ) exit;;
        * ) echo 'Please answer yes or no: ';;
    esac
done
echo " "
echo "********* STEP 4  - retrieve date, time, max seq and count of rows for future inserts and updates *********"
sleep 3
sqlplus -s /nolog << EOF
connect / as sysdba
set echo on
set time on
set timing on
spool GD_reorg_$sdate.log append

select to_char(sysdate,'YYYYMMDD HH24:MI:SS') "date and time"  from dual;

select max(id) from cds.global_data;

select 'be patient, this will take few minutes to complete' "warning" from dual;

select count(*) from cds.global_data;

spool off
set echo off
set time off
set timing off
set feedback off
set heading off

spool gd_date.lst
select to_char(sysdate,'YYYYMMDD HH24:MI:SS') from dual;
spool off
spool gd_max.lst
select max(id) from cds.global_data;
spool off

exit
EOF
export gd_date=`cat $HOME/rmg/REORG/gd_date.lst`
export gd_max=`cat $HOME/rmg/REORG/gd_max.lst`
echo " "
echo " "
echo "start date: " $gd_date
echo " "
echo "max gd row: " $gd_max
echo " "

while true; do
echo "***** Have date, time, max seq and count of rows been retrieved? ******"
echo " " 
read -p 'do you want to continue "y" or "n": ' yn
    case $yn in
        [Yy]* ) break;;
        [Nn]* ) exit;;
        * ) echo 'Please answer yes or no: ';;
    esac
done
echo " "
echo "********* STEP 5 - create the new table as a copy of the original *********"
sleep 3
sqlplus -s /nolog << EOF
connect / as sysdba
set echo on
set time on
set timing on
spool GD_reorg_$sdate.log append

select 'be patient, this will take few minutes to complete' "warning" from dual;
select 'table cds.global_data_new has been created' "Message" from dual;
/* create table cds.global_data_new tablespace cds_dat as select * from cds.global_data; */
commit;
select table_name from dba_tables where table_name='global_data_new' and owner='CDS';
exit
EOF

echo " "
while true; do
echo "***** Has the table been succesfully created? ******"
echo " " 
read -p 'do you want to continue "y" or "n": ' yn
    case $yn in
        [Yy]* ) break;;
        [Nn]* ) exit;;
        * ) echo 'Please answer yes or no: ';;
    esac
done
echo " "
echo "********* STEP 6 - Create existing indexes in the new table *********"
sleep 3
sqlplus -s /nolog << EOF
connect / as sysdba
set echo on
set time on
set timing on
spool GD_reorg_$sdate.log append
select 'be patient, this will take a while to complete' "warning" from dual;
/* @create_gd_indexes.sql */
select 'global_data indexes have been created' "message" from dual;
exit
EOF

echo " "
while true; do
echo "***** Confirm indexes, PK and FK constraints have been created ******"
echo " "
read -p 'do you want to continue "y" or "n": ' yn
    case $yn in
        [Yy]* ) break;;
        [Nn]* ) exit;;
        * ) echo 'Please answer yes or no: ';;
    esac
done
echo " "
echo "********* STEP 7 - lock table cds.global_data in exclusive mode  *********"
sleep 3
sqlplus -s /nolog << EOF
connect / as sysdba
set echo on
set time on
set timing on
spool GD_reorg_$sdate.log append

/* lock table cds.global_data in exclusive mode nowait; */

select 'verify it is locked:' "message" from dual;

select session_id,oracle_username,os_user_name,locked_mode from v\$locked_object where object_id in
(select object_id from dba_objects where object_name IN ('GLOBAL_DATA') and owner like 'CDS');

exit
EOF

echo " "
while true; do
echo "***** Is the table locked and ready for renaming? Before proceeding, checkpoint with Sam Morales and get confirmation ******"
echo " "
read -p 'do you want to continue "y" or "n": ' yn
    case $yn in
        [Yy]* ) break;;
        [Nn]* ) exit;;
        * ) echo 'Please answer yes or no: ';;
    esac
done
echo " "
echo "********* STEP 8 - Rename tables  *********"
sleep 3
sqlplus -s /nolog << EOF
connect / as sysdba
set echo on
set time on
set timing on
spool GD_reorg_$sdate.log append

/* alter table cds.global_data rename to global_data_old; */

/* alter table cds.global_data_new rename to global_data; */

exit
EOF

echo " "
while true; do
echo "***** Have the tables been successfully renamed? ******"
echo " "
read -p 'do you want to continue "y" or "n": ' yn
    case $yn in
        [Yy]* ) break;;
        [Nn]* ) exit;;
        * ) echo 'Please answer yes or no: ';;
    esac
done
echo " "
echo "********* STEP 9 - Verify if new rows were inserted during the process *********"
echo " "
echo "start date: " $gd_date
echo "max gd row: " $gd_max
sleep 3
sqlplus -s /nolog << EOF
connect / as sysdba
set echo on
set time on
set timing on
spool GD_reorg_$sdate.log append

SELECT ID
from cds.global_data_old
where created >= to_date('$gd_date','YYYYMMDD HH24:MI:SS') and ID > $gd_max;

exit
EOF

echo " "
while true; do
echo "***** Are there new rows to be inserted ?  ******"
read -p 'type "y" for rows to be inserted or "n" to skip : ' yn
    case $yn in
        [Yy]* ) echo 'Rows will be inserted';  
sqlplus -s /nolog << EOF
connect / as sysdba

set echo on
set time on
set timing on
spool GD_reorg_$sdate.log append

select 'Rows being inserted, please be patient' "message" from dual;

/* INSERT INTO cds.global_data(ID,entity_id,new_entity_id,variable_id,value,created,deleted,updated,version)
select ID,entity_id,new_entity_id,variable_id,value,created,deleted,updated,version 
from cds.global_data_old
where created >= to_date('$gd_date','YYYYMMDD HH24:MI:SS') and ID > $gd_max;
commit;
*/
exit
EOF

break;;
        [Nn]* ) break;;
        * ) echo 'Please answer yes or no: ';;
    esac
done

echo " "
while true; do
echo "***** Now that new rows were inserted (or not), we will check for new updates  ******"
echo " "
read -p 'do you want to continue "y" or "n": ' yn
    case $yn in
        [Yy]* ) break;;
        [Nn]* ) exit;;
        * ) echo 'Please answer yes or no: ';;
    esac
done
echo " "
echo "********* STEP 10 - Verify if rows were updated during the process *********"
sleep 3
sqlplus -s /nolog << EOF
connect / as sysdba

set echo on
set time on
set timing on
spool GD_reorg_$sdate.log append

select 'be patient, this will take a while to complete' "warning" from dual;

select b.id, b.new_entity_id
from cds.global_data a, cds.global_data_old b
where a.id = b.id 
and b.updated > a.updated 
and b.updated > to_date('$gd_date','YYYYMMDD HH24:MI:SS');

exit
EOF

echo " "
while true; do
echo "***** Are there rows to be updated?  ******"
read -p 'type "y" for rows to be updated or "n" to skip : ' yn
    case $yn in
        [Yy]* ) echo 'Rows will be updated';
sqlplus -s /nolog << EOF
connect / as sysdba

set echo on
set time on
set timing on
spool GD_reorg_$sdate.log append

select 'Rows being updated, please be patient' "message" from dual;

/* update cds.global_data a 
set (entity_id, new_entity_id, variable_id, value,created,deleted,updated,version)
=(select entity_id, new_entity_id, variable_id, value,created,deleted,updated,version 
from cds.global_data_old b
where a.id = b.id and b.updated > a.updated and b.updated > to_date('$gd_date','YYYYMMDD HH24:MI:SS')) 
where exists (
select entity_id, new_entity_id, variable_id, value,created,deleted,updated,version
from cds.global_data_old b
where a.id = b.id and b.updated > a.updated and b.updated > to_date('$gd_date','YYYYMMDD HH24:MI:SS'));
commit;
*/
exit
EOF
break;;
        [Nn]* ) break;;
        * ) echo 'Please answer yes or no: ';;
    esac
done

echo " "
while true; do
echo "***** If there were rows to be updated, have them been successfully updated?  ******"
echo "***** OR If there were NO rows to be updated, type "y" to continue              ******"
echo " "
read -p 'do you want to continue "y" or "n": ' yn
    case $yn in
        [Yy]* ) break;;
        [Nn]* ) exit;;
        * ) echo 'Please answer yes or no: ';;
    esac
done
echo " "
echo "********* STEP 11 - Validate all object counts match and dependencies are correct *********"
sleep 3
sqlplus -s /nolog << EOF
connect / as sysdba

set echo on
set time on
set timing on
set linesize 100
spool GD_reorg_$sdate.log append
column table_name format a40
column name format a40
column referenced_name format a38
column type format a15
select count(*),table_name from dba_indexes where table_name IN ('GLOBAL_DATA','GLOBAL_DATA_OLD') group by table_name;
select count(*),table_name from dba_constraints where table_name IN ('GLOBAL_DATA','GLOBAL_DATA_OLD') group by table_name;
select count(*),table_name from dba_tab_columns where table_name IN ('GLOBAL_DATA','GLOBAL_DATA_OLD') group by table_name;

select name, type, referenced_name from dba_dependencies where referenced_name IN ('GLOBAL_DATA','GLOBAL_DATA_OLD') order by 1,2,3;
 
exit
EOF

echo " "
while true; do
echo "***** Do all objects match and dependencies are consistent?  ******"
echo " "
read -p 'do you want to continue "y" or "n": ' yn
    case $yn in
        [Yy]* ) break;;
        [Nn]* ) exit;;
        * ) echo 'Please answer yes or no: ';;
    esac
done
echo " "
echo "********* STEP 12 - Rename indexes and constraints *********"
sleep 3
sqlplus -s /nolog << EOF
connect / as sysdba

set echo on
set time on
set timing on
spool GD_reorg_$sdate.log append

select 'be patient, this will take a while to complete' "warning" from dual;

/* @rename_gd_indexes.sql */

select 'global_data indexes have been renamed' "message" from dual;
 
exit
EOF

echo " "
while true; do
echo "***** Have indexes and constraints been successfully renamed?  ******"
echo " "
read -p 'do you want to continue "y" or "n": ' yn
    case $yn in
        [Yy]* ) break;;
        [Nn]* ) exit;;
        * ) echo 'Please answer yes or no: ';;
    esac
done
echo " "
echo "********* STEP 13 - Recompile invalid objects  *********"
sleep 3
sqlplus -s /nolog << EOF
connect / as sysdba

set echo on
set time on
set timing on

select 'be patient, this will take a while to complete' "warning" from dual;

@recompile_gd_objects.sql $sdate

select 'database objects have been recompiled' "message" from dual;
 
exit
EOF

echo " "
while true; do
echo "***** Have all objects been successfully recompiled?                        ******"
echo "***** If there are errors they can be addressed after the reorg is complete ******"
echo " "
read -p 'do you want to continue "y" or "n": ' yn
    case $yn in
        [Yy]* ) break;;
        [Nn]* ) exit;;
        * ) echo 'Please answer yes or no: ';;
    esac
done
echo " "
echo "********* STEP 14 - Regenerate the list of invalids and certify no new invalids exist *********"
sleep 3
sqlplus -s /nolog << EOF
connect / as sysdba

set echo on
set pagesize 100
set linesize 300
spool gd_inv_after_$sdate.log
column  object_name format a40
select owner, object_type, object_name  from dba_objects where status='INVALID' and owner not in ('PUBLIC', 'SYS', 'CTXSYS') order by 1,2,3;
spool off
 
exit
EOF

echo " "
while true; do
echo "***** Have the list of invalids been successfully regenerated? Compare before and after lists       ******"
echo "***** if there are new invalids they will need to be  addressed  after  the  reorg  completes       ******"
echo " "
read -p 'do you want to continue "y" or "n": ' yn
    case $yn in
        [Yy]* ) break;;
        [Nn]* ) exit;;
        * ) echo 'Please answer yes or no: ';;
    esac
done
echo " "
echo "********* STEP 15 - Grant permissions *********"
sleep 3
sqlplus -s /nolog << EOF
connect / as sysdba

set echo on
set time on
set timing on

@grant_gd_perms.sql $sdate

select 'Permissions have been granted' "message" from dual;

exit
EOF

echo " "
while true; do
echo "***** Have permissions been successfully granted? ******"
echo " "
read -p 'do you want to continue "y" or "n": ' yn
    case $yn in
        [Yy]* ) break;;
        [Nn]* ) exit;;
        * ) echo 'Please answer yes or no: ';;
    esac
done
echo " "
echo "********* STEP 16 - Final validation for all object counts and dependencies *********"
sleep 3
sqlplus -s /nolog << EOF
connect / as sysdba

set echo on
set time on
set timing on
set linesize 100
spool GD_reorg_$sdate.log append
column table_name format a40
column name format a40
column referenced_name format a38
column type format a15
select count(*),table_name from dba_indexes where table_name IN ('GLOBAL_DATA','GLOBAL_DATA_OLD') group by table_name;
select count(*),table_name from dba_constraints where table_name IN ('GLOBAL_DATA','GLOBAL_DATA_OLD') group by table_name;
select count(*),table_name from dba_tab_columns where table_name IN ('GLOBAL_DATA','GLOBAL_DATA_OLD') group by table_name;

select name, type, referenced_name from dba_dependencies where referenced_name IN ('GLOBAL_DATA','GLOBAL_DATA_OLD') order by 1,2,3;

exit
EOF
echo " "
while true; do
echo "***** Are objects final counts correct and dependencies consistent?  ******"
echo " "
read -p 'do you want to continue "y" or "n": ' yn
    case $yn in
        [Yy]* ) break;;
        [Nn]* ) exit;;
        * ) echo 'Please answer yes or no: ';;
    esac
done
echo " "
echo "********* STEP 17 - Run stats for table global_data, including indexes  *********"
echo " "
sleep 3
sqlplus -s /nolog << EOF
connect / as sysdba

set echo on
set time on
set timing on
spool GD_reorg_$sdate.log append

select 'be patient, this will take a while to complete' "warning" from dual;

EXEC DBMS_STATS.gather_table_stats('CDS', 'GLOBAL_DATA', estimate_percent => dbms_stats.auto_sample_size, method_opt => 'for all columns size skewonly', cascade => TRUE, degree => 6);
 
exit
EOF
echo " "
while true; do
echo "***** Has the stats completed successfully?  ******"
echo " "
read -p 'do you want to continue "y" or "n": ' yn
    case $yn in
        [Yy]* ) break;;
        [Nn]* ) exit;;
        * ) echo 'Please answer yes or no: ';;
    esac
done
echo " "
echo "********* STEP 18 - Make sure to re-enable the scripts below from crontab *********"
echo "********* new_gd_physical_deletes.sh and all_backup_db.sh                 *********"
echo "********* reschedule the full backup to run ASAP                          *********" 
echo "********* If there are new invalid objects, make sure to address them     *********"
sleep 3
echo "***********************************************************************************"
echo "*                                                                                 *"
echo "*     CONGRATULATIONS, YOU HAVE COMPLETED THE $sdate JGS REORG FOR GLOBAL DATA! *" 
echo "*                                                                                 *"
echo "***********************************************************************************"
sleep 2
cat GD_reorg_$sdate.log | mail -s "JGS - GLOBAL_DATA REORG LOG on $sdate" $MAILLIST

exit
