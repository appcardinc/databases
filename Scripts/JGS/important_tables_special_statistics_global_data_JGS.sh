#!/bin/sh
#
# Set up the env
export ORACLE_BASE=/app/oracle
export ORACLE_HOME=/app/oracle/product/10.2.0/orajgs01
export ORACLE_SID=orajgs01
export ORACLE_BASE ORACLE_HOME ORACLE_SID
export ORAENV_ASK=NO
export OA_PRD=$ORACLE_BASE/admin/orajgs01
export LD_LIBRARY_PATH=$ORACLE_HOME/lib:/usr/lib
export PATH=$PATH:$ORACLE_HOME/bin:/usr/sbin
export OA_PRD LD_LIBRARY_PATH PATH ORAENV_ASK
#

export REPORT=/tmp/important_tables_special_statistics_$ORACLE_SID.rpt
export SERVER_NAME=`hostname`

sqlplus -s "/ as sysdba" << EOF

set head off
set feedback off
spo $REPORT

select 'Oracle JGS: Special Statistics for Some Important Tables' from dual;

select 'Started: '||to_char(sysdate, 'YYYY-MON-DD HH24:MI:SS') from dual;

select ' '||to_char(sysdate, 'YYYY-MON-DD HH24:MI:SS')||' - Gather Table Statistics: CDS.GLOBAL_DATA.' from dual;

EXEC DBMS_STATS.gather_table_stats('CDS', 'GLOBAL_DATA', estimate_percent => dbms_stats.auto_sample_size, method_opt => 'for all columns size skewonly', cascade => TRUE, degree => 6);

select ' '||to_char(sysdate, 'YYYY-MON-DD HH24:MI:SS')||' - Gather Table Statistics: CDS.GLOBAL_DATA_HISTORY.' from dual;

EXEC DBMS_STATS.gather_table_stats('CDS', 'GLOBAL_DATA_HISTORY', estimate_percent => dbms_stats.auto_sample_size, method_opt => 'for all columns size skewonly', cascade => TRUE, degree => 6);

select ' '||to_char(sysdate, 'YYYY-MON-DD HH24:MI:SS')||' - Gather Table Statistics: CDS.COMM_OPT_OUT.' from dual;

EXEC DBMS_STATS.gather_table_stats('CDS', 'COMM_OPT_OUT', estimate_percent => dbms_stats.auto_sample_size, method_opt => 'for all columns size skewonly', cascade => TRUE, degree => 6);

select ' '||to_char(sysdate, 'YYYY-MON-DD HH24:MI:SS')||' - Gather Table Statistics: CDS.LOYALTY_HISTORY.' from dual;

EXEC DBMS_STATS.gather_table_stats('CDS', 'LOYALTY_HISTORY', estimate_percent => dbms_stats.auto_sample_size, method_opt => 'for all columns size skewonly', cascade => TRUE, degree => 6);

select ' '||to_char(sysdate, 'YYYY-MON-DD HH24:MI:SS')||' - Gather Table Statistics: CDS.PERSON.' from dual;

EXEC DBMS_STATS.gather_table_stats('CDS', 'PERSON', estimate_percent => dbms_stats.auto_sample_size, method_opt => 'for all columns size skewonly', cascade => TRUE, degree => 6);

select ' '||to_char(sysdate, 'YYYY-MON-DD HH24:MI:SS')||' - Gather Table Statistics: CDS.HOUSEHOLD.' from dual;

EXEC DBMS_STATS.gather_table_stats('CDS', 'HOUSEHOLD', estimate_percent => dbms_stats.auto_sample_size, method_opt => 'for all columns size skewonly', cascade => TRUE, degree => 6);

select ' '||to_char(sysdate, 'YYYY-MON-DD HH24:MI:SS')||' - Gather Table Statistics: CDS.CONSUMER.' from dual;

EXEC DBMS_STATS.gather_table_stats('CDS', 'CONSUMER', estimate_percent => dbms_stats.auto_sample_size, method_opt => 'for all columns size skewonly', cascade => TRUE, degree => 6);

select ' '||to_char(sysdate, 'YYYY-MON-DD HH24:MI:SS')||' - Gather Table Statistics: CDS.CONSUMER_HISTORY.' from dual;

EXEC DBMS_STATS.gather_table_stats('CDS', 'CONSUMER_HISTORY', estimate_percent => dbms_stats.auto_sample_size, method_opt => 'for all columns size skewonly', cascade => TRUE, degree => 6);

select ' '||to_char(sysdate, 'YYYY-MON-DD HH24:MI:SS')||' - Gather Table Statistics: CDS.COMMUNICATION.' from dual;

EXEC DBMS_STATS.gather_table_stats('CDS', 'COMMUNICATION', estimate_percent => dbms_stats.auto_sample_size, method_opt => 'for all columns size skewonly', cascade => TRUE, degree => 6);

select 'End: '||to_char(sysdate, 'YYYY-MON-DD HH24:MI:SS') from dual;

spo off;

EXIT
EOF

cat $REPORT | mail -s "Oracle JGS: Special Statistics for Some Important Tables" dmitryr@appcard.com z9o8w5q8u5t5g6z2@appcard.slack.com
