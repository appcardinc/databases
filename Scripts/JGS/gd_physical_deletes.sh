#!/bin/sh

#
# Set up the env
ORACLE_BASE=/app/oracle
ORACLE_HOME=/app/oracle/product/10.2.0/orajgs01
ORACLE_SID=orajgs01
export ORACLE_BASE ORACLE_HOME ORACLE_SID
ORAENV_ASK=NO
OA_PRD=$ORACLE_BASE/admin/orajgs01
LD_LIBRARY_PATH=$ORACLE_HOME/lib:/usr/lib
PATH=$PATH:$ORACLE_HOME/bin:/usr/sbin
export OA_PRD LD_LIBRARY_PATH PATH ORAENV_ASK

export REPORT=/tmp/gd_physical_deletes_$ORACLE_SID.rpt

# .oraenv
#
#
# ALL TABLESPACE AND DATAFILES
#
#

echo "Oracle JGS: GD Physical Deletes Started :: $(date)" | mail -s "Oracle JGS: GD Physical Deletes Started :: $(date)" dmitryr@appcard.com edo@appcard.com syamaguchi@appcard.com emoto@greenstamp.co.jp jb-support-nw@greenstamp.co.jp

$ORACLE_HOME/bin/sqlplus -s /nolog <<EOF
connect / as sysdba

SET SERVEROUT ON
set head off
set feedback off

spo $REPORT

DECLARE
  TYPE var_array_type IS TABLE OF COMMON.global_data_definition.variable_id%TYPE INDEX BY BINARY_INTEGER;
  var_ids var_array_type;

  C1 BINARY_INTEGER; C2 BINARY_INTEGER; C3 BINARY_INTEGER;
  t1 BINARY_INTEGER; t2 BINARY_INTEGER; t3 BINARY_INTEGER; t4 BINARY_INTEGER;
BEGIN
  t1 := dbms_utility.get_time;

--  SELECT distinct variable_id BULK COLLECT INTO var_ids FROM common.global_data_definition WHERE deleted < (SYSDATE - 30);

--	SELECT distinct gdd.variable_id BULK COLLECT INTO var_ids FROM common.global_data_definition gdd, cds.global_data gd 
--	WHERE gdd.variable_id = gd.variable_id AND gd.deleted is not null  AND gdd.deleted < (SYSDATE - 30);

	SELECT /*+ parallel(gdd, 4) */ distinct gdd.variable_id BULK COLLECT INTO var_ids FROM common.global_data_definition gdd, cds.global_data gd 
	WHERE gdd.variable_id = gd.variable_id AND gd.deleted is not null  AND gdd.deleted < (SYSDATE - 1);

  C1:= SQL%ROWCOUNT; -- Store count in variable

  dbms_output.put_line('C1 = '|| C1);

  IF (C1 > 0) THEN

  t2 := dbms_utility.get_time;

  C2:=0;
  -- Physically delete cds.global_data
  FOR i IN var_ids.FIRST..var_ids.LAST
  LOOP
    dbms_output.put_line('Deleting variableId='||var_ids(i));
    DELETE from cds.global_data WHERE deleted is not null AND variable_id = var_ids(i);

    C2:= C2 + SQL%ROWCOUNT; -- Store count in variable

    COMMIT;
  END LOOP;

  t3 := dbms_utility.get_time;


  dbms_output.put_line('--------------');
  dbms_output.put_line('Execution Time');
  dbms_output.put_line('--------------');
  dbms_output.put_line('Getting '|| C1 ||' variable ids in ' || TO_CHAR((t2 - t1)) ||' hsec');
  dbms_output.put_line('Deleted '|| C2 ||' cds.global_data in ' || TO_CHAR((t3 - t2)) ||' hsec');
--  dbms_output.put_line('Deleted '|| C3 ||' common.global_data_def in ' || TO_CHAR((t4 - t3)) ||' hsec');

  ELSE

    dbms_output.put_line('Nothing To Delete');

  END IF;

END;
/

spo off;

exit
EOF
#############################################

cat $REPORT | mail -s "Oracle JGS: GD Physical Deletes Trace :: $(date)" dmitryr@appcard.com edo@appcard.com syamaguchi@appcard.com emoto@greenstamp.co.jp jb-support-nw@greenstamp.co.jp

echo "Oracle JGS: GD Physical Deletes Ended :: $(date)" | mail -s "Oracle JGS: GD Physical Deletes Ended :: $(date)" dmitryr@appcard.com edo@appcard.com syamaguchi@appcard.com emoto@greenstamp.co.jp jb-support-nw@greenstamp.co.jp

