rman log=restore_`date +%d%m%y%H%M`.log <<EOF
connect target /
run {
restore database;
}
exit;
EOF
