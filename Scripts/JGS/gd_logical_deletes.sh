#!/bin/sh

#
# Set up the env
ORACLE_BASE=/app/oracle
ORACLE_HOME=/app/oracle/product/10.2.0/orajgs01
ORACLE_SID=orajgs01
export ORACLE_BASE ORACLE_HOME ORACLE_SID
ORAENV_ASK=NO
OA_PRD=$ORACLE_BASE/admin/orajgs01
LD_LIBRARY_PATH=$ORACLE_HOME/lib:/usr/lib
PATH=$PATH:$ORACLE_HOME/bin:/usr/sbin
export OA_PRD LD_LIBRARY_PATH PATH ORAENV_ASK
# .oraenv
#
#
# ALL TABLESPACE AND DATAFILES
#
#
$ORACLE_HOME/bin/sqlplus -s /nolog <<EOF
connect / as sysdba

DECLARE
   TYPE var_array_type IS TABLE OF COMMON.global_data_definition.variable_id%TYPE INDEX BY BINARY_INTEGER;
   var_ids var_array_type;

   N1 BINARY_INTEGER; N2 BINARY_INTEGER; N3 BINARY_INTEGER;
   t1 BINARY_INTEGER; t2 BINARY_INTEGER; t3 BINARY_INTEGER; t4 BINARY_INTEGER;
BEGIN
   t1 := dbms_utility.get_time;

SELECT variable_id BULK COLLECT INTO var_ids FROM 
(
  SELECT distinct variable_id FROM  -- Expired promotions
  (
    SELECT gdd.variable_id, p.promotion_id, p.end_date AS expiration
      FROM PMT.Promotions p
        JOIN PMT.definition_variable dv ON (
          (
            dv.owner_id = p.promotion_id AND 
            dv.owner_type = 'P'
          ) OR
          (
            dv.owner_id = p.template_id AND 
            dv.owner_type = 'T'
          )
        )
        JOIN COMMON.global_data_definition gdd ON 
          gdd.variable_id = dv.global_data_id
          AND TRIM(TRANSLATE(SUBSTR(gdd.name,1,4), ' 0123456789',' ')) is null
          AND gdd.SCOPE != 7
	  AND gdd.variable_id > 999

    UNION
    SELECT gdd.variable_id, p.promotion_id, p.end_date AS expiration
      FROM PMT.Promotions p
        JOIN PMT.definition_benefit db ON db.promotion_id = p.promotion_id
        JOIN PMT.definition_variable dv ON 
          dv.owner_id = db.definition_benefit_id AND 
          dv.owner_type = 'B'
        JOIN COMMON.global_data_definition gdd ON 
          gdd.variable_id = dv.global_data_id
          AND TRIM(TRANSLATE(SUBSTR(gdd.name,1,4), ' 0123456789',' ')) is null
          AND gdd.SCOPE != 7
	  AND gdd.variable_id > 999

    UNION
    SELECT gdd.variable_id, p.promotion_id, p.end_date AS expiration
      FROM PMT.Promotions p
        JOIN PMT.definition_list dl ON 
          dl.promotion_id = p.promotion_id
        JOIN COMMON.global_data_definition gdd ON 
          gdd.variable_id = dl.global_data_id
          AND TRIM(TRANSLATE(SUBSTR(gdd.name,1,4), ' 0123456789',' ')) is null
          AND gdd.SCOPE != 7
	  AND gdd.variable_id > 999

    UNION
      SELECT gdd.variable_id, p.promotion_id, p.end_date AS expiration
        FROM PMT.Promotions p
          JOIN PMT.Coupons c ON c.promotion_id = p.promotion_id
          JOIN PMT.definition_benefit db ON 
            db.promotion_id = c.promotion_id AND 
            db.benefit_id = c.coupon_id AND 
            db.benefit_type = 'U'
          JOIN PMT.Coupon_Overlays co ON co.coupon_id = c.coupon_id
          JOIN COMMON.global_data_definition gdd ON 
            gdd.variable_id = co.variable_id
            AND TRIM(TRANSLATE(SUBSTR(gdd.name,1,4), ' 0123456789',' ')) is null
            AND gdd.SCOPE != 7
	    AND gdd.variable_id > 999
  ) WHERE expiration < (SYSDATE - 30)
  MINUS
  SELECT distinct variable_id FROM -- Non-expired promotions
  (
    SELECT gdd.variable_id, p.promotion_id, p.end_date AS expiration
      FROM PMT.Promotions p
        JOIN PMT.definition_variable dv ON (
          (
            dv.owner_id = p.promotion_id AND 
            dv.owner_type = 'P'
          ) OR
          (
            dv.owner_id = p.template_id AND 
            dv.owner_type = 'T'
          )
        ) AND
          dv.deleted IS NULL
        JOIN COMMON.global_data_definition gdd ON 
          gdd.variable_id = dv.global_data_id
	  AND gdd.SCOPE != 7
	  AND gdd.deleted is null
	  AND gdd.variable_id > 999

    UNION
    SELECT gdd.variable_id, p.promotion_id, p.end_date AS expiration
      FROM PMT.Promotions p
        JOIN PMT.definition_benefit db ON 
          db.promotion_id = p.promotion_id AND
          db.deleted IS NULL
        JOIN PMT.definition_variable dv ON 
          dv.owner_id = db.definition_benefit_id AND 
          dv.owner_type = 'B' AND
          dv.deleted IS NULL
        JOIN COMMON.global_data_definition gdd ON 
          gdd.variable_id = dv.global_data_id
	  AND gdd.SCOPE != 7
	  AND gdd.deleted is null
	  AND gdd.variable_id > 999

    UNION
    SELECT gdd.variable_id, p.promotion_id, p.end_date AS expiration
      FROM PMT.Promotions p
        JOIN PMT.definition_list dl ON 
          dl.promotion_id = p.promotion_id AND
          dl.deleted IS NULL
        JOIN COMMON.global_data_definition gdd ON 
          gdd.variable_id = dl.global_data_id
	  AND gdd.SCOPE != 7
	  AND gdd.deleted is null
	  AND gdd.variable_id > 999

    UNION
    SELECT gdd.variable_id, p.promotion_id, p.end_date AS expiration
      FROM PMT.Promotions p
        JOIN PMT.Coupons c ON c.promotion_id = p.promotion_id
        JOIN PMT.definition_benefit db ON 
          db.promotion_id = c.promotion_id AND 
          db.benefit_id = c.coupon_id AND 
          db.benefit_type = 'U'
        JOIN PMT.Coupon_Overlays co ON co.coupon_id = c.coupon_id
        JOIN COMMON.global_data_definition gdd ON 
          gdd.variable_id = co.variable_id
	  AND gdd.SCOPE != 7
	  AND gdd.deleted is null
	  AND gdd.variable_id > 999
  ) WHERE expiration >= (SYSDATE - 30)
);

   N1:= SQL%ROWCOUNT; -- Store count in variable

   t2 := dbms_utility.get_time;

   -- Logically delete cds.global_data
   FORALL i IN var_ids.FIRST..var_ids.LAST
    UPDATE cds.global_data set deleted = systimestamp WHERE variable_id = var_ids(i) AND deleted is null;
 
   N2:= SQL%ROWCOUNT; -- Store count in variable

   COMMIT;
   
   t3 := dbms_utility.get_time;
 
   -- Logically delete common.global_data_definition  
   FORALL j IN var_ids.FIRST..var_ids.LAST
    UPDATE common.global_data_definition set deleted = systimestamp WHERE variable_id = var_ids(j) AND deleted is null;

   N3:= SQL%ROWCOUNT; -- Store count in variable

   COMMIT;

   t4 := dbms_utility.get_time;

   dbms_output.put_line('--------------');
   dbms_output.put_line('Execution Time');
   dbms_output.put_line('--------------');
   dbms_output.put_line('Getting '|| N1 ||' variable ids in ' || TO_CHAR((t2 - t1)) ||' hsec');
   dbms_output.put_line('Updated '|| N2 ||' cds.global_data in ' || TO_CHAR((t3 - t2)) ||' hsec');
   dbms_output.put_line('Updated '|| N3 ||' common.global_data_def in ' || TO_CHAR((t4 - t3)) ||' hsec');

END;
/

exit
EOF
#############################################

