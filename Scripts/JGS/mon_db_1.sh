#!/bin/bash
#*****************************************************************************************************
#*
#*  Name: mon_db.sh
#*
#*  Description: Database monitoring script
#*
#*     Check if: (1) all listeners are up
#*               (2) all databases are up
#*               (3) there are no recent errors in alert log files
#*               (4) there is more than 10% free space in all tablespaces
#*
#*  Mail a message to padepu mail alias defined in $HOME/.mailrc,
#*  if any errors were encountered
#*
#*  * Databases are listed in $HOME/.oratab(clone or symbolic link to /etc/oratab)
#   * Listeners are listed in $HOME/.listab
#*
#*****************************************************************************************************
#
# Source bash profile

. $HOME/.bash_profile_1
. $HOME/.maillist

export ERRORS_FOUND='FALSE'
export TEMP_FILE=/tmp/mon_db.tmp
export WORKING_DIR=$HOME/ops                         # Working directory
export LOG_LINES=15000                               # Define max lines in the alert log
# List of users who are to be notified is padepu, which is an alias in $HOME/.mailrc

# $HOME/.oratab - clone(or symbolic link) of /etc/oratab
# NB: Databases that have flag set to 'N' are not to be checked
export ORATAB=$HOME/.oratab

# $HOME/.listab - a list of active listeners
# Format is:
# instance:listener:[Y/N]

# NB: Listeners that have flag set to 'N' are not being checked
export LISTAB=$HOME/.listab

cd $WORKING_DIR
# (1) check if all listeners are up
# If 'N', then ignore listener down
#
for LISTENER in `cat $LISTAB | grep -v "#" | grep -i ":Y" | awk -F: {' print $2 '}`
do
   # Ignore case in listener name
   IS_LISTENER_DOWN=`ps auwwwx | egrep -i "tnslsnr $LISTENER -inherit" | grep -v grep > /dev/null 2>&1; echo $?`
   if [ $IS_LISTENER_DOWN -eq 1 ]
   then
      ERRORS_FOUND='TRUE'
echo 'LISTENER IS DOWN'
      mail -s "ORACLE ALERT: `hostname` => Listener $LISTENER is down!" $mail_list < /dev/null > /dev/null 2>&1
   fi
done

# Check for each of all running databases:
#    (2) if database is up and running
#    (3) if there are no recent errors in the alert log file
#    (4) if free space in all tablespaces is more than 10%

# Loop through all databases listed in $HOME/.oratab
# Ignore the ones with flag set to :N
for DB_ENTRY in `cat $ORATAB | grep -v \# | grep -v \* | grep -i ":Y" | grep -v ^$`
do
   DB=`echo $DB_ENTRY | awk -F: {' print $1 '}`;

   ORACLE_HOME=`echo $DB_ENTRY | awk -F: {' print $2 '}`
   ORACLE_BASE=`echo $ORACLE_HOME | sed -e s:/product/.*::g`

   ORACLE_SID=$DB;
   DBA=$ORACLE_BASE/admin
  
   export ORACLE_HOME
   export ORACLE_SID

#   . $HOME/.bash_profile ORACLE_SID

   # (2) check if database is up and running
   IS_DB_DOWN=`ps auwwwx | grep  "ora_pmon_$DB$" | grep -v grep > /dev/null 2>&1; echo $?`
   if [ $IS_DB_DOWN -eq 1 ]
   then
      ERRORS_FOUND='TRUE'
      mail -s "ORACLE ALERT: `hostname` => Database $DB is down!" $mail_list < /dev/null > /dev/null 2>&1
      # Stop here and continue with another database
      continue
   fi

   # (3) check if there are recent errors in database's alert log file
   ALERT_LOG=$DBA/$ORACLE_SID/bdump/alert_$ORACLE_SID.log
   # Create a new alert log and rename old one, if it reached more than $LOG_LINES
   # Then, reset LAST_COUNT
   LINES=`cat $DBA/$ORACLE_SID/bdump/alert_$ORACLE_SID.log | wc -l`
   if [ $LINES -ge $LOG_LINES ]; then
      mv    $DBA/$ORACLE_SID/bdump/alert_$ORACLE_SID.log  $DBA/$ORACLE_SID/bdump/alert_$ORACLE_SID.log.`date '+%m%d%y'`;
     touch $DBA/$ORACLE_SID/bdump/alert_$ORACLE_SID.log;
      LAST_COUNT=0
   fi

   # If the file that keeps track of how many lines were last time
   # processed does not exist, then initialize it
   if [ ! -f "./last_count_$DB" ]
   then
      echo 1 > last_count_$DB
   fi
   
   # Refresh COUNT variables
   CURR_COUNT=`cat $ALERT_LOG |wc -l`
   LAST_COUNT=`cat last_count_$DB`

   #  In case somebody has cleared out the alert log,
   # (there are less lines in the alert log than last time), then re-initialize
   if [ $CURR_COUNT -lt $LAST_COUNT ]
   then
      echo 1 > last_count_$DB
      LAST_COUNT=`cat last_count_$DB`
   fi
   # Create a file with only the last lines that were not checked before
   let LAST_COUNT=$LAST_COUNT+1
   tail -$LAST_COUNT $ALERT_LOG  > last_alert_$DB
   
   # Add a header message to mail body
   echo "Please, check for new errors in the alert log for database $DB at: $ALERT_LOG" > error_alert_$DB
   echo "" >> error_alert_$DB

   # grep the file for 'ORA-' and ignore standby errors
   egrep 'ORA-' last_alert_$DB | egrep -v "ORA-12152"\|"ORA-16055"\|"ORA-03113"\|"ORA-12570"\|"ORA-03114"\|"ORA-01089"\|"ORA-12541"\|"ORA-1109"\|"ORA-16136"\|"ORA-1531" > error_alert_$DB
   status=$?

   # Set the next starting check at the max. number of lines currently available in alert log ...
   echo $CURR_COUNT > last_count_$DB
   
   # Notify DBA if errors found
   if [ $status -eq 0 ]
   then
      ERRORS_FOUND='TRUE'
      mail -s "ORACLE ALERT: `hostname` => Errors in alert log database $DB!" $mail_list < error_alert_$DB > /dev/null 2>&1
      # gzip -f /app/oracle/admin/orajgs01/bdump/alert_orajgs01.log
       mv    $DBA/$ORACLE_SID/bdump/alert_$ORACLE_SID.log  $DBA/$ORACLE_SID/bdump/alert_$ORACLE_SID.log.`date '+%m%d%y'`;
      touch /app/oracle/admin/orajgs01/bdump/alert_orajgs01.log
   fi

   # (4) check if free space in all tablespaces of this database is more than 10%

   # Skip if this is a physical standby, since database is not open for query
   sqlplus -s "/ as sysdba" <<-EOF > $HOME/ops/db_role
   set verify off
   set head off
   select database_role from v\$database;
EOF
   db_role=`grep -v '^$' $HOME/ops/db_role`
   if [  "$db_role" = 'PHYSICAL STANDBY' ]; then
      continue
   fi
  

   sqlplus -s "/ as sysdba" <<-EOF > /dev/null 2>&1
        set feed off
        set linesize 100
        set pagesize 200
        spool error_tsp_$DB.lst

        SELECT F.TABLESPACE_NAME,
        TO_CHAR ((T.TOTAL_SPACE - F.FREE_SPACE),'999,999') "Used(MB)",
        TO_CHAR (F.FREE_SPACE, '999,999') "Free(MB)",
        TO_CHAR (T.TOTAL_SPACE, '999,999') "Total(MB)",
        TO_CHAR ((ROUND ((F.FREE_SPACE/T.TOTAL_SPACE)*100)),'999')||' %' "%Free"
        FROM   (
        SELECT  TABLESPACE_NAME,
        ROUND (SUM (BLOCKS*(SELECT VALUE/1024 FROM V\$PARAMETER WHERE NAME = 'db_block_size')/1024)) FREE_SPACE
        FROM DBA_FREE_SPACE
        GROUP BY TABLESPACE_NAME
        ) F,
        (
        SELECT  TABLESPACE_NAME,
        ROUND (SUM (BYTES/1048576)) TOTAL_SPACE
        FROM DBA_DATA_FILES
        GROUP BY TABLESPACE_NAME
        ) T,
        DBA_TABLESPACES S
        WHERE F.TABLESPACE_NAME = T.TABLESPACE_NAME
        AND   T.TABLESPACE_NAME = S.TABLESPACE_NAME
        AND   S.CONTENTS <> 'UNDO'
        AND (ROUND ((F.FREE_SPACE/T.TOTAL_SPACE)*100)) < 10
        UNION
        SELECT F.TABLESPACE_NAME,
        TO_CHAR ((T.TOTAL_SPACE - F.FREE_SPACE),'999,999') "Used(MB)",
        TO_CHAR (F.FREE_SPACE, '999,999') "Free(MB)",
        TO_CHAR (T.TOTAL_SPACE, '999,999') "Total(MB)",
        TO_CHAR ((ROUND ((F.FREE_SPACE/T.TOTAL_SPACE)*100)),'999')||' %' "%Free"
        FROM   (
        SELECT  TABLESPACE_NAME,
        ROUND (SUM (BLOCKS*(SELECT VALUE/1024 FROM V\$PARAMETER WHERE NAME = 'db_block_size')/1024)) FREE_SPACE
        FROM DBA_FREE_SPACE
        GROUP BY TABLESPACE_NAME
        ) F,
        (
        SELECT  TABLESPACE_NAME,
        ROUND (SUM (BYTES/1048576)) TOTAL_SPACE
        FROM DBA_DATA_FILES
        GROUP BY TABLESPACE_NAME
        ) T,
        V\$UNDOSTAT U, TS\$ S
        WHERE F.TABLESPACE_NAME = T.TABLESPACE_NAME
        AND   T.TABLESPACE_NAME = S.NAME
        AND   S.TS# = U.UNDOTSN
        AND   U.NOSPACEERRCNT > 0
        AND (ROUND ((F.FREE_SPACE/T.TOTAL_SPACE)*100)) < 10;

        spool off
        exit
EOF

   # If the error file exists and has size > 0, then send mail
   if [ -s error_tsp_$DB.lst ]
   then
      echo "Database: $DB" >> /tmp/mail_message
      cat error_tsp_$DB.lst >> /tmp/mail_message
      mv /tmp/mail_message error_tsp_$DB.lst
      ERRORS_FOUND='TRUE'
      mail -s "ORACLE ALERT: Tablespace space low for database: $DB" $mail_list < error_tsp_$DB.lst > /dev/null 2>&1
   fi

done

# Clean-up and exit
# pk 4/7/2016 rm -f last_alert_* error_alert_* error_tsp_* $TEMP_FILE


exit 0
