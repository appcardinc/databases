#!/bin/sh
#################################################################
# Script - fragmented_objects.sh                                   #
# Owner - Dobler Consulting LLC                                 #
# Date - 09/05/2018                                             #
#################################################################
ORACLE_BASE=/app/oracle
ORACLE_HOME=/app/oracle/product/10.2.0/orajgs01
ORACLE_SID=orajgs01
export ORACLE_BASE ORACLE_HOME ORACLE_SID ORAENV_ASK=NO
OA_PRD=$ORACLE_BASE/admin/orajgs01
LD_LIBRARY_PATH=$ORACLE_HOME/lib:/usr/lib
PATH=$PATH:$ORACLE_HOME/bin:/usr/sbin

#MAILLIST=jearnest@doblerllc.com
MAILLIST="ProLogicSupport@doblerllc.com,smorales@appcard.com,chak@appcard.com,ricardog@appcard.com"
SPOOLFILE=/tmp/${ORACLE_SID}_fragmented_objects.list
HOST="$HOSTNAME"
TMPFILE=/tmp/${ORACLE_SID}_fragmented_objects.tmp

#source $PROFILE

if [ -f $TMPFILE ]; then
rm -f $TMPFILE
fi

if [ -f $SPOOLFILE ]; then
rm -f $SPOOLFILE
fi

lobj=`sqlplus -s / as sysdba <<EOF
set head off
set feedback off
select count(*)
from   table(dbms_space.asa_recommendations('FALSE','FALSE','FALSE'))
where 100*RECLAIMABLE_SPACE/ALLOCATED_SPACE > 50
;
exit
EOF`

if [ $lobj -ge 1 ]; then

echo "To: $MAILLIST" > $TMPFILE
echo "From: $HOST " >> $TMPFILE
echo "Subject:  Fragmented Objects Report for database $ORACLE_SID on server $HOST" >> $TMPFILE 
echo "Content-type: text/html" >> $TMPFILE 
echo "MIME-Version: 1.0" >> $TMPFILE
sqlplus -s /nolog << EOF
connect / as sysdba
 set feed off
 set verify off
 set term off
 set echo off
 set trimout on
 set trimspool on 
set markup html on spool on PREFORMAT OFF ENTMAP OFF -
 HEAD "<TITLE>Fragmented Objects Report</TITLE> - <STYLE type='text/css'> -
   BODY {background: white} -
   TH {background: white} -
   TD {background: white} -
</STYLE>" -
BODY 'TEXT="black"' -
TABLE "BORDER='1'"

spool $SPOOLFILE
prompt <H2><center><u> List of Fragmented Objects </u><center></H1>

set heading on
set linesize 150
set pagesize 10000
column segment_owner format a20
column setment_name  format a30
column alloc_mb  heading "Allocated Space (MB)" format 99,999,999.99
column used_mb  heading "Used Space (MB)" format 99,999,999.99
column rclm_mb  heading "Reclaimable Space (MB)" format 99,999,999.99
column rclm_pct  heading "Reclaimable %" format 999.99
column chain_rowexcess heading "Excess Chain Rows %" format 999.99

select segment_owner,segment_name,round(ALLOCATED_SPACE/1024/1024,2) alloc_mb,
       round(USED_SPACE/1024/1024,2) used_mb, round(RECLAIMABLE_SPACE/1024/1024,2) rclm_mb,
       case when round(100*RECLAIMABLE_SPACE/ALLOCATED_SPACE,2)  > 50 then
         '<FONT COLOR="red">' || to_char(100*RECLAIMABLE_SPACE/ALLOCATED_SPACE,'999') || '</FONT>'
       else
         to_char(100*RECLAIMABLE_SPACE/ALLOCATED_SPACE,'999')
      end rclm_pct,CHAIN_ROWEXCESS,c1,c2,c3
from   table(dbms_space.asa_recommendations())
--rclm_pct will be a command line parameter, Using 25% as the default
where 100*RECLAIMABLE_SPACE/ALLOCATED_SPACE > 50
--segment_name like 'ROW%'
order by rclm_pct desc
/
spool off
exit
EOF

cat $SPOOLFILE >> $TMPFILE
/usr/sbin/sendmail -t < $TMPFILE

fi
exit 0
