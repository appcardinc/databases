#!/bin/sh
#
# Set up the env
ORACLE_BASE=/app/oracle
ORACLE_HOME=/app/oracle/product/10.2.0/orajgs01
ORACLE_SID=orajgs01
export ORACLE_BASE ORACLE_HOME ORACLE_SID
ORAENV_ASK=NO
OA_PRD=$ORACLE_BASE/admin/orajgs01
LD_LIBRARY_PATH=$ORACLE_HOME/lib:/usr/lib
PATH=$PATH:$ORACLE_HOME/bin:/usr/sbin
export OA_PRD LD_LIBRARY_PATH PATH ORAENV_ASK
#

export ORACLE_SID=orajgs01

export REPORT=/tmp/calculate_statistics_$ORACLE_SID.rpt
export SERVER_NAME=`hostname`

sqlplus -s "/ as sysdba" << EOF

set head off
set feedback off
spool $REPORT

select 'Oracle JGS: Calculate Statistics' from dual;

select 'Started: '||to_char(sysdate, 'YYYY-MON-DD HH24:MI:SS') from dual;

select ' '||to_char(sysdate, 'YYYY-MON-DD HH24:MI:SS')||' - Gather Schema Statistics: CDS' from dual;

exec dbms_stats.gather_schema_stats(ownname=>'CDS', options=>'GATHER AUTO', estimate_percent=>dbms_stats.auto_sample_size, method_opt=>'for all columns size repeat', degree=>6);

select ' '||to_char(sysdate, 'YYYY-MON-DD HH24:MI:SS')||' - Gather Schema Statistics: PMT' from dual;

exec dbms_stats.gather_schema_stats(ownname=>'PMT', options=>'GATHER AUTO', estimate_percent=>dbms_stats.auto_sample_size, method_opt=>'for all columns size repeat', degree=>6);

select ' '||to_char(sysdate, 'YYYY-MON-DD HH24:MI:SS')||' - Gather Schema Statistics: TLOG' from dual;

exec dbms_stats.gather_schema_stats(ownname=>'TLOG', options=>'GATHER AUTO', estimate_percent=>dbms_stats.auto_sample_size, method_opt=>'for all columns size repeat', degree=>6);

select 'End: '||to_char(sysdate, 'YYYY-MON-DD HH24:MI:SS') from dual;

spool off
EXIT
EOF
    
echo 'Oracle JGS DB Calculate Statistics' | mail -s "Oracle JGS DB Calculate Statistics " dmitryr@appcard.com < $REPORT

