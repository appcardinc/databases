#!/bin/sh
#################################################################
# Script - top_twenty_objects_by_size.sh                                   #
# Owner - Dobler Consulting LLC                                 #
# Date - 09/11/2018                                             #
#################################################################
ORACLE_BASE=/app/oracle
ORACLE_HOME=/app/oracle/product/10.2.0/orajgs01
ORACLE_SID=orajgs01
export ORACLE_BASE ORACLE_HOME ORACLE_SID ORAENV_ASK=NO
OA_PRD=$ORACLE_BASE/admin/orajgs01
LD_LIBRARY_PATH=$ORACLE_HOME/lib:/usr/lib
PATH=$PATH:$ORACLE_HOME/bin:/usr/sbin

#MAILLIST=jearnest@doblerllc.com
MAILLIST="ProLogicSupport@doblerllc.com,smorales@appcard.com,chak@appcard.com,ricardog@appcard.com"
SPOOLFILE=/tmp/${ORACLE_SID}_top_twenty_objects.sql
HOST="$HOSTNAME"
TMPFILE=/tmp/${ORACLE_SID}_top_twenty_objects.tmp

#source $PROFILE


if [ -f $TMPFILE ]; then
rm -f $TMPFILE
fi

if [ -f $SPOOLFILE ]; then
rm -f $SPOOLFILE
fi

echo "To: $MAILLIST" > $TMPFILE
echo "From: $HOST " >> $TMPFILE
echo "Subject: Top Twenty Objects by Size in database $ORACLE_SID on server $HOST" >> $TMPFILE
echo "Content-type: text/html" >> $TMPFILE
echo "MIME-Version: 1.0" >> $TMPFILE
 sqlplus -s /nolog << EOF 
connect / as sysdba
 set feed off
 set verify off
 set term off
 set echo off
 set trimout on
 set trimspool on 
spool $SPOOLFILE
 set markup html on PREFORMAT OFF ENTMAP OFF - 
 HEAD "<TITLE>Top Twenty Objects By Size</TITLE> - 
 <STYLE type='text/css'> -
   BODY {background: white} -
   TH {background: white} -
   TD {background: white} -
</STYLE>" -
BODY 'TEXT="black"' -
TABLE "BORDER='1'"

prompt <H2><center><u>Top Twenty Objects By Size in $ORACLE_SID</u></center></H2>
set linesize 300
set pagesize 10000
column owner heading "Segment Owner" format a30
column segment_name  heading "Segment Name" format a50
column segment_type  heading "Segment Type" format a50
column GBs heading "Size in GB" format 999,999,999.99
column num_rows  heading "Numer of Rows" format 999,999,999,999
column sorts  format 99,999,999,999

select seg.owner,seg.segment_name,seg.segment_type,seg.GBs GBs,
case seg.segment_type when 'TABLE' then tb.num_rows else NULL end num_rows
from
(select a.owner owner,a.segment_name segment_name,a.GBs GBs,a.segment_type
from
(Select owner,segment_name,segment_type, round(bytes/1024/1024/1024,2) GBs
from dba_segments
where owner not in ('SYS','SYSTEM','PERFSTAT','OUTLN',
'CTXSYS','AURORA$JIS$UTILITY$') order by nvl(bytes,0) desc,segment_name asc) a
where rownum<21) seg, dba_tables tb, dba_objects ob
where seg.segment_name = tb.table_name (+)
and seg.segment_name= ob.object_name
order by seg.gbs desc ,seg.segment_name asc
/
spool off
exit
EOF

cat $SPOOLFILE >> $TMPFILE
/usr/sbin/sendmail -t < $TMPFILE
