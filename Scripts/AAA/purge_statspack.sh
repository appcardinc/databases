#!/bin/sh -x
#*************************************************************************
#*
#*  Name: purge_statspack.sh
#*
#*  Description: Purge old statspack data(keep two weeks)
#*************************************************************************
#
# Source bash profile

. $HOME/.bash_profile

if [ $# -ne 1 ];then
   echo 'Usage:$0 SID'
   exit 1;
fi

SID=`echo $1 | tr A-Z a-z`
n=7
#sqlplus perfstat/`get_password.sh $SID perfstat`@$SID <<-EOF
sqlplus perfstat/perfstat <<-EOF
execute statspack.purge($n);
commit;
EOF

# zip rman backups
gzip /dump/prdhob/rman/ora_backup_db_PRDHOB_S*

