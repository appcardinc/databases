#!/bin/sh
#######################################################################################################################
###
###           This job analyzes all schemas for a database except SYS and SYSTEM
###
###  PREREQUISITE: create table to save current statistics              
###
###            exec dbms_stats.create_stat_table('SYS', 'STATTAB');
###
###  Statement to execute: 
###
###             exec dbms_stats.gather_schema_stats(ownname           => s.schema,
###                                                 cascade           => TRUE,
###                                                 method_opt        => 'FOR ALL INDEXED COLUMNS SIZE 1',
###                                                 degree            => 4,
###                                                 estimate_percent  => 25,
###                                                 statown           => 'SYS', 
###                                                 stattab           => 'STATTAB')
###  PARAMETER         DESCRIPTION
###  ---------         --------------------------------------------------------------------
###  ownname           analyze ALL tables  for this particular schema
###  cascade           analyze ALL indexes
###  method_opt        create  histograms:
###                      'FOR ALL COLUMNS SIZE 1'         - for all columns (default, not recommended)
###                      'FOR ALL INDEXED COLUMNS SIZE 1' - for indexes columns only;
###  degree            degree of parallelism, if desired
###  estimate_percent  this parameter could be set to 'DBMS_STATS.AUTO_SAMPLE_SIZE', however the latter one has some issues
###  statown, stattab  owner & table name where current statistics will be saved.
###                    This is an insurance policy in case of performance degradation as a result of this job execution.
###                    To create stattab, run: exec dbms_stats.create_stat_table('SYS', 'STATTAB');
###
###
#######################################################################################################################
. ~/.bash_profile
. ~/.maillist
#
# Check if SID is specified
#
if [ $# -eq 0 ];then
   echo 'You must enter SID'
   exit 1;
fi
SID=`echo $1 | tr A-Z a-z`
export ORACLE_SID=$SID
export options=GATHER
export method_opt='FOR ALL COLUMNS SIZE AUTO'
export LOGDIR=/home/oracle/scripts/log
export LOGFILE=$SID'_dbstats_'`date +%d%m%H%M`_$$'.log'
export CASCADE=TRUE
sqlplus /nolog <<-EOF
connect / as sysdba
spool $LOGDIR/$LOGFILE
ANALYZE TABLE CDS.CONSUMER_HISTORY COMPUTE STATISTICS
/
ANALYZE TABLE CDS.LOYALTY_CARD COMPUTE STATISTICS
/
ANALYZE TABLE CDS.PROFILE COMPUTE STATISTICS
/
ANALYZE TABLE CDS.AAA_QA_ACOUNTS COMPUTE STATISTICS
/
ANALYZE TABLE CDS.LOYALTY_HISTORY COMPUTE STATISTICS
/
ANALYZE TABLE CDS.HOUSEHOLD COMPUTE STATISTICS
/
ANALYZE TABLE CDS.ENTITY COMPUTE STATISTICS
/
ANALYZE TABLE CDS.PERSON COMPUTE STATISTICS
/
ANALYZE TABLE CDS.CONSUMER COMPUTE STATISTICS
/
ANALYZE TABLE CDS.GLOBAL_DATA_HISTORY COMPUTE STATISTICS
/
ANALYZE INDEX CDS.CONSUMER_D_IDX3 COMPUTE STATISTICS
/
ANALYZE INDEX CDS.CONSUMER_HID_IDX7 COMPUTE STATISTICS
/
ANALYZE INDEX CDS.CONSUMER_IST_IDX5 COMPUTE STATISTICS
/
ANALYZE INDEX CDS.CONSUMER_LID_IDX4 COMPUTE STATISTICS
/
ANALYZE INDEX CDS.CONSUMER_PID_IDX6 COMPUTE STATISTICS
/
ANALYZE INDEX CDS.CONSUMER_U_IDX3 COMPUTE STATISTICS
/
ANALYZE INDEX CDS.IDX_ENROLLED_CLIENT COMPUTE STATISTICS
/
ANALYZE INDEX CDS.IDX_IS_PRIMARY COMPUTE STATISTICS
/
ANALYZE INDEX CDS.CONSUMER_HISTORY_FK1_IND COMPUTE STATISTICS
/
ANALYZE INDEX CDS.CONSUMER_HISTORY_FK2_IND COMPUTE STATISTICS
/
ANALYZE INDEX CDS.CONSUMER_H_C_IDX3 COMPUTE STATISTICS
/
ANALYZE INDEX CDS.HOUSEHOLD_D_IDX3 COMPUTE STATISTICS
/
ANALYZE INDEX CDS.HOUSEHOLD_UD_IDX2 COMPUTE STATISTICS
/
ANALYZE INDEX CDS.IDX_EXT_HOUSEHOLD COMPUTE STATISTICS
/
ANALYZE INDEX CDS.HOUSEHOLD_ID_IDX1 COMPUTE STATISTICS
/
ANALYZE INDEX CDS.LOYALTY_H_C_IDX3 COMPUTE STATISTICS
/
ANALYZE INDEX CDS.STORE_CID_IDX4 COMPUTE STATISTICS
/
ANALYZE INDEX CDS.STORE_ID_IDX2 COMPUTE STATISTICS
/
ANALYZE INDEX CDS.PERSON_AK1_IND COMPUTE STATISTICS
/
ANALYZE INDEX CDS.PERSON_D_IDX4 COMPUTE STATISTICS
/
ANALYZE INDEX CDS.PERSON_FK3_IND COMPUTE STATISTICS
/
ANALYZE INDEX CDS.PERSON_U_IDX3 COMPUTE STATISTICS
/
ANALYZE INDEX CDS.PROFILE_IDX1 COMPUTE STATISTICS
/
ANALYZE INDEX CDS.LH_AUTH_CODE_IDX COMPUTE STATISTICS
/
ANALYZE INDEX CDS.LH_GENERATED_IDX COMPUTE STATISTICS
/


spool off
EOF
retval=`grep -c "procedure success" $LOGDIR/$LOGFILE`
echo "retvali is $retval "
if [ $retval = 1 ]
  then
    echo " DBMS GATHER STATS for $SID is complete" 
  else
    echo "Failed " | mail -s "DBMS GATHER STATS for $SID has failed" $mail_list 
    exit
fi
