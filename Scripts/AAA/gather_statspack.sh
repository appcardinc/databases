#!/bin/sh
#######################################################################################################################
Parameter Name	Range of Valid Values	Default Value	Meaning
##i_snap_level	0, 5, 6, 7, 10	5	Snapshot level
##i_ucomment	Text	Blank	Comment to be stored with snapshot
##i_executions_th	Integer >=0	100	SQL threshold: number of times statement was executed
##i_disk_reads_th	Integer >=0	1000	SQL threshold: number of disk reads the statement made
##i_parse_calls_th	Integer >=0	1000	SQL threshold: number of parse calls the statement made
##i_buffer_gets_th	Integer >=0	10000	SQL threshold: number of buffer gets the statement made
##i_sharable_mem_th	Integer >=0	1048576	SQL threshold: amount of sharable memory
##i_version_count_th	Integer >=0	20	SQL threshold: number of versions of a SQL statement
##i_seg_phy_reads_th	Integer >=0	1000	Segment statistic threshold: number of physical reads on a segment
##i_seg_log_reads_th	Integer >=0	10000	Segment statistic threshold: number of logical reads on a segment
##i_seg_buff_busy_th	Integer >=0	100	Segment statistic threshold: number of buffer busy waits for a segment
##i_seg_rowlock_w_th	Integer >=0	100	Segment statistic threshold: number of row lock waits for a segment
##i_seg_itl_waits_th	Integer >=0	100	Segment statistic threshold: number of ITL waits for a segment
##i_seg_cr_bks_sd_th	Integer >=0	1000	Segment statistic threshold: number of consistent reads blocks served by the instance for the segment (RAC)
##i_seg_cu_bks_sd_th	Integer >=0	1000	Segment statistic threshold: number of current blocks served by the instance for the segment (RAC)
##i_session_id	Valid SID from V$SESSION	0 (no session)	Session ID of the Oracle session for which to capture session granular statistics
##i_modify_parameter	TRUE, FALSE	FALSE	Determines whether the parameters specified are used for future snapshots
##########################################################################################################################

. ~/.bash_profile
#
# Check if SID is specified
#
if [ $# -eq 0 ];then
   echo 'You must enter SID'
   exit 1;
fi
SID=`echo $1 | tr A-Z a-z`
export ORACLE_SID=$SID
export snap_level=0
export STATUSRNAME=perfstat
export STATUSRPSWD=`get_password.sh $SID $STATUSRNAME`
sqlplus /nolog <<-EOF
connect $STATUSRNAME/$STATUSRPSWD as sysdba
set serveroutput on
set timing       on
BEGIN
             STATSPACK.SNAP(i_snap_level=>$snap_level);
END;
/
EOF
