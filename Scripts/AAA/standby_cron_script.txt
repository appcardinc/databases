[plprod-ora10-sb1-ec2-yprd1sb1]crontab -l
0 */2 * * * /home/oracle/oracle/scripts/clear_old_arch.sh > /home/oracle/oracle/    

Script:

clear_old_arch.sh

find /dump/yprd1sb1/arch/* -type f -mmin +120 -exec rm -f {} \;
find /dump/yprd1sb2/arch/* -type f -mmin +120 -exec rm -f {} \;
                                                                   