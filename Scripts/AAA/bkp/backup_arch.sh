#!/bin/sh
#**********************************************************************
#*
#*  Name: backup_arch.sh
#*  desc: backup of archivelogs and clear them
#**********************************************************************

if [ $# -ne 1 ];then
   echo 'Usage:$0 SID'
   exit 1;
fi

ORACLE_SID=`echo $1 | tr A-Z a-z`
export ORACLE_SID

# Set environment to this ORACLE_SID
. $HOME/.bash_profile $ORACLE_SID
. $HOME/.maillist

# Skip if it is a physical standby
sqlplus -s "/ as sysdba" <<-EOF > /$HOME/ops/database_role
set verify off
set head off
select database_role from v\$database;
EOF

status=`grep 'PRIMARY' /$HOME/ops/database_role >/dev/null 2>&1; echo $?`
if [  $status -eq 1 ]; then
   rm -f /tmp/database_role
   exit 2
fi

export DATE=`date +"%H%M%d-%m-%Y"`
export NLS_DATE_FORMAT='DD-MON-YYYY_HH24:MI:SS' 
#BACKUP_TAG=incr_backup_`date +"%d-%m-%Y"`
ACRH_TAG=arc_only_`date +"%H%M-%d-%m-%Y"`
CRTL_TAG=ctl_`date +"%d-%m-%Y"`
DAY_OF_WEEK=`date +%a`
BACKUP_TYPE='INCREMENTAL LEVEL 0'
TYPE=ARCH_CLEAR
BACKUP_TAG=full_backup_`date +"%d-%m-%Y"`
export RMAN_BACKUP_DIR="/dump/$ORACLE_SID/rman"
export RMAN_BACKUP_LOG=$RMAN_BACKUP_DIR/rman_archive_backup_$DATE.log
export BACKUP_ERROR=/tmp/backup_error_$$

# Create backup directories, if they do not exist
#mkdir -p $EXP_BACKUP_DIR $RMAN_BACKUP_DIR

echo "Started  RMAN Archive  backup of $ORACLE_SID at `date`"
#rman catalog rman/g0rd0n@rmancat1 target / <<-EOF > $RMAN_BACKUP_LOG
rman  target / <<-EOF > $RMAN_BACKUP_LOG
sql 'alter system archive log current';
CONFIGURE DEVICE TYPE DISK PARALLELISM 1 BACKUP TYPE TO COMPRESSED BACKUPSET;
CONFIGURE RETENTION POLICY TO RECOVERY WINDOW OF 3 DAYS;
CONFIGURE CHANNEL DEVICE TYPE DISK FORMAT '$RMAN_BACKUP_DIR/ora_backup_db_%d_S_%s_P_%p_T_%t';
CONFIGURE CONTROLFILE AUTOBACKUP ON;
CONFIGURE CONTROLFILE AUTOBACKUP FORMAT FOR DEVICE TYPE DISK TO '$RMAN_BACKUP_DIR/ora_cf%F';
# Backup  archivelog and clear them
RUN
{
configure controlfile autobackup on;
ALLOCATE CHANNEL c1 DEVICE TYPE disk;
sql 'alter system archive log current';
backup tag "${ACRH_TAG}" format '$RMAN_BACKUP_DIR/ora_backup_ARCHIVE_%d_S_%s_P_%p_T_%t' archivelog all ;
CROSSCHECK BACKUP;
delete force noprompt archivelog until time 'sysdate-1/12' backed up 1 times to device type disk;
DELETE noprompt EXPIRED BACKUP;
delete noprompt obsolete;
crosscheck archivelog all;
delete noprompt expired archivelog all;
release channel c1;
}
# List backup information
LIST ARCHIVELOG ALL;
LIST BACKUP OF CONTROLFILE;
LIST BACKUP SUMMARY;
LIST BACKUP;
# Reports
REPORT SCHEMA;
REPORT UNRECOVERABLE;
REPORT NEED BACKUP;
# Validate backups
# BACKUP CHECK LOGICAL VALIDATE DATABASE ARCHIVELOG ALL;
# Validate the restore of the backup
#RESTORE DATABASE VALIDATE;
EOF

echo "Finished RMAN  archive  backup of $ORACLE_SID at `date`"

# Ignore RMAN-06480: archivelog 'archivelog_file' cannot be found on disk
# and    RMAN-06485: changed archivelog expired
echo "Check Archive backup $RMAN_BACKUP_LOG " > $BACKUP_ERROR
egrep -a "ORA-"\|"error"\|"RMAN-" $RMAN_BACKUP_LOG |egrep -v  "RMAN-08137"|egrep -v "RMAN-06480" | egrep -v "RMAN-06485" > $BACKUP_ERROR
status_rman=$?

# Notify users if errors were found in either RMAN or export backup
if [ $status_rman -eq 0 ];then
   mail -s "ORACLE ALERT => [`hostname`]: Archive logs backup failed for - $ORACLE_SID!" $mail_list < $BACKUP_ERROR > /dev/null 2>&1
#   mail -s "ORACLE ALERT => [`hostname`]: Archive logs backup failed for - $ORACLE_SID!" 'evgeny@datateam.co.il' < $BACKUP_ERROR > /dev/null 2>&1
   exit 1
fi

mail -s "[`hostname`]: Archive logs  backup succeeded for - $ORACLE_SID" $mail_list < /dev/null > /dev/null 2>&1

# Cleanup and exit
rm -f $BACKUP_ERROR

exit 0
