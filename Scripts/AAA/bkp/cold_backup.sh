#!/bin/sh
#**********************************************************************
#*          
#*  Name: cold_backup.sh
#*          
#*  Description: Cold backup of a database
#*
#*
#**********************************************************************
#

. $HOME/.bash_profile $1

if [ $# -ne 2 ];then
   echo 'Usage:$0 SID backup_directory'
   exit 1;
fi

SID=`echo $1 | tr A-Z a-z`
BACKUP_DIR=$2
export ORACLE_SID=$SID

sys_password=`get_password.sh $SID sys`
if [ "$sys_password" = "" ];then
   echo 'SID not found in the password file'
   exit 1
fi

cd $HOME/ops

# Generate cold backup script
sqlplus -s "sys/$sys_password as sysdba" <<EOF>execute_cold_backup.sh
set pages 0
set feed off
set ver off
set termout off
set echo off
set lines 132

select '#!/bin/sh -x' from dual;
select 'cp -p ' || name   || ' $BACKUP_DIR/' || substr(name, (instr(name, '/', -1, 1))+1, length(name))       from v\$controlfile;
select 'cp -p ' || name   || ' $BACKUP_DIR/' || substr(name, (instr(name, '/', -1, 1))+1, length(name))       from v\$tempfile;
select 'cp -p ' || name   || ' $BACKUP_DIR/' || substr(name, (instr(name, '/', -1, 1))+1, length(name))       from v\$datafile;
select 'cp -p ' || member || ' $BACKUP_DIR/' || substr(member, (instr(member, '/', -1, 1))+1, length(member)) from v\$logfile;
exit
EOF

# Generate restore script in the backup directory
sqlplus -s "sys/$sys_password as sysdba" <<EOF> $BACKUP_DIR/execute_restore_db.sh
set pages 0
set feed off
set ver off
set termout off
set echo off
set lines 132

select '#!/bin/sh -x' from dual;
select 'cp -p ' || substr(name, (instr(name, '/', -1, 1))+1, length(name))       || ' ' || name   from v\$controlfile;
select 'cp -p ' || substr(name, (instr(name, '/', -1, 1))+1, length(name))       || ' ' || name   from v\$tempfile;
select 'cp -p ' || substr(name, (instr(name, '/', -1, 1))+1, length(name))       || ' ' || name   from v\$datafile;
select 'cp -p ' || substr(member, (instr(member, '/', -1, 1))+1, length(member)) || ' ' || member from v\$logfile;
exit
EOF


# Make them executable
chmod 755 ./execute_cold_backup.sh
chmod 755 $BACKUP_DIR/execute_restore_db.sh

# Shutdown database
sqlplus "sys/$sys_password as sysdba" <<EOF
create pfile from spfile;
shutdown immediate
startup
shutdown normal
exit
EOF

# Execute cold backup
./execute_cold_backup.sh

# Startup database
sqlplus "sys/$sys_password as sysdba" <<EOF
startup
EOF
