#!/bin/sh
#**********************************************************************
#*
#*  Name: backup_db.sh
#*
#**********************************************************************

if [ $# -ne 1 ];then
   echo 'Usage:$0 SID'
   exit 1;
fi

ORACLE_SID=`echo $1 | tr A-Z a-z`
export ORACLE_SID

# Set environment to this ORACLE_SID
. $HOME/.bash_profile $ORACLE_SID
. $HOME/.maillist

# Skip if it is a physical standby
sqlplus -s "/ as sysdba" <<-EOF > /$HOME/ops/database_role
set verify off
set head off
select database_role from v\$database;
EOF

status=`grep 'PRIMARY' /$HOME/ops/database_role >/dev/null 2>&1; echo $?`
if [  $status -eq 1 ]; then
   rm -f /tmp/database_role
   exit 2
fi

export DATE=`date '+%m%d%Y'`
export NLS_DATE_FORMAT='DD-MON-YYYY_HH24:MI:SS' 
#BACKUP_TAG=incr_backup_`date +"%d-%m-%Y"`
ACRH_TAG=arc_`date +"%d-%m-%Y"`
CRTL_TAG=ctl_`date +"%d-%m-%Y"`
SPF_TAG=spfile_`date +"%d-%m-%Y"`
DAY_OF_WEEK=`date +%a`
BACKUP_TYPE='INCREMENTAL LEVEL 0'
TYPE=FULL
BACKUP_TAG=full_backup_`date +"%d-%m-%Y"`
export EXP_BACKUP_DIR="/dump/$ORACLE_SID/export"
export RMAN_BACKUP_DIR="/dump/$ORACLE_SID/rman"
export NLS_LANG=AMERICAN_AMERICA.AL32UTF8
export EXP_BACKUP_LOG=$EXP_BACKUP_DIR/export_backup_$DATE.log
export RMAN_BACKUP_LOG=$RMAN_BACKUP_DIR/rman_backup_$DATE.log

export EXP_FILE=$EXP_BACKUP_DIR/export_$DATE.dmp
export TRACE_CONTROLFILE=$RMAN_BACKUP_DIR/trace_controlfile_$DATE.ctl
export BACKUP_ERROR=/tmp/backup_error_$$


#clear old export file 
rm $EXP_BACKUP_DIR/*.dmp.*

echo "Started  RMAN   backup of $ORACLE_SID at `date`"
#rman catalog rman/g0rd0n@rmancat1 target / <<-EOF > $RMAN_BACKUP_LOG
rman  target / <<-EOF > $RMAN_BACKUP_LOG
LIST INCARNATION;
sql 'alter system archive log current';
CONFIGURE DEVICE TYPE DISK PARALLELISM 1 BACKUP TYPE TO COMPRESSED BACKUPSET;
CONFIGURE RETENTION POLICY TO RECOVERY WINDOW OF 3 DAYS;
CONFIGURE CHANNEL DEVICE TYPE DISK MAXPIECESIZE 20G;
CONFIGURE CHANNEL DEVICE TYPE DISK FORMAT '$RMAN_BACKUP_DIR/ora_backup_db_%d_S_%s_P_%p_T_%t';
CONFIGURE CONTROLFILE AUTOBACKUP ON;
CONFIGURE CONTROLFILE AUTOBACKUP FORMAT FOR DEVICE TYPE DISK TO '$RMAN_BACKUP_DIR/ora_cf%F';
# Backup database and archivelog and clear old backups by policy
RUN
{
configure controlfile autobackup on;
ALLOCATE CHANNEL c1 DEVICE TYPE disk;
ALLOCATE CHANNEL c2 DEVICE TYPE disk;
ALLOCATE CHANNEL c3 DEVICE TYPE disk;
ALLOCATE CHANNEL c4 DEVICE TYPE disk;
ALLOCATE CHANNEL c5 DEVICE TYPE disk;
ALLOCATE CHANNEL c6 DEVICE TYPE disk;
ALLOCATE CHANNEL c7 DEVICE TYPE disk;
backup AS COMPRESSED BACKUPSET ${BACKUP_TYPE} database tag "${BACKUP_TAG}" format '$RMAN_BACKUP_DIR/ora_backup_DB_FULL_%d_S_%s_P_%p_T_%t' ;
sql 'alter system archive log current';
backup tag "${ACRH_TAG}" format '$RMAN_BACKUP_DIR/ora_backup_ARCHIVE_%d_S_%s_P_%p_T_%t' archivelog all  ;
backup tag "${CRTL_TAG}" current controlfile format '$RMAN_BACKUP_DIR/ora_backup_CONTROL_%d_S_%s_P_%p_T_%t';
backup tag "${SPF_TAG}" format '$RMAN_BACKUP_DIR/ora_backup_spfile_%d_S_%s_P_%p_T_%t' SPFILE;
CROSSCHECK BACKUP;
delete force  noprompt archivelog until time 'trunc(sysdate-1/12)' backed up 1 times to device type disk;
DELETE noprompt EXPIRED BACKUP;
delete noprompt obsolete;
crosscheck archivelog all;
delete noprompt expired archivelog all;
release channel c1;
release channel c2;
release channel c3;
release channel c4;
release channel c5;
release channel c6;
release channel c7;
}
# List backup information
LIST ARCHIVELOG ALL;
LIST BACKUP OF CONTROLFILE;
LIST BACKUP SUMMARY;
# Reports
REPORT SCHEMA;
REPORT UNRECOVERABLE;
REPORT NEED BACKUP;
# Validate backups
#BACKUP CHECK LOGICAL VALIDATE DATABASE ARCHIVELOG ALL;
# Validate the restore of the backup
#RESTORE DATABASE VALIDATE;
EOF

echo "Finished RMAN   backup of $ORACLE_SID at `date`"

sqlplus -s "/ as sysdba" <<-EOF > /dev/null 2>&1
ALTER DATABASE BACKUP CONTROLFILE TO TRACE AS '$TRACE_CONTROLFILE' REUSE;
EOF

echo "Started  EXPORT backup of $ORACLE_SID at `date`"
# Full export
exp backup_admin/backup_admin file=$EXP_FILE log=$EXP_BACKUP_LOG full=y statistics=none direct=y recordlength=65535 > /dev/null 2>&1
gzip $EXP_FILE
#pk gzip /dump/ytrsprd1/rman/ora_*
echo "Finished EXPORT backup of $ORACLE_SID at `date`"

# Check for errors during RMAN backup
# Ignore RMAN-06480: archivelog 'archivelog_file' cannot be found on disk
# and    RMAN-06485: changed archivelog expired
echo "Check $RMAN_BACKUP_LOG and $EXP_BACKUP_LOG" > $BACKUP_ERROR
egrep -a "ORA-"\|"error"\|"RMAN-" $RMAN_BACKUP_LOG | egrep -v "RMAN-06480" | egrep -v "RMAN-06485" > $BACKUP_ERROR
status_rman=$?

# Check for errors during export
egrep -a "ORA-"\|"error"\|"EXP-"  $EXP_BACKUP_LOG | egrep -v "EXP-00079" >> $BACKUP_ERROR
status_exp=$?

# Notify users if errors were found in either RMAN or export backup
if [ $status_rman -eq 0 -o $status_exp -eq 0 ];then
   mail -s "ORACLE ALERT => [`hostname`]: Database backup failed for - $ORACLE_SID!" $mail_list < $BACKUP_ERROR > /dev/null 2>&1
   exit 1
fi

mail -s "[`hostname`]: Database backup succeeded for - $ORACLE_SID" $mail_list < /dev/null > /dev/null 2>&1

# Cleanup and exit
rm -f $BACKUP_ERROR

exit 0
