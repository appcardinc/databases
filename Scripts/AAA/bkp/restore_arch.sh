#!/bin/sh
#**********************************************************************
#*
#*  Name: backup_db.sh
#*
#**********************************************************************

if [ $# -ne 1 ];then
   echo 'Usage:$0 SID'
   exit 1;
fi

ORACLE_SID=`echo $1 | tr A-Z a-z`
export ORACLE_SID=ytrsprd2

# Set environment to this ORACLE_SID
. $HOME/.bash_profile $ORACLE_SID
. $HOME/.maillist

# Skip if it is a physical standby
sqlplus -s "/ as sysdba" <<-EOF > /$HOME/ops/database_role
set verify off
set head off
select database_role from v\$database;
EOF

status=`grep 'PRIMARY' /$HOME/ops/database_role >/dev/null 2>&1; echo $?`
if [  $status -eq 1 ]; then
   rm -f /tmp/database_role
   exit 2
fi

export DATE=`date '+%m%d%Y'`
export NLS_DATE_FORMAT='DD-MON-YYYY_HH24:MI:SS' 
#BACKUP_TAG=incr_backup_`date +"%d-%m-%Y"`
ACRH_TAG=arc_`date +"%d-%m-%Y"`
CRTL_TAG=ctl_`date +"%d-%m-%Y"`
SPF_TAG=spfile_`date +"%d-%m-%Y"`
DAY_OF_WEEK=`date +%a`
BACKUP_TYPE='INCREMENTAL LEVEL 0'
TYPE=FULL
BACKUP_TAG=full_backup_`date +"%d-%m-%Y"`
export EXP_BACKUP_DIR="/dump/$ORACLE_SID/export"
export RMAN_BACKUP_DIR="/dump/$ORACLE_SID/rman"
export NLS_LANG=AMERICAN_AMERICA.AL32UTF8
export EXP_BACKUP_LOG=$EXP_BACKUP_DIR/export_backup_$DATE.log
export RMAN_BACKUP_LOG=$RMAN_BACKUP_DIR/rman_restore_$DATE.log

export EXP_FILE=$EXP_BACKUP_DIR/export_$DATE.dmp
export TRACE_CONTROLFILE=$RMAN_BACKUP_DIR/trace_controlfile_$DATE.ctl
export BACKUP_ERROR=/tmp/backup_error_$$



echo "Started  RMAN   backup of $ORACLE_SID at `date`"
rman catalog rman/g0rd0n@rmancat1 target / <<-EOF > $RMAN_BACKUP_LOG
CONFIGURE DEVICE TYPE DISK PARALLELISM 1 BACKUP TYPE TO COMPRESSED BACKUPSET;
CONFIGURE RETENTION POLICY TO RECOVERY WINDOW OF 3 DAYS;
CONFIGURE CHANNEL DEVICE TYPE DISK MAXPIECESIZE 20G;
CONFIGURE CHANNEL DEVICE TYPE DISK FORMAT '$RMAN_BACKUP_DIR/ora_backup_db_%d_S_%s_P_%p_T_%t';
CONFIGURE CONTROLFILE AUTOBACKUP ON;
CONFIGURE CONTROLFILE AUTOBACKUP FORMAT FOR DEVICE TYPE DISK TO '$RMAN_BACKUP_DIR/ora_cf%F';
# Backup database and archivelog and clear old backups by policy
RUN
{
ALLOCATE CHANNEL c1 DEVICE TYPE disk;
ALLOCATE CHANNEL c2 DEVICE TYPE disk;
ALLOCATE CHANNEL c3 DEVICE TYPE disk;
ALLOCATE CHANNEL c4 DEVICE TYPE disk;
ALLOCATE CHANNEL c5 DEVICE TYPE disk;
ALLOCATE CHANNEL c6 DEVICE TYPE disk;
ALLOCATE CHANNEL c7 DEVICE TYPE disk;
restore archivelog from logseq 292476 until logseq 292561;
release channel c1;
release channel c2;
release channel c3;
release channel c4;
release channel c5;
release channel c6;
release channel c7;
}
# List backup information
# Validate backups
#BACKUP CHECK LOGICAL VALIDATE DATABASE ARCHIVELOG ALL;
# Validate the restore of the backup
#RESTORE DATABASE VALIDATE;
EOF

echo "Finished RMAN   backup of $ORACLE_SID at `date`"

exit 0
