#!/bin/sh -x
#**********************************************************************
#*          
#*  Name: hot_backup.sh
#*          
#*  Description: Perform hot backup of a database
#*
#**********************************************************************
#

if [ $# -ne 2 ];then
   echo 'Usage:$0 SID backup_directory'
   exit 1;
fi

SID=`echo $1 | tr A-Z a-z`
BACKUP_DIR=$2

# Switch environment to this SID
. $HOME/.bash_profile $SID

cd $HOME/ops

# Generate hot backup SQL script
sqlplus -s "/ as sysdba" <<-EOF > exec_hot_backup.sql 2>&1
set ver off
set term off
set feed off
set serveroutput on size 1000000 format wrapped
set lines 256

-- Variables
variable bkp_dir varchar2(255) 
variable instance_name varchar2(255) 

begin
dbms_output.enable(1000000);
select '$BACKUP_DIR' into :bkp_dir from dual;

dbms_output.put( 'set echo on');
dbms_output.new_line;
dbms_output.new_line;

dbms_output.put( 'alter system archive log current;');
dbms_output.new_line;

dbms_output.put( 'create pfile=' || '''' || :bkp_dir || '/init$SID' || '.ora' || '''' || ' from spfile;');
dbms_output.new_line;
dbms_output.new_line;

dbms_output.put( 'alter database backup controlfile to trace as ' || '''' || :bkp_dir || '/backup_ctrl.trc' || '''' || ' reuse;');
dbms_output.new_line;
dbms_output.new_line;

for tablespace in (
   select tablespace_name name from dba_tablespaces where contents != 'TEMPORARY' order by 1 
)
loop
   dbms_output.put( 'alter tablespace ' || tablespace.name || ' begin backup;');
   dbms_output.new_line;
   for datafile in (
      select name
      from v\$datafile d
      where tablespace.name = ( select name from v\$tablespace t where t.ts#=d.ts#)
      order by 1
   )
   loop
      dbms_output.put('!cp -p ' || datafile.name  || ' ' || :bkp_dir || '/.');
      dbms_output.new_line;
   end loop;

   dbms_output.put( 'alter tablespace ' || tablespace.name || ' end backup;');
   dbms_output.new_line;

end loop;


dbms_output.put( 'alter system archive log current;');
dbms_output.new_line;
dbms_output.put( 'exit;');
dbms_output.new_line;

end;
/

exit
EOF

# Execute hot backup script
sqlplus "/ as sysdba" @exec_hot_backup.sql

# Generate restore script in the backup directory
sqlplus -s "/ as sysdba" <<EOF> $BACKUP_DIR/exec_restore_hot_backup.sh
set pages 0
set feed off
set ver off
set termout off
set echo off
set lines 132

select '#!/bin/sh -x' from dual;
select 'cp -p ' || '$BACKUP_DIR' || '/' || substr(name, (instr(name, '/', -1, 1))+1, length(name)) || ' ' || name from v\$tempfile;
select 'cp -p ' || '$BACKUP_DIR' || '/' || substr(name, (instr(name, '/', -1, 1))+1, length(name)) || ' ' || name from v\$datafile;
exit
EOF


# Make restore script executable
chmod 755 $BACKUP_DIR/exec_restore_hot_backup.sh
