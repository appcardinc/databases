#!/bin/sh -x
#*************************************************************************
#*
#*  Name: purge_logs.sh
#*
#*
#*  Description: Purge old log files
#*************************************************************************
#
# Source bash profile
. $HOME/.bash_profile_1

#-------------------------------------------------------------------------
# (1) Define purge directories
#-------------------------------------------------------------------------
export CHECK_IMAGES_DIR=/dump/images        # Check images directory 
export ARCH=/dump                      # Archive log file directory
export AUDIT=/u11/oradata                     # Audit files directory 

#-------------------------------------------------------------------------
# (2) Define retention periods
#-------------------------------------------------------------------------
# Archive log files
export KEEP_ARC=1
# Audit files
export KEEP_AUD=30
# Log files
export KEEP_LOG=30
# Trace files in bdump, cdump, udump
export KEEP_TRC=30
# Check images
export KEEP_CHK=62

for DB_ENTRY in `cat /etc/oratab | grep -v \# | grep -v \* | grep -v ^$`
do
   ORACLE_SID=`echo $DB_ENTRY | awk -F: {' print $1 '}`;
   ADMIN=$ORACLE_BASE/admin

   SAVE_IFS=$IFS
   IFS=:

#-------------------------------------------------------------------------
# (3) Define purge policies
#  - directory pattern retention_period
#-------------------------------------------------------------------------
cat <<EOF > /tmp/purge_definitions
$ADMIN/$ORACLE_SID/bdump        \*.trc   $KEEP_TRC
$ADMIN/$ORACLE_SID/bdump        \*.log\*  $KEEP_LOG
$ADMIN/$ORACLE_SID/cdump        \*.trc   $KEEP_TRC
$ADMIN/$ORACLE_SID/udump        \*.trc   $KEEP_TRC
$AUDIT/$ORACLE_SID/audit        \*.aud   $KEEP_AUD
$ORACLE_HOME/rdbms/audit        \*.aud   $KEEP_AUD
$ARCH/$ORACLE_SID/arch          \*.arc   $KEEP_ARC
$ARCH/$ORACLE_SID/arch          \*.dbf   $KEEP_ARC
$ARCH/$ORACLE_SID/arch/standby  \*.arc   $KEEP_ARC
$ARCH/$ORACLE_SID/arch/standby  \*.dbf   $KEEP_ARC
$CHECK_IMAGES_DIR               \*.bmp   $KEEP_CHK
$HOME/ops                       \*.log\*  $KEEP_LOG

echo $ADMIN/$ORACLE_SID
echo $ORACLE_HOME/rdbms/audit
echo $ARCH/$ORACLE_SID/arch
echo $CHECK_IMAGES_DIR
echo $HOME/ops


EOF

   while read pd
   do
      IFS=$SAVE_IFS
      DIR=`echo $pd | awk {' print $1 '}`
      PATTERN=`echo $pd | awk {' print $2 '}`
      KEEP=`echo $pd | awk {' print $3 '}`
   
      if [ -d $DIR ]; then
	echo $DIR
	echo $PATTERN
         find $DIR -name $PATTERN -mtime +$KEEP -follow -print -exec rm -f {} \;
      fi
   done < /tmp/purge_definitions

done
