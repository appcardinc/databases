#!/bin/sh
#######################################################################################################################
###
###           This job analyzes all schemas for a database except SYS and SYSTEM
###
###  PREREQUISITE: create table to save current statistics              
###
###            exec dbms_stats.create_stat_table('SYS', 'STATTAB');
###
###  Statement to execute: 
###
###             exec dbms_stats.gather_schema_stats(ownname           => s.schema,
###                                                 cascade           => TRUE,
###                                                 method_opt        => 'FOR ALL INDEXED COLUMNS SIZE 1',
###                                                 degree            => 4,
###                                                 estimate_percent  => 25,
###                                                 statown           => 'SYS', 
###                                                 stattab           => 'STATTAB')
###  PARAMETER         DESCRIPTION
###  ---------         --------------------------------------------------------------------
###  ownname           analyze ALL tables  for this particular schema
###  cascade           analyze ALL indexes
###  method_opt        create  histograms:
###                      'FOR ALL COLUMNS SIZE 1'         - for all columns (default, not recommended)
###                      'FOR ALL INDEXED COLUMNS SIZE 1' - for indexes columns only;
###  degree            degree of parallelism, if desired
###  estimate_percent  this parameter could be set to 'DBMS_STATS.AUTO_SAMPLE_SIZE', however the latter one has some issues
###  statown, stattab  owner & table name where current statistics will be saved.
###                    This is an insurance policy in case of performance degradation as a result of this job execution.
###                    To create stattab, run: exec dbms_stats.create_stat_table('SYS', 'STATTAB');
###
###
#######################################################################################################################

exit

#
# no need to run this script
# because we don t have such environment
#


. ~/.bash_profile_2
. ~/.maillist
#
# Check if SID is specified
#
if [ $# -eq 0 ];then
   echo 'You must enter SID'
   exit 1;
fi
SID=`echo $1 | tr A-Z a-z`
export ORACLE_SID=$SID
export options=GATHER
export method_opt='FOR ALL COLUMNS SIZE AUTO'
export LOGDIR=/home/oracle/scripts/log
export LOGFILE=$SID'_dbstats_'`date +%d%m%H%M`_$$'.log'
export CASCADE=TRUE
sqlplus /nolog <<-EOF
connect / as sysdba
spool $LOGDIR/$LOGFILE

CREATE INDEX TLOG.TX_ITEM_OPERATION_IDX ON TLOG.TX_ITEM (OPERATION) TABLESPACE TLOG_IDX NOLOGGING

/
spool off
EOF
retval=`grep -c "procedure success" $LOGDIR/$LOGFILE`
echo "retvali is $retval "
if [ $retval = 1 ]
  then
    echo " DBMS GATHER STATS for $SID is complete" 
  else
    echo "Failed " | mail -s "DBMS GATHER STATS for $SID has failed" $mail_list 
    exit
fi
