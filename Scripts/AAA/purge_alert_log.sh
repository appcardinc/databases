#!/bin/sh -x
#*************************************************************************
#*
#*  Name: purge_alert_logs.sh
#*
#*
#*  Description: Purge old log files
#*************************************************************************
#
# Source bash profile
. $HOME/.bash_profile

#-------------------------------------------------------------------------
# (1) Define purge directories
#-------------------------------------------------------------------------

#-------------------------------------------------------------------------
# (2) Define retention periods
#-------------------------------------------------------------------------

for DB_ENTRY in `cat /etc/oratab | grep -v \# | grep -v \* | grep -v ^$`
do
   ORACLE_SID=`echo $DB_ENTRY | awk -F: {' print $1 '}`;
   ADMIN=$ORACLE_BASE/admin

   SAVE_IFS=$IFS
   IFS=:

#-------------------------------------------------------------------------
# (3) Define purge policies
#  - directory pattern retention_period
#-------------------------------------------------------------------------
$ADMIN/$ORACLE_SID/bdump        \*.trc   $KEEP_TRC
$ADMIN/$ORACLE_SID/bdump        \*.log\*  $KEEP_LOG

echo $ADMIN/$ORACLE_SID

   while read pd
   do
      IFS=$SAVE_IFS
      DIR=`echo $pd | awk {' print $1 '}`
      PATTERN=`echo $pd | awk {' print $2 '}`
      KEEP=`echo $pd | awk {' print $3 '}`
   
      if [ -d $DIR ]; then
	echo $DIR
	echo $PATTERN
	cp $ADMIN/$ORACLE_SID/bdump/alert_ytrsprd1.log $ADMIN/$ORACLE_SID/bdump/alert_ytrsprd1.log_`date +"%H%M-%d-%m-%Y"`.old
	echo > $ADMIN/$ORACLE_SID/bdump/alert_ytrsprd1.log
#         find $DIR -name $PATTERN -mtime +$KEEP -follow -print -exec rm -f {} \;
      fi
   done < /tmp/purge_definitions

done
