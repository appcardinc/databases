#!/bin/sh
#######################################################################################################################
###
###           This job analyzes all schemas for a database except SYS and SYSTEM
###
###  PREREQUISITE: create table to save current statistics              
###
###            exec dbms_stats.create_stat_table('SYS', 'STATTAB');
###
###  Statement to execute: 
###
###             exec dbms_stats.gather_schema_stats(ownname           => s.schema,
###                                                 cascade           => TRUE,
###                                                 method_opt        => 'FOR ALL INDEXED COLUMNS SIZE 1',
###                                                 degree            => 4,
###                                                 estimate_percent  => 25,
###                                                 statown           => 'SYS', 
###                                                 stattab           => 'STATTAB')
###  PARAMETER         DESCRIPTION
###  ---------         --------------------------------------------------------------------
###  ownname           analyze ALL tables  for this particular schema
###  cascade           analyze ALL indexes
###  method_opt        create  histograms:
###                      'FOR ALL COLUMNS SIZE 1'         - for all columns (default, not recommended)
###                      'FOR ALL INDEXED COLUMNS SIZE 1' - for indexes columns only;
###  degree            degree of parallelism, if desired
###  estimate_percent  this parameter could be set to 'DBMS_STATS.AUTO_SAMPLE_SIZE', however the latter one has some issues
###  statown, stattab  owner & table name where current statistics will be saved.
###                    This is an insurance policy in case of performance degradation as a result of this job execution.
###                    To create stattab, run: exec dbms_stats.create_stat_table('SYS', 'STATTAB');
###
###
#######################################################################################################################
. ~/.bash_profile_2
. ~/.maillist
#
# Check if SID is specified
#
if [ $# -eq 0 ];then
   echo 'You must enter SID'
   exit 1;
fi
SID=`echo $1 | tr A-Z a-z`
export ORACLE_SID=$SID
export options=GATHER
export method_opt='FOR ALL COLUMNS SIZE AUTO'
export LOGDIR=/home/oracle/scripts/log
export LOGFILE=$SID'_dbstats_'`date +%d%m%H%M`_$$'.log'
export CASCADE=TRUE
sqlplus /nolog <<-EOF
connect / as sysdba
spool $LOGDIR/$LOGFILE
ANALYZE TABLE CDS.CONSUMER_HISTORY COMPUTE STATISTICS
/
DELETE FROM TLOG.TX_TRIGGER_ITEM WHERE TX_BENEFIT_ID IN (SELECT TX_BENEFIT_ID FROM TLOG.TX_BENEFIT WHERE TX_ID IN (select TX_ID FROM tlog.tx_data where created < SYSDATE -60))
/
commit
/
DELETE FROM TLOG.TX_BENEFIT WHERE TX_ID IN (select TX_ID FROM tlog.tx_data where created < SYSDATE -60)
/
commit
/
DELETE FROM TLOG.TX_ITEM WHERE TX_ID IN (select TX_ID FROM tlog.tx_data where created < SYSDATE -71)
/
commit
/
DELETE FROM TLOG.TX_ITEM WHERE TX_ID IN (select TX_ID FROM tlog.tx_data where created < SYSDATE -70)
/
commit
/
DELETE FROM TLOG.TX_ITEM WHERE TX_ID IN (select TX_ID FROM tlog.tx_data where created < SYSDATE -69)
/
commit
/
DELETE FROM TLOG.TX_ITEM WHERE TX_ID IN (select TX_ID FROM tlog.tx_data where created < SYSDATE -68)
/
commit
/
DELETE FROM TLOG.TX_ITEM WHERE TX_ID IN (select TX_ID FROM tlog.tx_data where created < SYSDATE -67)
/
commit
/
DELETE FROM TLOG.TX_ITEM WHERE TX_ID IN (select TX_ID FROM tlog.tx_data where created < SYSDATE -66)
/
commit
/
DELETE FROM TLOG.TX_ITEM WHERE TX_ID IN (select TX_ID FROM tlog.tx_data where created < SYSDATE -65)
/
commit
/
DELETE FROM TLOG.TX_ITEM WHERE TX_ID IN (select TX_ID FROM tlog.tx_data where created < SYSDATE -64)
/
commit
/
DELETE FROM TLOG.TX_ITEM WHERE TX_ID IN (select TX_ID FROM tlog.tx_data where created < SYSDATE -63)
/
commit
/
DELETE FROM TLOG.TX_ITEM WHERE TX_ID IN (select TX_ID FROM tlog.tx_data where created < SYSDATE -62)
/
commit
/
DELETE FROM TLOG.TX_ITEM WHERE TX_ID IN (select TX_ID FROM tlog.tx_data where created < SYSDATE -61)
/
commit
/
DELETE FROM TLOG.TX_ITEM WHERE TX_ID IN (select TX_ID FROM tlog.tx_data where created < SYSDATE -60)
/
commit
/
DELETE FROM TLOG.TX_ITEM WHERE TX_ID IN (select TX_ID FROM tlog.tx_data where created < SYSDATE -59)
/
commit
/
DELETE FROM TLOG.TX_ITEM WHERE TX_ID IN (select TX_ID FROM tlog.tx_data where created < SYSDATE -58)
/
commit
/
DELETE FROM TLOG.TX_ITEM WHERE TX_ID IN (select TX_ID FROM tlog.tx_data where created < SYSDATE -57)
/
commit
/
DELETE FROM TLOG.TX_TENDER WHERE TX_ID IN (select TX_ID FROM tlog.tx_data where created < SYSDATE -60)
/
commit
/
DELETE FROM TLOG.TX_TENDER WHERE TX_ID IN (select TX_ID FROM tlog.tx_data where created < SYSDATE -59)
/
commit
/
DELETE FROM TLOG.TX_TENDER WHERE TX_ID IN (select TX_ID FROM tlog.tx_data where created < SYSDATE -58)
/
commit
/
DELETE FROM TLOG.TX_TENDER WHERE TX_ID IN (select TX_ID FROM tlog.tx_data where created < SYSDATE -57)
/
commit
/
DELETE FROM TLOG.TX_DATA WHERE CREATED < SYSDATE -60
/
commit
/
ANALYZE TABLE CDS.LOYALTY_CARD COMPUTE STATISTICS
/
ANALYZE TABLE CDS.LOYALTY_HISTORY COMPUTE STATISTICS
/
ANALYZE TABLE CDS.HOUSEHOLD COMPUTE STATISTICS
/
ANALYZE TABLE CDS.ENTITY COMPUTE STATISTICS
/
ANALYZE TABLE CDS.PERSON COMPUTE STATISTICS
/
ANALYZE TABLE CDS.CONSUMER COMPUTE STATISTICS
/
ANALYZE TABLE CDS.GLOBAL_DATA COMPUTE STATISTICS
/
ANALYZE TABLE CDS.GLOBAL_DATA_HISTORY COMPUTE STATISTICS
/
ANALYZE INDEX CDS.CONSUMER_D_IDX3 COMPUTE STATISTICS
/
ANALYZE INDEX CDS.CONSUMER_HID_IDX7 COMPUTE STATISTICS
/
ANALYZE INDEX CDS.CONSUMER_IST_IDX5 COMPUTE STATISTICS
/
ANALYZE INDEX CDS.CONSUMER_LID_IDX4 COMPUTE STATISTICS
/
ANALYZE INDEX CDS.CONSUMER_PID_IDX6 COMPUTE STATISTICS
/
ANALYZE INDEX CDS.CONSUMER_U_IDX3 COMPUTE STATISTICS
/
ANALYZE INDEX CDS.IDX_ENROLLED_CLIENT COMPUTE STATISTICS
/
ANALYZE INDEX CDS.IDX_IS_PRIMARY COMPUTE STATISTICS
/
ANALYZE INDEX CDS.CONSUMER_HISTORY_FK1_IND COMPUTE STATISTICS
/
ANALYZE INDEX CDS.CONSUMER_HISTORY_FK2_IND COMPUTE STATISTICS
/
ANALYZE INDEX CDS.CONSUMER_H_C_IDX3 COMPUTE STATISTICS
/
ANALYZE INDEX CDS.HOUSEHOLD_D_IDX3 COMPUTE STATISTICS
/
ANALYZE INDEX CDS.HOUSEHOLD_UD_IDX2 COMPUTE STATISTICS
/
ANALYZE INDEX CDS.IDX_EXT_HOUSEHOLD COMPUTE STATISTICS
/
ANALYZE INDEX CDS.HOUSEHOLD_ID_IDX1 COMPUTE STATISTICS
/
ANALYZE INDEX CDS.LOYALTY_H_C_IDX3 COMPUTE STATISTICS
/
ANALYZE INDEX CDS.STORE_CID_IDX4 COMPUTE STATISTICS
/
ANALYZE INDEX CDS.STORE_ID_IDX2 COMPUTE STATISTICS
/
ANALYZE INDEX CDS.PERSON_AK1_IND COMPUTE STATISTICS
/
ANALYZE INDEX CDS.PERSON_D_IDX4 COMPUTE STATISTICS
/
ANALYZE INDEX CDS.PERSON_FK3_IND COMPUTE STATISTICS
/
ANALYZE INDEX CDS.PERSON_U_IDX3 COMPUTE STATISTICS
/
ANALYZE INDEX CDS.PROFILE_IDX1 COMPUTE STATISTICS
/
ANALYZE INDEX CDS.LH_AUTH_CODE_IDX COMPUTE STATISTICS
/
ANALYZE INDEX CDS.LH_GENERATED_IDX COMPUTE STATISTICS
/

spool off
EOF
retval=`grep -c "procedure success" $LOGDIR/$LOGFILE`
echo "retvali is $retval "
if [ $retval = 1 ]
  then
    echo " DBMS GATHER STATS for $SID is complete" 
  else
    echo "Failed " | mail -s "DBMS GATHER STATS for $SID has failed" $mail_list 
    exit
fi
