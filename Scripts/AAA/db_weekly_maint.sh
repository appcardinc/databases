#!/bin/sh
#######################################################################################################################
###
###           This job executes scripts required for a weekly maintenance of a database.
###
###
#######################################################################################################################
. $HOME/.bash_profile
#
# Check if SID is specified
#
if [ $# -eq 0 ];then
   echo 'You must enter SID'
   exit 1;
fi

SID=`echo $1 | tr A-Z a-z`

#***************************************************************************************************************
#  Generate PGA's statistics
#
$HOME/scripts/cron/pga_stats.sh $SID > $HOME/ops/pga_stats_$SID.log 2>&1

#***************************************************************************************************************
#  Analyze all schemas in the database except SYS and SYSTEM
#
#$HOME/scripts/cron/analyze_schema.sh $SID > $HOME/ops/analyze_$SID.log   2>&1
