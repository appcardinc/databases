#!/bin/sh
#######################################################################################################################
###
###           This job analyzes all schemas for a database except SYS and SYSTEM
###
###  PREREQUISITE: create table to save current statistics              
###
###            exec dbms_stats.create_stat_table('SYS', 'STATTAB');
###
###  Statement to execute: 
###
#######################################################################################################################
. ~/.bash_profile
#
# Check if SID is specified
#
if [ $# -eq 0 ];then
   echo 'You must enter SID'
   exit 1;
fi
SID=`echo $1 | tr A-Z a-z`

sqlplus  "/ as sysdba" <<-EOF
set  linesize 130
set  pagesize  99

show parameter PGA

SELECT optimal_count,   round(optimal_count*100/total,   2) optimal_perc, 
       onepass_count,   round(onepass_count*100/total,   2) onepass_perc, 
       multipass_count, round(multipass_count*100/total, 2) multipass_perc 
FROM 
       (SELECT decode(sum(total_executions), 0, 1, sum(total_executions)) total, 
               sum(OPTIMAL_EXECUTIONS)     optimal_count, 
               sum(ONEPASS_EXECUTIONS)     onepass_count, 
               sum(MULTIPASSES_EXECUTIONS) multipass_count 
        FROM   v\$sql_workarea_histogram 
        WHERE  low_optimal_size > 64*1024);

SELECT low_optimal_size/1024 low_kb,(high_optimal_size+1)/1024 high_kb,
       optimal_executions, onepass_executions, multipasses_executions 
FROM   v\$sql_workarea_histogram 
WHERE  total_executions != 0;

EOF
