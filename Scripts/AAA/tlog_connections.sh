#!/bin/sh
#
# Set up the env
ORACLE_BASE=/app/oracle
ORACLE_HOME=/app/oracle/product/10.2.0/ytrsprd1
ORACLE_SID=ytrsprd1
export ORACLE_BASE ORACLE_HOME ORACLE_SID
ORAENV_ASK=NO
OA_PRD=$ORACLE_BASE/admin/ytrsprd1
LD_LIBRARY_PATH=$ORACLE_HOME/lib:/usr/lib
PATH=$PATH:$ORACLE_HOME/bin:/usr/sbin
export OA_PRD LD_LIBRARY_PATH PATH ORAENV_ASK
#

export ORACLE_SID=$1
export MAX_ARCH_LAG=$2

export REPORT=/tmp/max_tlog_$ORACLE_SID.rpt
export SERVER_NAME=`hostname`

sqlplus -s "/ as sysdba" << EOF
SET SERVEROUTPUT ON SIZE 100000
spool $REPORT

declare
v_lag number(20);
begin
select (
select count(*) from v\$session where machine = 'tlogaaa-02.atl.greenpoints.com') into v_lag from dual;

if v_lag > $MAX_ARCH_LAG
 then 
 dbms_output.put_line('TLOG Connections are exceeded :  sessions Page DBA immediately');
 dbms_output.put_line('lag is  = ' || v_lag);
end if;  
end;
/

spool off
EXIT
EOF

Response=`grep -c 'Page DBA immediately' $REPORT`
if [ $Response -gt 0 ]
then
    echo 'oraprod-05 ytrsprd1 tlog connections issue, please check' | mail -s "YTRSPRD1 TLOG Connections issue" dba@shsolutions.com dmitryr@appcard.com padepu@shsolutions.com
fi
