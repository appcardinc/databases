#!/bin/sh -x
#*************************************************************************
#*
#*  Name: purge_client_monitoring_log.sh
#*
#*  Description: Purge client_monitoring_log what is older than 14 days
#*
#*************************************************************************
#
# Source bash profile


if [ $# -ne 1 ];then
   echo 'Usage:$0 SID'
   exit 1;
fi

SID=`echo $1 | tr A-Z a-z`

. $HOME/.bash_profile $SID

export SID


sqlplus log/`get_password.sh $SID log`@$SID <<EOF
set echo on
set term on
set ver on
set feed on

delete from client_monitoring_log where to_char(CML_CREATION_TIME, 'dd-mon-yy') < sysdate - 14;
commit;
exit;
EOF
