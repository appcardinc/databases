#!/bin/sh
#######################################################################################################################
###
###           This job analyzes all schemas for a database except SYS and SYSTEM
###
###  PREREQUISITE: create table to save current statistics              
###
###            exec dbms_stats.create_stat_table('SYS', 'STATTAB');
###
###  Statement to execute: 
###
###             exec dbms_stats.gather_schema_stats(ownname           => s.schema,
###                                                 cascade           => TRUE,
###                                                 method_opt        => 'FOR ALL INDEXED COLUMNS SIZE 1',
###                                                 degree            => 4,
###                                                 estimate_percent  => 25,
###                                                 statown           => 'SYS', 
###                                                 stattab           => 'STATTAB')
###  PARAMETER         DESCRIPTION
###  ---------         --------------------------------------------------------------------
###  ownname           analyze ALL tables  for this particular schema
###  cascade           analyze ALL indexes
###  method_opt        create  histograms:
###                      'FOR ALL COLUMNS SIZE 1'         - for all columns (default, not recommended)
###                      'FOR ALL INDEXED COLUMNS SIZE 1' - for indexes columns only;
###  degree            degree of parallelism, if desired
###  estimate_percent  this parameter could be set to 'DBMS_STATS.AUTO_SAMPLE_SIZE', however the latter one has some issues
###  statown, stattab  owner & table name where current statistics will be saved.
###                    This is an insurance policy in case of performance degradation as a result of this job execution.
###                    To create stattab, run: exec dbms_stats.create_stat_table('SYS', 'STATTAB');
###
###
#######################################################################################################################
. ~/.bash_profile
#
# Check if SID is specified
#
if [ $# -eq 0 ];then
   echo 'You must enter SID'
   exit 1;
fi
SID=`echo $1 | tr A-Z a-z`

#
# Verify SID's validity and get password for sys
#
sys_password=`get_password.sh $SID sys`
if [ $sys_password = "" ];then
   echo 'SID not found in the password file'
   exit 1
fi

sqlplus "sys/$sys_password@$SID as sysdba" <<-EOF
set serveroutput on
set timing       on
BEGIN
     FOR s in (select distinct(owner) schema from dba_tables where owner not like '%SYS%')
     LOOP
         dbms_output.put_line('Analyzing the entire schema: '||s.schema); 
         dbms_stats.gather_schema_stats(ownname           => s.schema,
                                        cascade           => TRUE,
                                        method_opt        => 'FOR ALL INDEXED COLUMNS SIZE 1',
                                        degree            => 4,
                                        estimate_percent  => 25,
                                        statown           => 'SYS',      
                                        stattab           => 'STATTAB');
     END LOOP;
END;
/
EOF
