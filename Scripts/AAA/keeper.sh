#!/bin/sh
#######################################################################################################################
###
###           This job determines SQL statement in the Shared Pool to be pinned and does so
###
###
#######################################################################################################################
. ~/.bash_profile
#
# Check if SID is specified
#
if [ $# -eq 0 ];then
   echo 'You must enter SID'
   exit 1;
fi
SID=`echo $1 | tr A-Z a-z`
IXORA=/home/oracle/scripts/dba
#
# Verify SID's validity and get password for sys
#
#sys_password=`get_password.sh $SID sys`
#if [ $sys_password = "" ];then
#   echo 'SID not found in the password file'
#   exit 1
#fi

cd $IXORA
sqlplus -s "sys/G0rd0n@$SID as sysdba" <<-EOF
@xr_keeper.sql
EOF
