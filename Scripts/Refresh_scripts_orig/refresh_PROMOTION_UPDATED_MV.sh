#!/bin/sh

#
# Set up the env

PATH=$PATH:$HOME/.local/bin:$HOME/bin

export PATH

export TNS_ADMIN=/home/jboss/tnsora

export ORACLE_SID=prodrotw
export REPORT=/tmp/promotion_updated_mv_refresh_$ORACLE_SID.rpt
export ENCPWD=`cat /home/jboss/db_scripts/encpwd | openssl enc -base64 -d`

#export MAIL_RECIPIENTS="chak@appcard.com dmitryr@appcard.com dlocke@appcard.com smorales@appcard.com"
export MAIL_RECIPIENTS="dmitryr@appcard.com"
 
sqlplus -s "cds/$ENCPWD@$ORACLE_SID" << EOF

set head off
set feedback off
spo $REPORT

select 'Refresh PMT.PROMOTION_UPDATED_MV' from dual;

select 'Started: '||to_char(sysdate, 'YYYY-MON-DD HH24:MI:SS') from dual;

drop table PMT.TEMP_PROMOTION_UPDATED_MV;

create table PMT.TEMP_PROMOTION_UPDATED_MV nologging
as
SELECT p2.promotion_id as promotion_id, p2."UPDATED" as pmt_updated, 'promotions' as pmt_system
              FROM pmt.promotions p2
                WHERE p2.deleted IS NULL
            UNION ALL
            SELECT db.promotion_id as promotion_id, db."UPDATED" AS pmt_updated, 'definition_benefit' as pmt_system
              FROM pmt.definition_benefit db
                WHERE db.deleted IS NULL
            UNION ALL
            SELECT dl.promotion_id AS promotion_id, dl."UPDATED" AS pmt_updated, 'definition_list' as pmt_system
              FROM pmt.definition_list dl
                WHERE dl.deleted IS NULL
            UNION ALL
            SELECT dv.owner_id AS promotion_id, dv."UPDATED" AS pmt_updated, 'definition_variable' as pmt_system
              FROM pmt.definition_variable dv
                WHERE dv.owner_type = 'P'
                  AND dv.deleted IS NULL
            UNION ALL
            SELECT p3.promotion_id as promotion_id, dtl."UPDATED" as pmt_updated, 'data_list' as pmt_system
            FROM PMT.promotions p3
              JOIN PMT.DEFINITION_LIST dl ON dl.promotion_id = p3.promotion_id and dl.deleted is null
              JOIN PMT.CONSOLIDATION_LIST cl ON cl.DEFINITION_LIST_ID = dl.DEFINITION_LIST_ID AND cl.deleted IS NULL
              JOIN PMT.data_list dtl ON dtl.data_list_id = cl.DATA_ID AND dtl.deleted IS NULL
            WHERE p3.deleted is null
            UNION ALL
            SELECT p4.promotion_id, dlv."UPDATED" as pmt_updated, 'data_list_value' as pmt_system
            FROM PMT.promotions p4
              JOIN PMT.DEFINITION_LIST dl ON dl.promotion_id = p4.promotion_id and dl.deleted is null
              JOIN PMT.CONSOLIDATION_LIST cl ON cl.DEFINITION_LIST_ID = dl.DEFINITION_LIST_ID AND cl.deleted IS NULL
              JOIN PMT.data_list dtl ON dtl.data_list_id = cl.DATA_ID AND dtl.deleted IS NULL
              JOIN PMT.data_list_value dlv ON dlv.data_list_id = dtl.data_list_id AND dlv.deleted IS NULL
            WHERE p4.promotion_id IS NULL;

drop table PMT.PROMOTION_UPDATED_MV;

create table PMT.PROMOTION_UPDATED_MV nologging as
with p AS (select * from PMT.TEMP_PROMOTION_UPDATED_MV)
SELECT distinct p3.promotion_id AS promotion_id, p3.pmt_updated AS pmt_updated, 
  p4.pmt_system
  FROM
    (SELECT p2.promotion_id, MAX(p2.pmt_updated) AS pmt_updated
      FROM p p2
      GROUP BY p2.promotion_id
    ) p3
    JOIN
      p p4 ON p4.promotion_id = p3.promotion_id 
        AND p4.pmt_updated = (SELECT MAX(p4.pmt_updated) FROM p p4 WHERE p4.promotion_id = p3.promotion_id);

grant select on PMT.PROMOTION_UPDATED_MV to public;        

select 'Ended: '||to_char(sysdate, 'YYYY-MON-DD HH24:MI:SS') from dual;

select 'Number of Records PMT.PROMOTION_UPDATED_MV: '||count(1) from PMT.PROMOTION_UPDATED_MV;

spo off;

EXIT
EOF

 
echo -e "Subject: Refresh PMT.PROMOTION_UPDATED_MV on PRODROTW Oracle 12c AWS RDS\nTo: $MAIL_RECIPIENTS" | cat - $REPORT | /usr/sbin/sendmail $MAIL_RECIPIENTS 
