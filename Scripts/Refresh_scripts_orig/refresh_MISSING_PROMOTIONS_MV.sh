#!/bin/sh

#
# Set up the env

PATH=$PATH:$HOME/.local/bin:$HOME/bin

export PATH

export TNS_ADMIN=/home/jboss/tnsora

export ORACLE_SID=prodrotw
export REPORT=/tmp/missing_promotions_mv_refresh_$ORACLE_SID.rpt
export ENCPWD=`cat /home/jboss/db_scripts/encpwd | openssl enc -base64 -d`

#export MAIL_RECIPIENTS="chak@appcard.com dmitryr@appcard.com dlocke@appcard.com smorales@appcard.com"
export MAIL_RECIPIENTS="dmitryr@appcard.com"
 
sqlplus -s "cds/$ENCPWD@$ORACLE_SID" << EOF

set head off
set feedback off
spo $REPORT

select 'Refresh PMT.MISSING_PROMOTIONS_MV' from dual;

select 'Started: '||to_char(sysdate, 'YYYY-MON-DD HH24:MI:SS') from dual;

drop table PMT.MISSING_PROMOTIONS_MV;

create table PMT.MISSING_PROMOTIONS_MV nologging 
as
SELECT sp.store_id as store_id, sp.promotion_id, p.promotion_type as promotion_type, 
  p.start_date as start_date, p.end_date as end_date, bp.status_id as status_id, 
  l.run_code as run_code, l.last_run as last_run
FROM
(
  SELECT sp.store_id AS store_id, sp.promotion_id as promotion_id
  FROM pmt.promotions_by_store_mv sp
  MINUS
  SELECT pl.store_id as store_id, pl.promotion_id as promotion_id
  FROM COMMON.store_promo_list pl
    JOIN pmt.promotions p ON p.promotion_id = pl.promotion_id
  WHERE pl.on_jbrain = 1
    AND run_code = (SELECT run_code from common.latest_evaluation_mv)
) sp
JOIN pmt.promotions p ON p.promotion_id = sp.promotion_id
JOIN pmt.business_process bp ON bp.business_process_id = p.business_process_id
CROSS JOIN common.latest_evaluation_mv l
WHERE trunc(SYSDATE) BETWEEN trunc(p.start_date -  7) AND trunc(p.end_date)
  AND bp.status_id >= 8 
  AND bp.status_id != 9;

grant select on PMT.MISSING_PROMOTIONS_MV to public;        

select 'Ended: '||to_char(sysdate, 'YYYY-MON-DD HH24:MI:SS') from dual;

select 'Number of Records PMT.MISSING_PROMOTIONS_MV: '||count(1) from PMT.MISSING_PROMOTIONS_MV;

spo off;

EXIT
EOF

 
echo -e "Subject: Refresh PMT.MISSING_PROMOTIONS_MV on PRODROTW Oracle 12c AWS RDS\nTo: $MAIL_RECIPIENTS" | cat - $REPORT | /usr/sbin/sendmail $MAIL_RECIPIENTS 
